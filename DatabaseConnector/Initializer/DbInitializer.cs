using DatabaseConnector.Tables.Configuration;
using DatabaseConnector.Tables.Fields;
using DatabaseConnector.Tables.Permissions;
using DatabaseConnector.Tables.Settings;
using DatabaseConnector.Tables.StatCards;
using DatabaseConnector.Tables.Structure;
using DatabaseConnector.Tables.Users;
using Encryption;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DatabaseConnector
{
  public static class DbInitializer
  {
    public static void Initialize(BullyAssureContext db)
    {
      db.Database.EnsureCreated();

      if (!db.GlobalSettings.Any())
      {
        db.GlobalSettings.Add(
          new GlobalSettings
          {
            MerchantID = "12637265",
            MerchantKey = "zjihlp3pqa7q2"
          }
        );
        db.SaveChanges();
      }

      if (!db.OrgStructures.Any())
      {
        db.OrgStructures.AddRange(
          new OrgStructure
          {
            StructureName = "Administrator"
          },
          new OrgStructure
          {
            StructureName = "Developer"
          },
          new OrgStructure
          {
            StructureName = "Kennel"
          },
          new OrgStructure
          {
            StructureName = "Breeder"
          },
          new OrgStructure
          {
            StructureName = "Owner"
          }
        );
        db.SaveChanges();
      }

      if (!db.PermissionsStructures.Any())
      {
        db.PermissionsStructures.AddRange(new List<PermissionsStructure>
                {
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Administrator").StructureID,
                        PermissionName = "Pedigree View",
                        Path = "Pedigree"
                    },
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Administrator").StructureID,
                        PermissionName = "Owner View",
                        Path = "Owner"
                    },
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Administrator").StructureID,
                        PermissionName = "Kennel View",
                        Path = "Kennel"
                    },
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Administrator").StructureID,
                        PermissionName = "Breeder View",
                        Path = "Breeder"
                    },
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Administrator").StructureID,
                        PermissionName = "Permission View",
                        Path = "Permission"
                    },
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Administrator").StructureID,
                        PermissionName = "Settings View",
                        Path = "Settings"
                    },
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Administrator").StructureID,
                        PermissionName = "Dashboard View",
                        Path = "Dashboard"
                    },
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Administrator").StructureID,
                        PermissionName = "User View",
                        Path = "User"
                    },

                    ///Developer

                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Developer").StructureID,
                        PermissionName = "Pedigree View",
                        Path = "Pedigree"
                    },
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Developer").StructureID,
                        PermissionName = "Owner View",
                        Path = "Owner"
                    },
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Developer").StructureID,
                        PermissionName = "Permission View",
                        Path = "Permission"
                    },
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Developer").StructureID,
                        PermissionName = "Settings View",
                        Path = "Settings"
                    },
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Developer").StructureID,
                        PermissionName = "Dashboard View",
                        Path = "Dashboard"
                    },
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Developer").StructureID,
                        PermissionName = "User View",
                        Path = "User"
                    },

                    ///Kennel

                    //new PermissionsStructure
                    //{
                    //    OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Kennel").StructureID,
                    //    PermissionName = "Settings View",
                    //    Path = "Settings"
                    //},
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Kennel").StructureID,
                        PermissionName = "Pedigree View",
                        Path = "Pedigree"
                    },
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Kennel").StructureID,
                        PermissionName = "Breeder View",
                        Path = "Breeder"
                    },
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Kennel").StructureID,
                        PermissionName = "Dashboard View",
                        Path = "Dashboard"
                    },
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Kennel").StructureID,
                        PermissionName = "User View",
                        Path = "User"
                    },

                    ///Breeder

                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Breeder").StructureID,
                        PermissionName = "Kennel View",
                        Path = "Kennel"
                    },
                    //new PermissionsStructure
                    //{
                    //    OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Breeder").StructureID,
                    //    PermissionName = "Settings View",
                    //    Path = "Settings"
                    //},
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Breeder").StructureID,
                        PermissionName = "Dashboard View",
                        Path = "Dashboard"
                    },
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Breeder").StructureID,
                        PermissionName = "User View",
                        Path = "User"
                    },

                    ///Owner
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Owner").StructureID,
                        PermissionName = "Pedigree View",
                        Path = "Pedigree"
                    },
                    new PermissionsStructure
                    {
                        OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Owner").StructureID,
                        PermissionName = "Dashboard View",
                        Path = "Dashboard"
                    },
                });

        db.SaveChanges();
      }

      if (!db.Users.Any())
      {
        db.Users.Add(new UserMain
        {
          Username = "admin",
          Firstname = "Administrator",
          Surname = "Admin",
          Email = "vbester3@gmail.com",
          isAdmin = true,
          Password = new PasswordEncrypt().GeneratePassword("1234"),
          OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Administrator").StructureID,
          UserPermission = new UserPermissions
          {
            EditUsers = true,
            EditMenuNav = true,
            EditPermissions = true,
            EditKennelFields = true,
            EditBreederFields = true,
            EditPedigreeFields = true,
            EditGlobalSettings = true,
            ViewAllKennels = true,
            ViewAllBreeders = true,
            ViewAllPedigrees = true,
            ViewAllUsers = true
          }
        });
        db.Users.Add(new UserMain
        {
          Username = "vincent",
          Firstname = "Vincent",
          Surname = "Bester",
          Email = "vbester3@gmail.com",
          isDeveloper = true,
          Password = new PasswordEncrypt().GeneratePassword("1234"),
          OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Developer").StructureID,
          UserPermission = new UserPermissions
          {
            EditUsers = true,
            EditMenuNav = true,
            EditPermissions = true,
            EditKennelFields = true,
            EditBreederFields = true,
            EditPedigreeFields = true,
            EditGlobalSettings = true,
            ViewAllKennels = true,
            ViewAllBreeders = true,
            ViewAllPedigrees = true,
            ViewAllUsers = true
          }
        });
        db.SaveChanges();
      }

      if (!db.FieldTypes.Any())
      {
        db.FieldTypes.AddRange(new List<FieldType>{
                    new FieldType{
                        FieldTypeName = "Input"
                    },
                    new FieldType{
                        FieldTypeName = "Textarea"
                    },
                    new FieldType{
                        FieldTypeName = "Dropdown"
                    },
                    new FieldType{
                        FieldTypeName = "Checkbox"
                    },
                    new FieldType{
                        FieldTypeName = "Document"
                    }
                });
        db.SaveChanges();
      }

      if (!db.Settings.Any())
      {
        db.Settings.AddRange(new List<Setting>
                {
                    new Setting
                    {
                        SettingName = "Dashboard",
                        SettingNavName = "Dashboard",
                        Action = "Index",
                        To = "Dashboard",
                        Order = 1
                    },
                    new Setting
                    {
                        SettingName = "Kennels",
                        SettingNavName = "Kennels",
                        Action = "Index",
                        To = "Kennel",
                        Order = 2
                    },
                    new Setting
                    {
                        SettingName = "Breeders",
                        SettingNavName = "Breeders",
                        Action = "Index",
                        To = "Breeder",
                        Order = 3
                    },
                    new Setting
                    {
                        SettingName = "Pedigrees",
                        SettingNavName = "Pedigrees",
                        Action = "Index",
                        To = "Pedigree",
                        Order = 4
                    },
                    new Setting
                    {
                        SettingName = "Pedigree Applications",
                        SettingNavName = "Pedigree Applications",
                        Action = "Applications",
                        To = "Pedigree",
                        Order = 5
                    },
                    new Setting
                    {
                        SettingName = "MyProfile",
                        SettingNavName = "My Profile",
                        Action = "Index",
                        To = "User",
                        Order = 6
                    },
                    new Setting
                    {
                        SettingName = "Settings",
                        SettingNavName = "Settings",
                        Action = "Index",
                        To = "Settings",
                        Order = 7
                    }
                });
        db.SaveChanges();
      }

      if (!db.SystemConfigurations.Any())
      {
        db.SystemConfigurations.Add(new SystemConfiguration
        {
          EmailFromAddress = "Bully Assure System",
          Password = "xdr5%5tgb",
          UserName = "vbester3.games@gmail.com",
          SMTPClient = "smtp.gmail.com",
          Port = 25,
          ErrorRedirectAction = "LoginPage",
          ErrorRedirectTo = "Login"
        });
        db.SaveChanges();
      }

      if (!db.StatCards.Any())
      {
        db.StatCards.AddRange(new List<StatCard>
                {
                    new StatCard
                    {
                        CardName = "Pedigrees",
                        CardDescription = "Total of Pedigrees on system.",
                        CardAction = "Index",
                        CardTo = "Pedigree",
                        CardNavName = "To Pedigrees",
                        ValueBottomAlg = "SELECT COUNT(*) as Value FROM Pedigrees",
                        ValueTopAlg = "SELECT 1 as Value",
                        CreatedbyUserID = db.Users.First(e => e.Username == "admin").UserID,
                        CreatedDate = DateTime.Now
                    },
                    new StatCard
                    {
                        CardName = "Owners",
                        CardDescription = "Total of Owners on system.",
                        CardAction = "Index",
                        CardTo = "Owner",
                        CardNavName = "To Owners",
                        ValueBottomAlg = "SELECT COUNT(*) as Value FROM Owners",
                        ValueTopAlg = "SELECT 1 as Value",
                        CreatedbyUserID = db.Users.First(e => e.Username == "admin").UserID,
                        CreatedDate = DateTime.Now
                    }
                });
        db.SaveChanges();
      }
    }
  }
}