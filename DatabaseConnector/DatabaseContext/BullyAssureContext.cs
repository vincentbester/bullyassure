namespace DatabaseConnector
{
  using DatabaseConnector.Tables.Audits;
  using DatabaseConnector.Tables.Breeders;
  using DatabaseConnector.Tables.Communication;
  using DatabaseConnector.Tables.Configuration;
  using DatabaseConnector.Tables.Documents;
  using DatabaseConnector.Tables.Fields;
  using DatabaseConnector.Tables.Kennels;
  using DatabaseConnector.Tables.Owners;
  using DatabaseConnector.Tables.Pedigrees;
  using DatabaseConnector.Tables.Permissions;
  using DatabaseConnector.Tables.Settings;
  using DatabaseConnector.Tables.StatCards;
  using DatabaseConnector.Tables.Structure;
  using DatabaseConnector.Tables.Users;
  using Microsoft.EntityFrameworkCore;

  public partial class BullyAssureContext : DbContext
  {
    public BullyAssureContext(DbContextOptions<BullyAssureContext> options)
        : base(options)
    {
    }

    public virtual DbSet<PedigreeMain> Pedigrees { get; set; }
    public virtual DbSet<PedigreeUser> PedigreeUsers { get; set; }
    public virtual DbSet<PedigreeFieldsValue> PedigreeFieldsValues { get; set; }
    public virtual DbSet<PedigreePageField> PedigreePageField { get; set; }
    public virtual DbSet<PedigreeApplication> PedigreeApplications { get; set; }
    public virtual DbSet<PedigreeApplication_Response> PedigreeApplicationResponses { get; set; }
    public virtual DbSet<ResponseDocuments> ResponseDocuments { get; set; }
    public virtual DbSet<OwnerMain> Owners { get; set; }
    public virtual DbSet<OwnerFieldValue> OwnerFieldValues { get; set; }
    public virtual DbSet<OwnerPageField> OwnerPageFields { get; set; }
    public virtual DbSet<OwnerPedigree> OwnerPedigrees { get; set; }
    public virtual DbSet<DocumentMain> Documents { get; set; }
    public virtual DbSet<KennelMain> Kennels { get; set; }
    public virtual DbSet<KennelUser> KennelUsers { get; set; }
    public virtual DbSet<KennelFieldsValues> KennelFieldsValues { get; set; }
    public virtual DbSet<KennelPageField> KennelPageFields { get; set; }
    public virtual DbSet<BreederMain> Breeders { get; set; }
    public virtual DbSet<BreederUser> BreederUsers { get; set; }
    public virtual DbSet<BreederFieldsValues> BreederFieldsValues { get; set; }
    public virtual DbSet<BreederPageField> BreederPageFields { get; set; }
    public virtual DbSet<UserMain> Users { get; set; }
    public virtual DbSet<AuditLogin> AuditLogins { get; set; }
    public virtual DbSet<ApplicationLogging> Loggings { get; set; }
    public virtual DbSet<EmailCapture> EmailCaptures { get; set; }
    public virtual DbSet<UserPermissions> UserPermissions { get; set; }
    public virtual DbSet<UserSession> UserSessions { get; set; }
    public virtual DbSet<FieldType> FieldTypes { get; set; }
    public virtual DbSet<Setting> Settings { get; set; }
    public virtual DbSet<UserSetting> UserSettings { get; set; }
    public virtual DbSet<StatCard> StatCards { get; set; }
    public virtual DbSet<SystemConfiguration> SystemConfigurations { get; set; }
    public virtual DbSet<OrgStructure> OrgStructures { get; set; }
    public virtual DbSet<PermissionsStructure> PermissionsStructures { get; set; }
    public virtual DbSet<GlobalSettings> GlobalSettings { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<AuditLogin>()
          .Property(s => s.AuditID);

      modelBuilder.Entity<ApplicationLogging>()
          .Property(s => s.LoggingID);

      modelBuilder.Entity<EmailCapture>()
          .Property(s => s.EmailCaptureID);

      modelBuilder.Entity<PedigreeMain>(entity =>
      {
        entity.HasKey(s => s.PedigreeID);

        entity.Property(s => s.BAID)
          .UseIdentityColumn()
          .ValueGeneratedOnAdd()
          .HasColumnName("BAID");

        entity.Property(s => s.PedigreeID);

        entity.Property(s => s.SireID);

        entity.Property(s => s.DamID);

        entity.HasOne(e => e.Dam)
          .WithOne();

        entity.HasOne(e => e.Sire)
          .WithOne();
      });

      modelBuilder.Entity<OwnerPedigree>()
          .Property(s => s.OwnerPedigreeID);

      modelBuilder.Entity<OwnerMain>()
          .Property(s => s.OwnerID);

      modelBuilder.Entity<OwnerFieldValue>()
          .Property(s => s.OwnerFieldValueID);

      modelBuilder.Entity<KennelPageField>()
          .Property(s => s.FieldID);

      modelBuilder.Entity<DocumentMain>()
          .Property(s => s.DocumentID);

      modelBuilder.Entity<UserMain>()
          .Property(s => s.UserID);

      modelBuilder.Entity<UserSession>()
          .Property(s => s.SessionID);

      modelBuilder.Entity<FieldType>()
          .Property(s => s.FieldID);

      modelBuilder.Entity<Setting>()
          .Property(s => s.ID);

      modelBuilder.Entity<UserSetting>()
          .Property(s => s.UserSettingPermissionID);

      modelBuilder.Entity<StatCard>()
          .Property(s => s.CardID);

      modelBuilder.Entity<SystemConfiguration>()
          .Property(s => s.ConfigID);

      modelBuilder.Entity<OrgStructure>()
          .Property(s => s.StructureID);

      modelBuilder.Entity<PermissionsStructure>()
          .Property(s => s.PermissionID);

      modelBuilder.Entity<UserPermissions>()
          .Property(s => s.UserPermissionsId);

      modelBuilder.Entity<GlobalSettings>()
          .Property(s => s.GlobalSettingID);
    }
  }
}