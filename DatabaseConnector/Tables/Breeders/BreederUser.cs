﻿using DatabaseConnector.Tables.Users;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DatabaseConnector.Tables.Breeders
{
  public class BreederUser
  {
    [Key]
    public Guid BreederUserId { get; set; }

    public Guid BreederId { get; set; }

    [JsonIgnore]
    public BreederMain Breeder { get; set; }

    public Guid UserId { get; set; }

    [JsonIgnore]
    public UserMain User { get; set; }
  }
}