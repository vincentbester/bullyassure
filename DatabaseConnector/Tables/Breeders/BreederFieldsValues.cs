﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace DatabaseConnector.Tables.Breeders
{
  public class BreederFieldsValues
  {
    [Key]
    public Guid BreederFieldValueID { get; set; }

    public Guid BreederID { get; set; }

    [JsonIgnore]
    public BreederMain Breeder { get; set; }

    public string FieldValue { get; set; }
    public string FieldValueAlpha { get; set; }
    public Guid BreederFieldsID { get; set; }

    [JsonIgnore]
    public BreederPageField BreederFields { get; set; }

    public bool Required { get; set; }
    public bool isDeleted { get; set; }
  }
}