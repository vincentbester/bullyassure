﻿using DatabaseConnector.Tables.Kennels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DatabaseConnector.Tables.Breeders
{
  public class BreederMain
  {
    [Key]
    public Guid BreederId { get; set; }

    public string BreederFirstName { get; set; }
    public string BreederSurname { get; set; }
    public string CellNumber { get; set; }
    public string EmailAddress { get; set; }
    public Guid KennelId { get; set; }

    [JsonIgnore]
    public KennelMain Kennel { get; set; }

    public bool isDeleted { get; set; }
    public List<BreederFieldsValues> BreederFieldsValues { get; set; }
  }
}