﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DatabaseConnector.Tables.Audits
{
  public class ApplicationLogging
  {
    [Key]
    public Guid LoggingID { get; set; }

    public Guid? UserID { get; set; }
    public string ErrorMessage { get; set; }
    public string Topath { get; set; }
    public string QueryString { get; set; }
    public DateTime CaptureDate { get; set; }
  }
}