using System;
using System.ComponentModel.DataAnnotations;

namespace DatabaseConnector.Tables.Audits
{
  public class AuditLogin
  {
    [Key]
    public Guid AuditID { get; set; }

    public string Username { get; set; }
    public string Status { get; set; }
    public DateTime CaptureDate { get; set; }
  }

  public class LogAuditLogin
  {
    public LogAuditLogin(BullyAssureContext _db)
    {
      db = _db;
    }

    private BullyAssureContext db;

    public enum AuditLoginStatus
    {
      Success = 200,
      Failed = 500
    }

    public void LogAudit(string Username, string Status)
    {
      if (!string.IsNullOrEmpty(Username))
      {
        db.AuditLogins.Add(new AuditLogin
        {
          Username = Username,
          Status = Status,
          CaptureDate = DateTime.Now
        });
        db.SaveChanges();
      }
    }
  }
}