﻿using DatabaseConnector.Tables.Users;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DatabaseConnector.Tables.Pedigrees
{
  public class PedigreeUser
  {
    [Key]
    public Guid PedigreeUserId { get; set; }

    public Guid PedigreeID { get; set; }

    [JsonIgnore]
    public PedigreeMain Pedigree { get; set; }

    public Guid UserId { get; set; }

    [JsonIgnore]
    public UserMain User { get; set; }
  }
}