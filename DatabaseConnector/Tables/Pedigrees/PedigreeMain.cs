using DatabaseConnector.Tables.Breeders;
using DatabaseConnector.Tables.Kennels;
using DatabaseConnector.Tables.Owners;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace DatabaseConnector.Tables.Pedigrees
{
  public class PedigreeMain
  {
    [Key]
    public Guid PedigreeID { get; set; }

    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long BAID { get; set; }

    public string RegID { get; set; }
    public string Name { get; set; }
    public string Breed { get; set; }
    public string Sex { get; set; }
    public string Description { get; set; }
    public string DOB { get; set; }
    public string DOI { get; set; }
    public string ChipNo { get; set; }
    public string AddressPostal { get; set; }
    public string Address { get; set; }
    public string PostalCode { get; set; }
    public string Country { get; set; }
    public Guid? OwnerId { get; set; }

    [JsonIgnore]
    public OwnerMain Owner { get; set; }

    public Guid? CoOwnerId { get; set; }

    [JsonIgnore]
    public OwnerMain CoOwner { get; set; }

    public Guid? BreederId { get; set; }

    [JsonIgnore]
    public BreederMain Breeder { get; set; }

    public Guid? KennelId { get; set; }

    [JsonIgnore]
    public KennelMain Kennel { get; set; }

    public Guid? SireID { get; set; }

    [JsonIgnore]
    public PedigreeMain Sire { get; set; }

    public Guid? DamID { get; set; }

    [JsonIgnore]
    public PedigreeMain Dam { get; set; }

    public bool Flagged { get; set; }
    public bool IsDeleted { get; set; }
    public string FrontImage { get; set; }
    public string BackImage { get; set; }
    public string LeftImage { get; set; }
    public string RightImage { get; set; }
    public string AdditionalImage { get; set; }

    [JsonIgnore]
    public PedigreeApplication PedigreeApplication { get; set; }

    public List<PedigreeFieldsValue> PedigreeFieldsValues { get; set; }
  }
}