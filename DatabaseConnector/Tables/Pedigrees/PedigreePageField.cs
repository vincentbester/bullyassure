﻿using DatabaseConnector.Tables.Fields;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DatabaseConnector.Tables.Pedigrees
{
  public class PedigreePageField

  {
    [Key]
    public Guid FieldID { get; set; }

    public Guid FieldTypeID { get; set; }

    [JsonIgnore]
    public FieldType FieldType { get; set; }

    public string FieldName { get; set; }
    public bool isDeleted { get; set; }
  }
}