﻿using DatabaseConnector.Tables.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DatabaseConnector.Tables.Pedigrees
{
  public class PedigreeApplication
  {
    [Key]
    public Guid PedigreeApplicationId { get; set; }

    public Guid PedigreeId { get; set; }

    [JsonIgnore]
    public PedigreeMain Pedigree { get; set; }

    public Guid UserId { get; set; }

    [JsonIgnore]
    public UserMain User { get; set; }

    public List<PedigreeApplication_Response> Responses { get; set; }
    public DateTime DateApplied { get; set; }
    public bool Closed { get; set; }
    public DateTime ApplicationDateFinalised { get; set; }
    public bool Accepted { get; set; }
    public bool PaymentReceived { get; set; }
    public DateTime PaymentReceivedDate { get; set; }
  }

  public class PedigreeApplication_Response
  {
    [Key]
    public Guid ResponseId { get; set; }

    public Guid UserID { get; set; }

    [JsonIgnore]
    public UserMain User { get; set; }

    public string Message { get; set; }
    public List<ResponseDocuments> ResponseDocuments { get; set; }
    public DateTime DateCaptured { get; set; }
  }

  public class ResponseDocuments
  {
    [Key]
    public Guid ResponseDocId { get; set; }

    public string DocumentName { get; set; }
    public string DocumentBinary { get; set; }
    public DateTime DateUploaded { get; set; }
  }
}