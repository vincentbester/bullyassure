﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DatabaseConnector.Tables.Pedigrees
{
  public class PedigreeFieldsValue
  {
    [Key]
    public Guid PedigreeFieldValueID { get; set; }

    public Guid PedigreeID { get; set; }

    [JsonIgnore]
    public PedigreeMain Pedigree { get; set; }

    public string FieldValue { get; set; }
    public string FieldValueAlpha { get; set; }
    public Guid PedigreeFieldsID { get; set; }

    [JsonIgnore]
    public PedigreePageField PedigreeFields { get; set; }

    public bool Required { get; set; }
    public bool isDeleted { get; set; }
  }
}