using System;
using System.ComponentModel.DataAnnotations;

namespace DatabaseConnector.Tables.Documents
{
  public class DocumentMain
  {
    [Key]
    public Guid DocumentID { get; set; }

    public bool isDeleted { get; set; }
  }
}