﻿using DatabaseConnector.Tables.Users;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DatabaseConnector.Tables.Kennels
{
  public class KennelUser
  {
    [Key]
    public Guid KennelUserId { get; set; }

    public Guid KennelID { get; set; }

    [JsonIgnore]
    public KennelMain Kennel { get; set; }

    public Guid UserId { get; set; }

    [JsonIgnore]
    public UserMain User { get; set; }
  }
}