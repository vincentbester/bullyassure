﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace DatabaseConnector.Tables.Kennels
{
  public class KennelFieldsValues
  {
    [Key]
    public Guid KennelFieldValueID { get; set; }

    public Guid KennelID { get; set; }

    [JsonIgnore]
    public KennelMain Kennel { get; set; }

    public string FieldValue { get; set; }
    public string FieldValueAlpha { get; set; }
    public Guid KennelFieldsID { get; set; }

    [JsonIgnore]
    public KennelPageField KennelFields { get; set; }

    public bool Required { get; set; }
    public bool isDeleted { get; set; }
  }
}