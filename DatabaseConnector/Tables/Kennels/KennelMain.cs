﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DatabaseConnector.Tables.Kennels
{
  public class KennelMain
  {
    [Key]
    public Guid KennelID { get; set; }

    public string KennelName { get; set; }
    public bool MembershipUpToDate { get; set; }
    public string EmailAddress { get; set; }
    public bool isDeleted { get; set; }
    public List<KennelFieldsValues> KennelFieldsValues { get; set; }
  }
}