﻿using DatabaseConnector.Tables.Users;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace DatabaseConnector.Tables.Communication
{
  public class EmailCapture
  {
    [Key]
    public Guid EmailCaptureID { get; set; }

    public string ToEmail { get; set; }
    public string FromEmail { get; set; }
    public string EmailMessage { get; set; }
    public Guid UserID { get; set; }

    [JsonIgnore]
    public UserMain User { get; set; }

    public DateTime CaptureDate { get; set; }
    public bool SentSuccessfull { get; set; }
  }
}