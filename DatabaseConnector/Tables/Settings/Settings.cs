﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DatabaseConnector.Tables.Settings
{
  public class Setting
  {
    [Key]
    public Guid ID { get; set; }

    public string SettingName { get; set; }
    public int Order { get; set; }
    public string To { get; set; }
    public string Action { get; set; }
    public string SettingNavName { get; set; }
    public bool isDeleted { get; set; }
  }
}