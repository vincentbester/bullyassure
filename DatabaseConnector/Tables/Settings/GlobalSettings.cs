﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DatabaseConnector.Tables.Settings
{
  public class GlobalSettings
  {
    [Key]
    public Guid GlobalSettingID { get; set; }

    public string MerchantID { get; set; }
    public string MerchantKey { get; set; }
    public long PaymentFeePercentage { get; set; }
    public long KennelSubscriptionValue { get; set; }
    public long BreederSubscriptionValue { get; set; }
    public long PedigreeRegistrationValue { get; set; }
  }
}