using System;
using System.ComponentModel.DataAnnotations;

namespace DatabaseConnector.Tables.Fields
{
  public class FieldType
  {
    [Key]
    public Guid FieldID { get; set; }

    public string FieldTypeName { get; set; }
    public bool isDeleted { get; set; }
  }
}