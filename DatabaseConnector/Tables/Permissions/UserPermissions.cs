﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DatabaseConnector.Tables.Permissions
{
  public class UserPermissions
  {
    [Key]
    public Guid UserPermissionsId { get; set; }

    public bool EditMenuNav { get; set; }
    public bool EditPermissions { get; set; }
    public bool EditUsers { get; set; }
    public bool EditGlobalSettings { get; set; }
    public bool EditKennelFields { get; set; }
    public bool EditBreederFields { get; set; }
    public bool EditPedigreeFields { get; set; }
    public bool ViewAllKennels { get; set; }
    public bool ViewAllBreeders { get; set; }
    public bool ViewAllPedigrees { get; set; }
    public bool ViewAllUsers { get; set; }
  }
}