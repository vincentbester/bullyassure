﻿using DatabaseConnector.Tables.Owners;
using DatabaseConnector.Tables.Pedigrees;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DatabaseConnector.Tables.Permissions
{
  public class OwnerPedigree
  {
    [Key]
    public Guid OwnerPedigreeID { get; set; }

    public Guid OwnerID { get; set; }

    [JsonIgnore]
    public OwnerMain Owner { get; set; }

    public Guid PedigreeID { get; set; }

    [JsonIgnore]
    public PedigreeMain Pedigree { get; set; }
  }
}