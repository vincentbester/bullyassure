﻿using DatabaseConnector.Tables.Structure;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace DatabaseConnector.Tables.Permissions
{
  public class PermissionsStructure
  {
    [Key]
    public Guid PermissionID { get; set; }

    public string PermissionName { get; set; }
    public Guid OrgStructureID { get; set; }

    [JsonIgnore]
    public OrgStructure OrgStructure { get; set; }

    public string Path { get; set; }
  }
}