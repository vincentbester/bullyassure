﻿using DatabaseConnector.Tables.Settings;
using DatabaseConnector.Tables.Users;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DatabaseConnector.Tables.Permissions
{
  public class UserSetting
  {
    [Key]
    public Guid UserSettingPermissionID { get; set; }

    public Guid UserID { get; set; }

    [JsonIgnore]
    public UserMain User { get; set; }

    public Guid SettingID { get; set; }

    [JsonIgnore]
    public Setting Setting { get; set; }
  }
}