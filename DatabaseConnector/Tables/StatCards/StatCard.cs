﻿using DatabaseConnector.Tables.Users;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace DatabaseConnector.Tables.StatCards
{
  public class StatCard
  {
    [Key]
    public Guid CardID { get; set; }

    public double Value { get; set; }
    public string ValueTopAlg { get; set; }
    public string ValueBottomAlg { get; set; }
    public string CardName { get; set; }
    public string CardDescription { get; set; }
    public string CardAction { get; set; }
    public string CardTo { get; set; }
    public string CardNavName { get; set; }
    public Guid CreatedbyUserID { get; set; }

    [JsonIgnore]
    public UserMain CreatedbyUser { get; set; }

    public DateTime CreatedDate { get; set; }
    public bool isDeleted { get; set; }
  }
}