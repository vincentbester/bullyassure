﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DatabaseConnector.Tables.Configuration
{
  public class SystemConfiguration
  {
    [Key]
    public Guid ConfigID { get; set; }

    public string EmailFromAddress { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }
    public string SMTPClient { get; set; }
    public int Port { get; set; }
    public string ErrorRedirectAction { get; set; }
    public string ErrorRedirectTo { get; set; }
    public string WebsiteUrl { get; set; }
  }
}