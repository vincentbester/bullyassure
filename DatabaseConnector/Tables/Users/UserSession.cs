using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace DatabaseConnector.Tables.Users
{
  public class UserSession
  {
    [Key]
    public Guid SessionID { get; set; }

    public Guid UserMainID { get; set; }

    [JsonIgnore]
    public UserMain UserMain { get; set; }

    public DateTime EntryDate { get; set; }
    public DateTime ExpiryDate { get; set; }
    public string Token { get; set; }
  }
}