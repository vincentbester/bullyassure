using DatabaseConnector.Tables.Permissions;
using DatabaseConnector.Tables.Structure;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace DatabaseConnector.Tables.Users
{
  public class UserMain
  {
    [Key]
    public Guid UserID { get; set; }

    public string Username { get; set; }
    public string Firstname { get; set; }
    public string Surname { get; set; }
    public string Password { get; set; }
    public string Email { get; set; }
    public string Cellnumber { get; set; }
    public string ProfilePicture { get; set; }
    public string Address_Postal { get; set; }
    public string Address { get; set; }
    public string PostalCode { get; set; }
    public string Country { get; set; }
    public bool isAdmin { get; set; }
    public bool isDeveloper { get; set; }
    public bool isBreeder { get; set; }
    public bool isOwner { get; set; }
    public bool isKennel { get; set; }
    public Guid OrgStructureID { get; set; }

    [JsonIgnore]
    public OrgStructure OrgStructure { get; set; }

    public bool IsDeleted { get; set; }
    public Guid UserPermissionId { get; set; }

    [JsonIgnore]
    public UserPermissions UserPermission { get; set; }
  }
}