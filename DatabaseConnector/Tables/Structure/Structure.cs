﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace DatabaseConnector.Tables.Structure
{
  public class OrgStructure
  {
    [Key]
    public Guid StructureID { get; set; }

    public string StructureName { get; set; }
    public Guid? ParentStructureID { get; set; }

    [JsonIgnore]
    public OrgStructure ParentStructure { get; set; }
  }
}