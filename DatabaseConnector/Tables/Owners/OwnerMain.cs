using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DatabaseConnector.Tables.Owners
{
  public class OwnerMain
  {
    [Key]
    public Guid OwnerID { get; set; }

    public string OwnerName { get; set; }
    public string OwnerSurname { get; set; }
    public string OwnerAddress { get; set; }
    public string EmailAddress { get; set; }
    public string CellNumber { get; set; }
    public bool isDeleted { get; set; }
    public List<OwnerFieldValue> OwnerFieldValues { get; set; }
  }
}