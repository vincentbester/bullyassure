using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DatabaseConnector.Tables.Owners
{
  public class OwnerFieldValue
  {
    [Key]
    public Guid OwnerFieldValueID { get; set; }

    public Guid OwnerID { get; set; }

    [JsonIgnore]
    public OwnerMain Owner { get; set; }

    public string FieldValue { get; set; }
    public Guid OwnerFieldsID { get; set; }

    [JsonIgnore]
    public OwnerPageField OwnerFields { get; set; }

    public bool Required { get; set; }
    public bool isDeleted { get; set; }
  }
}