﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaseConnector.Migrations
{
  public partial class UpdatedPedigreesTable : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeFieldsValues_Pedigrees_OwnerID",
          table: "PedigreeFieldsValues");

      migrationBuilder.RenameColumn(
          name: "OwnerID",
          table: "PedigreeFieldsValues",
          newName: "PedigreeID");

      migrationBuilder.RenameColumn(
          name: "OwnerFieldValueID",
          table: "PedigreeFieldsValues",
          newName: "PedigreeFieldValueID");

      migrationBuilder.RenameIndex(
          name: "IX_PedigreeFieldsValues_OwnerID",
          table: "PedigreeFieldsValues",
          newName: "IX_PedigreeFieldsValues_PedigreeID");

      migrationBuilder.AddColumn<bool>(
          name: "isDeleted",
          table: "PedigreePageField",
          type: "INTEGER",
          nullable: false,
          defaultValue: false);

      migrationBuilder.AddColumn<string>(
          name: "FieldValueAlpha",
          table: "PedigreeFieldsValues",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<bool>(
          name: "isDeleted",
          table: "OwnerPageFields",
          type: "INTEGER",
          nullable: false,
          defaultValue: false);

      migrationBuilder.AddColumn<bool>(
          name: "isDeleted",
          table: "KennelPageFields",
          type: "INTEGER",
          nullable: false,
          defaultValue: false);

      migrationBuilder.AddColumn<bool>(
          name: "isDeleted",
          table: "BreederPageFields",
          type: "INTEGER",
          nullable: false,
          defaultValue: false);

      migrationBuilder.CreateIndex(
          name: "IX_Pedigrees_DamID",
          table: "Pedigrees",
          column: "DamID",
          unique: true);

      migrationBuilder.CreateIndex(
          name: "IX_Pedigrees_SireID",
          table: "Pedigrees",
          column: "SireID",
          unique: true);

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeFieldsValues_Pedigrees_PedigreeID",
          table: "PedigreeFieldsValues",
          column: "PedigreeID",
          principalTable: "Pedigrees",
          principalColumn: "PedigreeID",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Pedigrees_DamID",
          table: "Pedigrees",
          column: "DamID",
          principalTable: "Pedigrees",
          principalColumn: "PedigreeID",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Pedigrees_SireID",
          table: "Pedigrees",
          column: "SireID",
          principalTable: "Pedigrees",
          principalColumn: "PedigreeID",
          onDelete: ReferentialAction.Restrict);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeFieldsValues_Pedigrees_PedigreeID",
          table: "PedigreeFieldsValues");

      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Pedigrees_DamID",
          table: "Pedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Pedigrees_SireID",
          table: "Pedigrees");

      migrationBuilder.DropIndex(
          name: "IX_Pedigrees_DamID",
          table: "Pedigrees");

      migrationBuilder.DropIndex(
          name: "IX_Pedigrees_SireID",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "isDeleted",
          table: "PedigreePageField");

      migrationBuilder.DropColumn(
          name: "FieldValueAlpha",
          table: "PedigreeFieldsValues");

      migrationBuilder.DropColumn(
          name: "isDeleted",
          table: "OwnerPageFields");

      migrationBuilder.DropColumn(
          name: "isDeleted",
          table: "KennelPageFields");

      migrationBuilder.DropColumn(
          name: "isDeleted",
          table: "BreederPageFields");

      migrationBuilder.RenameColumn(
          name: "PedigreeID",
          table: "PedigreeFieldsValues",
          newName: "OwnerID");

      migrationBuilder.RenameColumn(
          name: "PedigreeFieldValueID",
          table: "PedigreeFieldsValues",
          newName: "OwnerFieldValueID");

      migrationBuilder.RenameIndex(
          name: "IX_PedigreeFieldsValues_PedigreeID",
          table: "PedigreeFieldsValues",
          newName: "IX_PedigreeFieldsValues_OwnerID");

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeFieldsValues_Pedigrees_OwnerID",
          table: "PedigreeFieldsValues",
          column: "OwnerID",
          principalTable: "Pedigrees",
          principalColumn: "PedigreeID",
          onDelete: ReferentialAction.Cascade);
    }
  }
}