﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DatabaseConnector.Migrations
{
  public partial class AddedEditMenuNavSettign : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.AddColumn<bool>(
          name: "EditMenuNav",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          defaultValue: false);

      migrationBuilder.AddColumn<Guid>(
          name: "KennelId",
          table: "Pedigrees",
          type: "TEXT",
          nullable: false,
          defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

      migrationBuilder.CreateIndex(
          name: "IX_Pedigrees_KennelId",
          table: "Pedigrees",
          column: "KennelId");

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Kennels_KennelId",
          table: "Pedigrees",
          column: "KennelId",
          principalTable: "Kennels",
          principalColumn: "KennelID",
          onDelete: ReferentialAction.Cascade);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Kennels_KennelId",
          table: "Pedigrees");

      migrationBuilder.DropIndex(
          name: "IX_Pedigrees_KennelId",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "EditMenuNav",
          table: "UserPermissions");

      migrationBuilder.DropColumn(
          name: "KennelId",
          table: "Pedigrees");
    }
  }
}