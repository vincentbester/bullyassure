﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaseConnector.Migrations
{
  public partial class UpdatedPedigreeTable : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.AddColumn<string>(
          name: "Address",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<string>(
          name: "AddressPostal",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<long>(
          name: "BAID",
          table: "Pedigrees",
          type: "INTEGER",
          nullable: false,
          defaultValue: 0L);

      migrationBuilder.AddColumn<string>(
          name: "Country",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<string>(
          name: "PostalCode",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<string>(
          name: "RegID",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropColumn(
          name: "Address",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "AddressPostal",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "BAID",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "Country",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "PostalCode",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "RegID",
          table: "Pedigrees");
    }
  }
}