﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DatabaseConnector.Migrations
{
  public partial class AddedPaymentTable : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_BreederUsers_Breeders_KennelBreederId",
          table: "BreederUsers");

      migrationBuilder.DropIndex(
          name: "IX_PedigreeApplications_PedigreeId",
          table: "PedigreeApplications");

      migrationBuilder.DropIndex(
          name: "IX_BreederUsers_KennelBreederId",
          table: "BreederUsers");

      migrationBuilder.DropColumn(
          name: "KennelBreederId",
          table: "BreederUsers");

      migrationBuilder.CreateTable(
          name: "GlobalSettings",
          columns: table => new
          {
            GlobalSettingID = table.Column<Guid>(type: "TEXT", nullable: false),
            MerchantID = table.Column<string>(type: "TEXT", nullable: true),
            MerchantKey = table.Column<string>(type: "TEXT", nullable: true),
            PaymentFeePercentage = table.Column<long>(type: "INTEGER", nullable: false),
            KennelSubscriptionValue = table.Column<long>(type: "INTEGER", nullable: false),
            BreederSubscriptionValue = table.Column<long>(type: "INTEGER", nullable: false),
            PedigreeRegistrationValue = table.Column<long>(type: "INTEGER", nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_GlobalSettings", x => x.GlobalSettingID);
          });

      migrationBuilder.CreateIndex(
          name: "IX_PedigreeApplications_PedigreeId",
          table: "PedigreeApplications",
          column: "PedigreeId",
          unique: true);

      migrationBuilder.CreateIndex(
          name: "IX_BreederUsers_BreederId",
          table: "BreederUsers",
          column: "BreederId");

      migrationBuilder.AddForeignKey(
          name: "FK_BreederUsers_Breeders_BreederId",
          table: "BreederUsers",
          column: "BreederId",
          principalTable: "Breeders",
          principalColumn: "BreederId",
          onDelete: ReferentialAction.Cascade);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_BreederUsers_Breeders_BreederId",
          table: "BreederUsers");

      migrationBuilder.DropTable(
          name: "GlobalSettings");

      migrationBuilder.DropIndex(
          name: "IX_PedigreeApplications_PedigreeId",
          table: "PedigreeApplications");

      migrationBuilder.DropIndex(
          name: "IX_BreederUsers_BreederId",
          table: "BreederUsers");

      migrationBuilder.AddColumn<Guid>(
          name: "KennelBreederId",
          table: "BreederUsers",
          type: "TEXT",
          nullable: true);

      migrationBuilder.CreateIndex(
          name: "IX_PedigreeApplications_PedigreeId",
          table: "PedigreeApplications",
          column: "PedigreeId");

      migrationBuilder.CreateIndex(
          name: "IX_BreederUsers_KennelBreederId",
          table: "BreederUsers",
          column: "KennelBreederId");

      migrationBuilder.AddForeignKey(
          name: "FK_BreederUsers_Breeders_KennelBreederId",
          table: "BreederUsers",
          column: "KennelBreederId",
          principalTable: "Breeders",
          principalColumn: "BreederId",
          onDelete: ReferentialAction.Restrict);
    }
  }
}