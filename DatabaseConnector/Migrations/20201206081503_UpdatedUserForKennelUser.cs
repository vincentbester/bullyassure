﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaseConnector.Migrations
{
  public partial class UpdatedUserForKennelUser : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.AddColumn<bool>(
          name: "isKennel",
          table: "Users",
          type: "INTEGER",
          nullable: false,
          defaultValue: false);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropColumn(
          name: "isKennel",
          table: "Users");
    }
  }
}