﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaseConnector.Migrations
{
  public partial class UpdatedPedigreeModel : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_OwnerPedigrees_Pedigrees_PedigreeID",
          table: "OwnerPedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeFieldsValues_Pedigrees_PedigreeID",
          table: "PedigreeFieldsValues");

      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Pedigrees_DamID",
          table: "Pedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Pedigrees_SireID",
          table: "Pedigrees");

      migrationBuilder.DropPrimaryKey(
          name: "PK_Pedigrees",
          table: "Pedigrees");

      migrationBuilder.DropIndex(
          name: "IX_Pedigrees_DamID",
          table: "Pedigrees");

      migrationBuilder.DropIndex(
          name: "IX_Pedigrees_SireID",
          table: "Pedigrees");

      migrationBuilder.DropIndex(
          name: "IX_PedigreeFieldsValues_PedigreeID",
          table: "PedigreeFieldsValues");

      migrationBuilder.DropIndex(
          name: "IX_OwnerPedigrees_PedigreeID",
          table: "OwnerPedigrees");

      migrationBuilder.AlterColumn<long>(
          name: "BAID",
          table: "Pedigrees",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(long),
          oldType: "INTEGER")
          .Annotation("Sqlite:Autoincrement", true);

      migrationBuilder.AddColumn<string>(
          name: "AdditionalImage",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<string>(
          name: "BackImage",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<long>(
          name: "DamBAID",
          table: "Pedigrees",
          type: "INTEGER",
          nullable: true);

      migrationBuilder.AddColumn<string>(
          name: "FrontImage",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<string>(
          name: "LeftImage",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<string>(
          name: "RightImage",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<long>(
          name: "SireBAID",
          table: "Pedigrees",
          type: "INTEGER",
          nullable: true);

      migrationBuilder.AddColumn<long>(
          name: "PedigreeBAID",
          table: "PedigreeFieldsValues",
          type: "INTEGER",
          nullable: true);

      migrationBuilder.AddColumn<long>(
          name: "PedigreeBAID",
          table: "OwnerPedigrees",
          type: "INTEGER",
          nullable: true);

      migrationBuilder.AddPrimaryKey(
          name: "PK_Pedigrees",
          table: "Pedigrees",
          column: "BAID");

      migrationBuilder.CreateIndex(
          name: "IX_Pedigrees_DamBAID",
          table: "Pedigrees",
          column: "DamBAID",
          unique: true);

      migrationBuilder.CreateIndex(
          name: "IX_Pedigrees_SireBAID",
          table: "Pedigrees",
          column: "SireBAID",
          unique: true);

      migrationBuilder.CreateIndex(
          name: "IX_PedigreeFieldsValues_PedigreeBAID",
          table: "PedigreeFieldsValues",
          column: "PedigreeBAID");

      migrationBuilder.CreateIndex(
          name: "IX_OwnerPedigrees_PedigreeBAID",
          table: "OwnerPedigrees",
          column: "PedigreeBAID");

      migrationBuilder.AddForeignKey(
          name: "FK_OwnerPedigrees_Pedigrees_PedigreeBAID",
          table: "OwnerPedigrees",
          column: "PedigreeBAID",
          principalTable: "Pedigrees",
          principalColumn: "BAID",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeFieldsValues_Pedigrees_PedigreeBAID",
          table: "PedigreeFieldsValues",
          column: "PedigreeBAID",
          principalTable: "Pedigrees",
          principalColumn: "BAID",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Pedigrees_DamBAID",
          table: "Pedigrees",
          column: "DamBAID",
          principalTable: "Pedigrees",
          principalColumn: "BAID",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Pedigrees_SireBAID",
          table: "Pedigrees",
          column: "SireBAID",
          principalTable: "Pedigrees",
          principalColumn: "BAID",
          onDelete: ReferentialAction.Restrict);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_OwnerPedigrees_Pedigrees_PedigreeBAID",
          table: "OwnerPedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeFieldsValues_Pedigrees_PedigreeBAID",
          table: "PedigreeFieldsValues");

      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Pedigrees_DamBAID",
          table: "Pedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Pedigrees_SireBAID",
          table: "Pedigrees");

      migrationBuilder.DropPrimaryKey(
          name: "PK_Pedigrees",
          table: "Pedigrees");

      migrationBuilder.DropIndex(
          name: "IX_Pedigrees_DamBAID",
          table: "Pedigrees");

      migrationBuilder.DropIndex(
          name: "IX_Pedigrees_SireBAID",
          table: "Pedigrees");

      migrationBuilder.DropIndex(
          name: "IX_PedigreeFieldsValues_PedigreeBAID",
          table: "PedigreeFieldsValues");

      migrationBuilder.DropIndex(
          name: "IX_OwnerPedigrees_PedigreeBAID",
          table: "OwnerPedigrees");

      migrationBuilder.DropColumn(
          name: "AdditionalImage",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "BackImage",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "DamBAID",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "FrontImage",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "LeftImage",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "RightImage",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "SireBAID",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "PedigreeBAID",
          table: "PedigreeFieldsValues");

      migrationBuilder.DropColumn(
          name: "PedigreeBAID",
          table: "OwnerPedigrees");

      migrationBuilder.AlterColumn<long>(
          name: "BAID",
          table: "Pedigrees",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(long),
          oldType: "INTEGER")
          .OldAnnotation("Sqlite:Autoincrement", true);

      migrationBuilder.AddPrimaryKey(
          name: "PK_Pedigrees",
          table: "Pedigrees",
          column: "PedigreeID");

      migrationBuilder.CreateIndex(
          name: "IX_Pedigrees_DamID",
          table: "Pedigrees",
          column: "DamID",
          unique: true);

      migrationBuilder.CreateIndex(
          name: "IX_Pedigrees_SireID",
          table: "Pedigrees",
          column: "SireID",
          unique: true);

      migrationBuilder.CreateIndex(
          name: "IX_PedigreeFieldsValues_PedigreeID",
          table: "PedigreeFieldsValues",
          column: "PedigreeID");

      migrationBuilder.CreateIndex(
          name: "IX_OwnerPedigrees_PedigreeID",
          table: "OwnerPedigrees",
          column: "PedigreeID");

      migrationBuilder.AddForeignKey(
          name: "FK_OwnerPedigrees_Pedigrees_PedigreeID",
          table: "OwnerPedigrees",
          column: "PedigreeID",
          principalTable: "Pedigrees",
          principalColumn: "PedigreeID",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeFieldsValues_Pedigrees_PedigreeID",
          table: "PedigreeFieldsValues",
          column: "PedigreeID",
          principalTable: "Pedigrees",
          principalColumn: "PedigreeID",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Pedigrees_DamID",
          table: "Pedigrees",
          column: "DamID",
          principalTable: "Pedigrees",
          principalColumn: "PedigreeID",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Pedigrees_SireID",
          table: "Pedigrees",
          column: "SireID",
          principalTable: "Pedigrees",
          principalColumn: "PedigreeID",
          onDelete: ReferentialAction.Restrict);
    }
  }
}