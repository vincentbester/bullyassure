﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaseConnector.Migrations
{
  public partial class UpdatedPedigreeOwnerMapping : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Users_CoOwnerId",
          table: "Pedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Users_OwnerId",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "MyProperty",
          table: "Owners");

      migrationBuilder.AddColumn<string>(
          name: "CellNumber",
          table: "Owners",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Owners_CoOwnerId",
          table: "Pedigrees",
          column: "CoOwnerId",
          principalTable: "Owners",
          principalColumn: "OwnerID",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Owners_OwnerId",
          table: "Pedigrees",
          column: "OwnerId",
          principalTable: "Owners",
          principalColumn: "OwnerID",
          onDelete: ReferentialAction.Restrict);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Owners_CoOwnerId",
          table: "Pedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Owners_OwnerId",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "CellNumber",
          table: "Owners");

      migrationBuilder.AddColumn<int>(
          name: "MyProperty",
          table: "Owners",
          type: "INTEGER",
          nullable: false,
          defaultValue: 0);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Users_CoOwnerId",
          table: "Pedigrees",
          column: "CoOwnerId",
          principalTable: "Users",
          principalColumn: "UserID",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Users_OwnerId",
          table: "Pedigrees",
          column: "OwnerId",
          principalTable: "Users",
          principalColumn: "UserID",
          onDelete: ReferentialAction.Restrict);
    }
  }
}