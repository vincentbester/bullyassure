﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DatabaseConnector.Migrations
{
  public partial class UpdatedTables : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_OwnerFieldValues_OwnerFields_OwnerFieldsID",
          table: "OwnerFieldValues");

      migrationBuilder.DropForeignKey(
          name: "FK_OwnerPedigree_Owners_OwnerID",
          table: "OwnerPedigree");

      migrationBuilder.DropForeignKey(
          name: "FK_OwnerPedigree_Pedigrees_PedigreeID",
          table: "OwnerPedigree");

      migrationBuilder.DropTable(
          name: "OwnerFields");

      migrationBuilder.DropPrimaryKey(
          name: "PK_Pedigrees",
          table: "Pedigrees");

      migrationBuilder.DropPrimaryKey(
          name: "PK_OwnerPedigree",
          table: "OwnerPedigree");

      migrationBuilder.DropColumn(
          name: "CandidateID",
          table: "Pedigrees");

      migrationBuilder.RenameTable(
          name: "OwnerPedigree",
          newName: "OwnerPedigrees");

      migrationBuilder.RenameColumn(
          name: "isDeleted",
          table: "Pedigrees",
          newName: "IsDeleted");

      migrationBuilder.RenameIndex(
          name: "IX_OwnerPedigree_PedigreeID",
          table: "OwnerPedigrees",
          newName: "IX_OwnerPedigrees_PedigreeID");

      migrationBuilder.RenameIndex(
          name: "IX_OwnerPedigree_OwnerID",
          table: "OwnerPedigrees",
          newName: "IX_OwnerPedigrees_OwnerID");

      migrationBuilder.AddColumn<Guid>(
          name: "PedigreeID",
          table: "Pedigrees",
          nullable: false,
          defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

      migrationBuilder.AddColumn<Guid>(
          name: "DamID",
          table: "Pedigrees",
          nullable: true);

      migrationBuilder.AddColumn<Guid>(
          name: "SireID",
          table: "Pedigrees",
          nullable: true);

      migrationBuilder.AddColumn<bool>(
          name: "isDeleted",
          table: "Jobs",
          nullable: false,
          defaultValue: false);

      migrationBuilder.AddPrimaryKey(
          name: "PK_Pedigrees",
          table: "Pedigrees",
          column: "PedigreeID");

      migrationBuilder.AddPrimaryKey(
          name: "PK_OwnerPedigrees",
          table: "OwnerPedigrees",
          column: "OwnerPedigreeID");

      migrationBuilder.CreateTable(
          name: "PageFields",
          columns: table => new
          {
            FieldID = table.Column<Guid>(nullable: false),
            FieldTypeID = table.Column<Guid>(nullable: false),
            FieldName = table.Column<string>(nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_PageFields", x => x.FieldID);
            table.ForeignKey(
                      name: "FK_PageFields_FieldTypes_FieldTypeID",
                      column: x => x.FieldTypeID,
                      principalTable: "FieldTypes",
                      principalColumn: "FieldID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "KennelFieldsValues",
          columns: table => new
          {
            KennelFieldValueID = table.Column<Guid>(nullable: false),
            KennelID = table.Column<Guid>(nullable: false),
            FieldValue = table.Column<string>(nullable: true),
            KennelFieldsID = table.Column<Guid>(nullable: false),
            Required = table.Column<bool>(nullable: false),
            isDeleted = table.Column<bool>(nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_KennelFieldsValues", x => x.KennelFieldValueID);
            table.ForeignKey(
                      name: "FK_KennelFieldsValues_PageFields_KennelFieldsID",
                      column: x => x.KennelFieldsID,
                      principalTable: "PageFields",
                      principalColumn: "FieldID",
                      onDelete: ReferentialAction.Cascade);
            table.ForeignKey(
                      name: "FK_KennelFieldsValues_Jobs_KennelID",
                      column: x => x.KennelID,
                      principalTable: "Jobs",
                      principalColumn: "KennelID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "PedigreeFieldsValues",
          columns: table => new
          {
            OwnerFieldValueID = table.Column<Guid>(nullable: false),
            OwnerID = table.Column<Guid>(nullable: false),
            FieldValue = table.Column<string>(nullable: true),
            OwnerFieldsID = table.Column<Guid>(nullable: false),
            Required = table.Column<bool>(nullable: false),
            isDeleted = table.Column<bool>(nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_PedigreeFieldsValues", x => x.OwnerFieldValueID);
            table.ForeignKey(
                      name: "FK_PedigreeFieldsValues_PageFields_OwnerFieldsID",
                      column: x => x.OwnerFieldsID,
                      principalTable: "PageFields",
                      principalColumn: "FieldID",
                      onDelete: ReferentialAction.Cascade);
            table.ForeignKey(
                      name: "FK_PedigreeFieldsValues_Pedigrees_OwnerID",
                      column: x => x.OwnerID,
                      principalTable: "Pedigrees",
                      principalColumn: "PedigreeID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateIndex(
          name: "IX_KennelFieldsValues_KennelFieldsID",
          table: "KennelFieldsValues",
          column: "KennelFieldsID");

      migrationBuilder.CreateIndex(
          name: "IX_KennelFieldsValues_KennelID",
          table: "KennelFieldsValues",
          column: "KennelID");

      migrationBuilder.CreateIndex(
          name: "IX_PageFields_FieldTypeID",
          table: "PageFields",
          column: "FieldTypeID");

      migrationBuilder.CreateIndex(
          name: "IX_PedigreeFieldsValues_OwnerFieldsID",
          table: "PedigreeFieldsValues",
          column: "OwnerFieldsID");

      migrationBuilder.CreateIndex(
          name: "IX_PedigreeFieldsValues_OwnerID",
          table: "PedigreeFieldsValues",
          column: "OwnerID");

      migrationBuilder.AddForeignKey(
          name: "FK_OwnerFieldValues_PageFields_OwnerFieldsID",
          table: "OwnerFieldValues",
          column: "OwnerFieldsID",
          principalTable: "PageFields",
          principalColumn: "FieldID",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_OwnerPedigrees_Owners_OwnerID",
          table: "OwnerPedigrees",
          column: "OwnerID",
          principalTable: "Owners",
          principalColumn: "OwnerID",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_OwnerPedigrees_Pedigrees_PedigreeID",
          table: "OwnerPedigrees",
          column: "PedigreeID",
          principalTable: "Pedigrees",
          principalColumn: "PedigreeID",
          onDelete: ReferentialAction.Cascade);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_OwnerFieldValues_PageFields_OwnerFieldsID",
          table: "OwnerFieldValues");

      migrationBuilder.DropForeignKey(
          name: "FK_OwnerPedigrees_Owners_OwnerID",
          table: "OwnerPedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_OwnerPedigrees_Pedigrees_PedigreeID",
          table: "OwnerPedigrees");

      migrationBuilder.DropTable(
          name: "KennelFieldsValues");

      migrationBuilder.DropTable(
          name: "PedigreeFieldsValues");

      migrationBuilder.DropTable(
          name: "PageFields");

      migrationBuilder.DropPrimaryKey(
          name: "PK_Pedigrees",
          table: "Pedigrees");

      migrationBuilder.DropPrimaryKey(
          name: "PK_OwnerPedigrees",
          table: "OwnerPedigrees");

      migrationBuilder.DropColumn(
          name: "PedigreeID",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "DamID",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "SireID",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "isDeleted",
          table: "Jobs");

      migrationBuilder.RenameTable(
          name: "OwnerPedigrees",
          newName: "OwnerPedigree");

      migrationBuilder.RenameColumn(
          name: "IsDeleted",
          table: "Pedigrees",
          newName: "isDeleted");

      migrationBuilder.RenameIndex(
          name: "IX_OwnerPedigrees_PedigreeID",
          table: "OwnerPedigree",
          newName: "IX_OwnerPedigree_PedigreeID");

      migrationBuilder.RenameIndex(
          name: "IX_OwnerPedigrees_OwnerID",
          table: "OwnerPedigree",
          newName: "IX_OwnerPedigree_OwnerID");

      migrationBuilder.AddColumn<Guid>(
          name: "CandidateID",
          table: "Pedigrees",
          type: "TEXT",
          nullable: false,
          defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

      migrationBuilder.AddPrimaryKey(
          name: "PK_Pedigrees",
          table: "Pedigrees",
          column: "CandidateID");

      migrationBuilder.AddPrimaryKey(
          name: "PK_OwnerPedigree",
          table: "OwnerPedigree",
          column: "OwnerPedigreeID");

      migrationBuilder.CreateTable(
          name: "OwnerFields",
          columns: table => new
          {
            OwnerFieldID = table.Column<Guid>(type: "TEXT", nullable: false),
            FieldName = table.Column<string>(type: "TEXT", nullable: true),
            FieldTypeID = table.Column<Guid>(type: "TEXT", nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_OwnerFields", x => x.OwnerFieldID);
            table.ForeignKey(
                      name: "FK_OwnerFields_FieldTypes_FieldTypeID",
                      column: x => x.FieldTypeID,
                      principalTable: "FieldTypes",
                      principalColumn: "FieldID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateIndex(
          name: "IX_OwnerFields_FieldTypeID",
          table: "OwnerFields",
          column: "FieldTypeID");

      migrationBuilder.AddForeignKey(
          name: "FK_OwnerFieldValues_OwnerFields_OwnerFieldsID",
          table: "OwnerFieldValues",
          column: "OwnerFieldsID",
          principalTable: "OwnerFields",
          principalColumn: "OwnerFieldID",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_OwnerPedigree_Owners_OwnerID",
          table: "OwnerPedigree",
          column: "OwnerID",
          principalTable: "Owners",
          principalColumn: "OwnerID",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_OwnerPedigree_Pedigrees_PedigreeID",
          table: "OwnerPedigree",
          column: "PedigreeID",
          principalTable: "Pedigrees",
          principalColumn: "CandidateID",
          onDelete: ReferentialAction.Cascade);
    }
  }
}