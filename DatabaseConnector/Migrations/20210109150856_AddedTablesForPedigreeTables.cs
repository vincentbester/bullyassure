﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DatabaseConnector.Migrations
{
  public partial class AddedTablesForPedigreeTables : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.CreateTable(
          name: "PedigreeApplications",
          columns: table => new
          {
            PedigreeApplciationId = table.Column<Guid>(type: "TEXT", nullable: false),
            PedigreeId = table.Column<Guid>(type: "TEXT", nullable: false),
            PedigreeBAID = table.Column<long>(type: "INTEGER", nullable: true),
            UserId = table.Column<Guid>(type: "TEXT", nullable: false),
            DateApplied = table.Column<DateTime>(type: "TEXT", nullable: false),
            ApplicationFinalised = table.Column<DateTime>(type: "TEXT", nullable: false),
            Accepted = table.Column<bool>(type: "INTEGER", nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_PedigreeApplications", x => x.PedigreeApplciationId);
            table.ForeignKey(
                      name: "FK_PedigreeApplications_Pedigrees_PedigreeBAID",
                      column: x => x.PedigreeBAID,
                      principalTable: "Pedigrees",
                      principalColumn: "BAID",
                      onDelete: ReferentialAction.Restrict);
            table.ForeignKey(
                      name: "FK_PedigreeApplications_Users_UserId",
                      column: x => x.UserId,
                      principalTable: "Users",
                      principalColumn: "UserID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "PedigreeUsers",
          columns: table => new
          {
            PedigreeUserId = table.Column<Guid>(type: "TEXT", nullable: false),
            PedigreeID = table.Column<Guid>(type: "TEXT", nullable: false),
            PedigreeBAID = table.Column<long>(type: "INTEGER", nullable: true),
            UserId = table.Column<Guid>(type: "TEXT", nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_PedigreeUsers", x => x.PedigreeUserId);
            table.ForeignKey(
                      name: "FK_PedigreeUsers_Pedigrees_PedigreeBAID",
                      column: x => x.PedigreeBAID,
                      principalTable: "Pedigrees",
                      principalColumn: "BAID",
                      onDelete: ReferentialAction.Restrict);
            table.ForeignKey(
                      name: "FK_PedigreeUsers_Users_UserId",
                      column: x => x.UserId,
                      principalTable: "Users",
                      principalColumn: "UserID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "PedigreeApplicationResponses",
          columns: table => new
          {
            ResponseId = table.Column<Guid>(type: "TEXT", nullable: false),
            Message = table.Column<string>(type: "TEXT", nullable: true),
            DateCaptured = table.Column<DateTime>(type: "TEXT", nullable: false),
            PedigreeApplicationPedigreeApplciationId = table.Column<Guid>(type: "TEXT", nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_PedigreeApplicationResponses", x => x.ResponseId);
            table.ForeignKey(
                      name: "FK_PedigreeApplicationResponses_PedigreeApplications_PedigreeApplicationPedigreeApplciationId",
                      column: x => x.PedigreeApplicationPedigreeApplciationId,
                      principalTable: "PedigreeApplications",
                      principalColumn: "PedigreeApplciationId",
                      onDelete: ReferentialAction.Restrict);
          });

      migrationBuilder.CreateTable(
          name: "ResponseDocuments",
          columns: table => new
          {
            ResponseDocId = table.Column<Guid>(type: "TEXT", nullable: false),
            DocumentName = table.Column<string>(type: "TEXT", nullable: true),
            DocumentBinary = table.Column<string>(type: "TEXT", nullable: true),
            DateUploaded = table.Column<DateTime>(type: "TEXT", nullable: false),
            PedigreeApplication_ResponseResponseId = table.Column<Guid>(type: "TEXT", nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_ResponseDocuments", x => x.ResponseDocId);
            table.ForeignKey(
                      name: "FK_ResponseDocuments_PedigreeApplicationResponses_PedigreeApplication_ResponseResponseId",
                      column: x => x.PedigreeApplication_ResponseResponseId,
                      principalTable: "PedigreeApplicationResponses",
                      principalColumn: "ResponseId",
                      onDelete: ReferentialAction.Restrict);
          });

      migrationBuilder.CreateIndex(
          name: "IX_PedigreeApplicationResponses_PedigreeApplicationPedigreeApplciationId",
          table: "PedigreeApplicationResponses",
          column: "PedigreeApplicationPedigreeApplciationId");

      migrationBuilder.CreateIndex(
          name: "IX_PedigreeApplications_PedigreeBAID",
          table: "PedigreeApplications",
          column: "PedigreeBAID");

      migrationBuilder.CreateIndex(
          name: "IX_PedigreeApplications_UserId",
          table: "PedigreeApplications",
          column: "UserId");

      migrationBuilder.CreateIndex(
          name: "IX_PedigreeUsers_PedigreeBAID",
          table: "PedigreeUsers",
          column: "PedigreeBAID");

      migrationBuilder.CreateIndex(
          name: "IX_PedigreeUsers_UserId",
          table: "PedigreeUsers",
          column: "UserId");

      migrationBuilder.CreateIndex(
          name: "IX_ResponseDocuments_PedigreeApplication_ResponseResponseId",
          table: "ResponseDocuments",
          column: "PedigreeApplication_ResponseResponseId");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropTable(
          name: "PedigreeUsers");

      migrationBuilder.DropTable(
          name: "ResponseDocuments");

      migrationBuilder.DropTable(
          name: "PedigreeApplicationResponses");

      migrationBuilder.DropTable(
          name: "PedigreeApplications");
    }
  }
}