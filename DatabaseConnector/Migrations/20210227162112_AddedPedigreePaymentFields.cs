﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DatabaseConnector.Migrations
{
  public partial class AddedPedigreePaymentFields : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.AddColumn<bool>(
          name: "PaymentReceived",
          table: "PedigreeApplications",
          type: "INTEGER",
          nullable: false,
          defaultValue: false);

      migrationBuilder.AddColumn<DateTime>(
          name: "PaymentReceivedDate",
          table: "PedigreeApplications",
          type: "TEXT",
          nullable: false,
          defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropColumn(
          name: "PaymentReceived",
          table: "PedigreeApplications");

      migrationBuilder.DropColumn(
          name: "PaymentReceivedDate",
          table: "PedigreeApplications");
    }
  }
}