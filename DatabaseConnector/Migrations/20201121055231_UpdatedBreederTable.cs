﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaseConnector.Migrations
{
  public partial class UpdatedBreederTable : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.AddColumn<string>(
          name: "BreederFirstName",
          table: "Breeders",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<string>(
          name: "BreederSurname",
          table: "Breeders",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<string>(
          name: "CellNumber",
          table: "Breeders",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<string>(
          name: "EmailAddress",
          table: "Breeders",
          type: "TEXT",
          nullable: true);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropColumn(
          name: "BreederFirstName",
          table: "Breeders");

      migrationBuilder.DropColumn(
          name: "BreederSurname",
          table: "Breeders");

      migrationBuilder.DropColumn(
          name: "CellNumber",
          table: "Breeders");

      migrationBuilder.DropColumn(
          name: "EmailAddress",
          table: "Breeders");
    }
  }
}