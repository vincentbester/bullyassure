﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DatabaseConnector.Migrations
{
  public partial class UpdatedPedigreeApplicationModel : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_OwnerPedigrees_Pedigrees_PedigreeBAID",
          table: "OwnerPedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeApplicationResponses_PedigreeApplications_PedigreeA~",
          table: "PedigreeApplicationResponses");

      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeApplications_Pedigrees_PedigreeBAID",
          table: "PedigreeApplications");

      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeFieldsValues_Pedigrees_PedigreeBAID",
          table: "PedigreeFieldsValues");

      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Breeders_BreederId",
          table: "Pedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Kennels_KennelId",
          table: "Pedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Pedigrees_DamBAID",
          table: "Pedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Pedigrees_SireBAID",
          table: "Pedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeUsers_Pedigrees_PedigreeBAID",
          table: "PedigreeUsers");

      migrationBuilder.DropForeignKey(
          name: "FK_ResponseDocuments_PedigreeApplicationResponses_PedigreeAppl~",
          table: "ResponseDocuments");

      migrationBuilder.DropIndex(
          name: "IX_PedigreeUsers_PedigreeBAID",
          table: "PedigreeUsers");

      migrationBuilder.DropPrimaryKey(
          name: "PK_Pedigrees",
          table: "Pedigrees");

      migrationBuilder.DropIndex(
          name: "IX_Pedigrees_DamBAID",
          table: "Pedigrees");

      migrationBuilder.DropIndex(
          name: "IX_Pedigrees_SireBAID",
          table: "Pedigrees");

      migrationBuilder.DropIndex(
          name: "IX_PedigreeFieldsValues_PedigreeBAID",
          table: "PedigreeFieldsValues");

      migrationBuilder.DropIndex(
          name: "IX_PedigreeApplications_PedigreeBAID",
          table: "PedigreeApplications");

      migrationBuilder.DropIndex(
          name: "IX_OwnerPedigrees_PedigreeBAID",
          table: "OwnerPedigrees");

      migrationBuilder.DropColumn(
          name: "PedigreeBAID",
          table: "PedigreeUsers");

      migrationBuilder.DropColumn(
          name: "DamBAID",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "SireBAID",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "PedigreeBAID",
          table: "PedigreeFieldsValues");

      migrationBuilder.DropColumn(
          name: "ApplicationFinalised",
          table: "PedigreeApplications");

      migrationBuilder.DropColumn(
          name: "PedigreeBAID",
          table: "PedigreeApplications");

      migrationBuilder.DropColumn(
          name: "PedigreeBAID",
          table: "OwnerPedigrees");

      migrationBuilder.RenameIndex(
          name: "IX_PedigreeApplicationResponses_PedigreeApplicationPedigreeApp~",
          table: "PedigreeApplicationResponses",
          newName: "IX_PedigreeApplicationResponses_PedigreeApplicationPedigreeApplciationId");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserID",
          table: "UserSettings",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "SettingID",
          table: "UserSettings",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserSettingPermissionID",
          table: "UserSettings",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserMainID",
          table: "UserSessions",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "Token",
          table: "UserSessions",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "ExpiryDate",
          table: "UserSessions",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<DateTime>(
          name: "EntryDate",
          table: "UserSessions",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<Guid>(
          name: "SessionID",
          table: "UserSessions",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<bool>(
          name: "isOwner",
          table: "Users",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "isKennel",
          table: "Users",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeveloper",
          table: "Users",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "isBreeder",
          table: "Users",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "isAdmin",
          table: "Users",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "Username",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "UserPermissionId",
          table: "Users",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "Surname",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ProfilePicture",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "PostalCode",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Password",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "OrgStructureID",
          table: "Users",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<bool>(
          name: "IsDeleted",
          table: "Users",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "Firstname",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Email",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Country",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Cellnumber",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Address_Postal",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Address",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "UserID",
          table: "Users",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<bool>(
          name: "ViewAllUsers",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "ViewAllPedigrees",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "ViewAllKennels",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "ViewAllBreeders",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "EditUsers",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "EditPermissions",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "EditPedigreeFields",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "EditMenuNav",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "EditKennelFields",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "EditGlobalSettings",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "EditBreederFields",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserPermissionsId",
          table: "UserPermissions",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "WebsiteUrl",
          table: "SystemConfigurations",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "UserName",
          table: "SystemConfigurations",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "SMTPClient",
          table: "SystemConfigurations",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<int>(
          name: "Port",
          table: "SystemConfigurations",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "integer");

      migrationBuilder.AlterColumn<string>(
          name: "Password",
          table: "SystemConfigurations",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ErrorRedirectTo",
          table: "SystemConfigurations",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ErrorRedirectAction",
          table: "SystemConfigurations",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "EmailFromAddress",
          table: "SystemConfigurations",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "ConfigID",
          table: "SystemConfigurations",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "StatCards",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "ValueTopAlg",
          table: "StatCards",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ValueBottomAlg",
          table: "StatCards",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<double>(
          name: "Value",
          table: "StatCards",
          type: "REAL",
          nullable: false,
          oldClrType: typeof(double),
          oldType: "double precision");

      migrationBuilder.AlterColumn<Guid>(
          name: "CreatedbyUserID",
          table: "StatCards",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<DateTime>(
          name: "CreatedDate",
          table: "StatCards",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<string>(
          name: "CardTo",
          table: "StatCards",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CardNavName",
          table: "StatCards",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CardName",
          table: "StatCards",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CardDescription",
          table: "StatCards",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CardAction",
          table: "StatCards",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "CardID",
          table: "StatCards",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "Settings",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "To",
          table: "Settings",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "SettingNavName",
          table: "Settings",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "SettingName",
          table: "Settings",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<int>(
          name: "Order",
          table: "Settings",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "integer");

      migrationBuilder.AlterColumn<string>(
          name: "Action",
          table: "Settings",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "ID",
          table: "Settings",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeApplication_ResponseResponseId",
          table: "ResponseDocuments",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DocumentName",
          table: "ResponseDocuments",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DocumentBinary",
          table: "ResponseDocuments",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "DateUploaded",
          table: "ResponseDocuments",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<Guid>(
          name: "ResponseDocId",
          table: "ResponseDocuments",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "PermissionName",
          table: "PermissionsStructures",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Path",
          table: "PermissionsStructures",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "OrgStructureID",
          table: "PermissionsStructures",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "PermissionID",
          table: "PermissionsStructures",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserId",
          table: "PedigreeUsers",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeID",
          table: "PedigreeUsers",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeUserId",
          table: "PedigreeUsers",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "SireID",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Sex",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "RightImage",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "RegID",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "PostalCode",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeID",
          table: "Pedigrees",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerId",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Name",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "LeftImage",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelId",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<bool>(
          name: "IsDeleted",
          table: "Pedigrees",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "FrontImage",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<bool>(
          name: "Flagged",
          table: "Pedigrees",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "Description",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "DamID",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DOI",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DOB",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Country",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "CoOwnerId",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ChipNo",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederId",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "Breed",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "BackImage",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "AddressPostal",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Address",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "AdditionalImage",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<long>(
          name: "BAID",
          table: "Pedigrees",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(long),
          oldType: "bigint")
          .OldAnnotation("Sqlite:Autoincrement", true);

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "PedigreePageField",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldTypeID",
          table: "PedigreePageField",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "FieldName",
          table: "PedigreePageField",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldID",
          table: "PedigreePageField",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "PedigreeFieldsValues",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "Required",
          table: "PedigreeFieldsValues",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeID",
          table: "PedigreeFieldsValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeFieldsID",
          table: "PedigreeFieldsValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "FieldValueAlpha",
          table: "PedigreeFieldsValues",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldValue",
          table: "PedigreeFieldsValues",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeFieldValueID",
          table: "PedigreeFieldsValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserId",
          table: "PedigreeApplications",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeId",
          table: "PedigreeApplications",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<DateTime>(
          name: "DateApplied",
          table: "PedigreeApplications",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<bool>(
          name: "Accepted",
          table: "PedigreeApplications",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeApplciationId",
          table: "PedigreeApplications",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AddColumn<DateTime>(
          name: "ApplicationDateFinalised",
          table: "PedigreeApplications",
          type: "TEXT",
          nullable: false,
          defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

      migrationBuilder.AddColumn<bool>(
          name: "Closed",
          table: "PedigreeApplications",
          type: "INTEGER",
          nullable: false,
          defaultValue: false);

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeApplicationPedigreeApplciationId",
          table: "PedigreeApplicationResponses",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Message",
          table: "PedigreeApplicationResponses",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "DateCaptured",
          table: "PedigreeApplicationResponses",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<Guid>(
          name: "ResponseId",
          table: "PedigreeApplicationResponses",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AddColumn<Guid>(
          name: "UserID",
          table: "PedigreeApplicationResponses",
          type: "TEXT",
          nullable: false,
          defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "Owners",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "OwnerSurname",
          table: "Owners",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "OwnerName",
          table: "Owners",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "OwnerAddress",
          table: "Owners",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "EmailAddress",
          table: "Owners",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CellNumber",
          table: "Owners",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerID",
          table: "Owners",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeID",
          table: "OwnerPedigrees",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerID",
          table: "OwnerPedigrees",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerPedigreeID",
          table: "OwnerPedigrees",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "OwnerPageFields",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldTypeID",
          table: "OwnerPageFields",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "FieldName",
          table: "OwnerPageFields",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldID",
          table: "OwnerPageFields",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "OwnerFieldValues",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "Required",
          table: "OwnerFieldValues",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerID",
          table: "OwnerFieldValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerFieldsID",
          table: "OwnerFieldValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "FieldValue",
          table: "OwnerFieldValues",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerFieldValueID",
          table: "OwnerFieldValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "StructureName",
          table: "OrgStructures",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "ParentStructureID",
          table: "OrgStructures",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "StructureID",
          table: "OrgStructures",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserID",
          table: "Loggings",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Topath",
          table: "Loggings",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "QueryString",
          table: "Loggings",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ErrorMessage",
          table: "Loggings",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "CaptureDate",
          table: "Loggings",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<Guid>(
          name: "LoggingID",
          table: "Loggings",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserId",
          table: "KennelUsers",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelID",
          table: "KennelUsers",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelUserId",
          table: "KennelUsers",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "Kennels",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "MembershipUpToDate",
          table: "Kennels",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "KennelName",
          table: "Kennels",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "EmailAddress",
          table: "Kennels",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelID",
          table: "Kennels",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "KennelPageFields",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldTypeID",
          table: "KennelPageFields",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "FieldName",
          table: "KennelPageFields",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldID",
          table: "KennelPageFields",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "KennelFieldsValues",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "Required",
          table: "KennelFieldsValues",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelID",
          table: "KennelFieldsValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelFieldsID",
          table: "KennelFieldsValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "FieldValueAlpha",
          table: "KennelFieldsValues",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldValue",
          table: "KennelFieldsValues",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelFieldValueID",
          table: "KennelFieldsValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "FieldTypes",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "FieldTypeName",
          table: "FieldTypes",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldID",
          table: "FieldTypes",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserID",
          table: "EmailCaptures",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "ToEmail",
          table: "EmailCaptures",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<bool>(
          name: "SentSuccessfull",
          table: "EmailCaptures",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "FromEmail",
          table: "EmailCaptures",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "EmailMessage",
          table: "EmailCaptures",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "CaptureDate",
          table: "EmailCaptures",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<Guid>(
          name: "EmailCaptureID",
          table: "EmailCaptures",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "Documents",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<Guid>(
          name: "DocumentID",
          table: "Documents",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserId",
          table: "BreederUsers",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelBreederId",
          table: "BreederUsers",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederId",
          table: "BreederUsers",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederUserId",
          table: "BreederUsers",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "Breeders",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelId",
          table: "Breeders",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "EmailAddress",
          table: "Breeders",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CellNumber",
          table: "Breeders",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "BreederSurname",
          table: "Breeders",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "BreederFirstName",
          table: "Breeders",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederId",
          table: "Breeders",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "BreederPageFields",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldTypeID",
          table: "BreederPageFields",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "FieldName",
          table: "BreederPageFields",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldID",
          table: "BreederPageFields",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "BreederFieldsValues",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<bool>(
          name: "Required",
          table: "BreederFieldsValues",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "FieldValueAlpha",
          table: "BreederFieldsValues",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldValue",
          table: "BreederFieldsValues",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederID",
          table: "BreederFieldsValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederFieldsID",
          table: "BreederFieldsValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederFieldValueID",
          table: "BreederFieldsValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "Username",
          table: "AuditLogins",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Status",
          table: "AuditLogins",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "CaptureDate",
          table: "AuditLogins",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<Guid>(
          name: "AuditID",
          table: "AuditLogins",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AddPrimaryKey(
          name: "PK_Pedigrees",
          table: "Pedigrees",
          column: "PedigreeID");

      migrationBuilder.CreateIndex(
          name: "IX_PedigreeUsers_PedigreeID",
          table: "PedigreeUsers",
          column: "PedigreeID");

      migrationBuilder.CreateIndex(
          name: "IX_Pedigrees_DamID",
          table: "Pedigrees",
          column: "DamID",
          unique: true);

      migrationBuilder.CreateIndex(
          name: "IX_Pedigrees_SireID",
          table: "Pedigrees",
          column: "SireID",
          unique: true);

      migrationBuilder.CreateIndex(
          name: "IX_PedigreeFieldsValues_PedigreeID",
          table: "PedigreeFieldsValues",
          column: "PedigreeID");

      migrationBuilder.CreateIndex(
          name: "IX_PedigreeApplications_PedigreeId",
          table: "PedigreeApplications",
          column: "PedigreeId");

      migrationBuilder.CreateIndex(
          name: "IX_PedigreeApplicationResponses_UserID",
          table: "PedigreeApplicationResponses",
          column: "UserID");

      migrationBuilder.CreateIndex(
          name: "IX_OwnerPedigrees_PedigreeID",
          table: "OwnerPedigrees",
          column: "PedigreeID");

      migrationBuilder.AddForeignKey(
          name: "FK_OwnerPedigrees_Pedigrees_PedigreeID",
          table: "OwnerPedigrees",
          column: "PedigreeID",
          principalTable: "Pedigrees",
          principalColumn: "PedigreeID",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeApplicationResponses_PedigreeApplications_PedigreeApplicationPedigreeApplciationId",
          table: "PedigreeApplicationResponses",
          column: "PedigreeApplicationPedigreeApplciationId",
          principalTable: "PedigreeApplications",
          principalColumn: "PedigreeApplciationId",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeApplicationResponses_Users_UserID",
          table: "PedigreeApplicationResponses",
          column: "UserID",
          principalTable: "Users",
          principalColumn: "UserID",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeApplications_Pedigrees_PedigreeId",
          table: "PedigreeApplications",
          column: "PedigreeId",
          principalTable: "Pedigrees",
          principalColumn: "PedigreeID",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeFieldsValues_Pedigrees_PedigreeID",
          table: "PedigreeFieldsValues",
          column: "PedigreeID",
          principalTable: "Pedigrees",
          principalColumn: "PedigreeID",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Breeders_BreederId",
          table: "Pedigrees",
          column: "BreederId",
          principalTable: "Breeders",
          principalColumn: "BreederId",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Kennels_KennelId",
          table: "Pedigrees",
          column: "KennelId",
          principalTable: "Kennels",
          principalColumn: "KennelID",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Pedigrees_DamID",
          table: "Pedigrees",
          column: "DamID",
          principalTable: "Pedigrees",
          principalColumn: "PedigreeID",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Pedigrees_SireID",
          table: "Pedigrees",
          column: "SireID",
          principalTable: "Pedigrees",
          principalColumn: "PedigreeID",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeUsers_Pedigrees_PedigreeID",
          table: "PedigreeUsers",
          column: "PedigreeID",
          principalTable: "Pedigrees",
          principalColumn: "PedigreeID",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_ResponseDocuments_PedigreeApplicationResponses_PedigreeApplication_ResponseResponseId",
          table: "ResponseDocuments",
          column: "PedigreeApplication_ResponseResponseId",
          principalTable: "PedigreeApplicationResponses",
          principalColumn: "ResponseId",
          onDelete: ReferentialAction.Restrict);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_OwnerPedigrees_Pedigrees_PedigreeID",
          table: "OwnerPedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeApplicationResponses_PedigreeApplications_PedigreeApplicationPedigreeApplciationId",
          table: "PedigreeApplicationResponses");

      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeApplicationResponses_Users_UserID",
          table: "PedigreeApplicationResponses");

      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeApplications_Pedigrees_PedigreeId",
          table: "PedigreeApplications");

      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeFieldsValues_Pedigrees_PedigreeID",
          table: "PedigreeFieldsValues");

      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Breeders_BreederId",
          table: "Pedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Kennels_KennelId",
          table: "Pedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Pedigrees_DamID",
          table: "Pedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Pedigrees_SireID",
          table: "Pedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeUsers_Pedigrees_PedigreeID",
          table: "PedigreeUsers");

      migrationBuilder.DropForeignKey(
          name: "FK_ResponseDocuments_PedigreeApplicationResponses_PedigreeApplication_ResponseResponseId",
          table: "ResponseDocuments");

      migrationBuilder.DropIndex(
          name: "IX_PedigreeUsers_PedigreeID",
          table: "PedigreeUsers");

      migrationBuilder.DropPrimaryKey(
          name: "PK_Pedigrees",
          table: "Pedigrees");

      migrationBuilder.DropIndex(
          name: "IX_Pedigrees_DamID",
          table: "Pedigrees");

      migrationBuilder.DropIndex(
          name: "IX_Pedigrees_SireID",
          table: "Pedigrees");

      migrationBuilder.DropIndex(
          name: "IX_PedigreeFieldsValues_PedigreeID",
          table: "PedigreeFieldsValues");

      migrationBuilder.DropIndex(
          name: "IX_PedigreeApplications_PedigreeId",
          table: "PedigreeApplications");

      migrationBuilder.DropIndex(
          name: "IX_PedigreeApplicationResponses_UserID",
          table: "PedigreeApplicationResponses");

      migrationBuilder.DropIndex(
          name: "IX_OwnerPedigrees_PedigreeID",
          table: "OwnerPedigrees");

      migrationBuilder.DropColumn(
          name: "ApplicationDateFinalised",
          table: "PedigreeApplications");

      migrationBuilder.DropColumn(
          name: "Closed",
          table: "PedigreeApplications");

      migrationBuilder.DropColumn(
          name: "UserID",
          table: "PedigreeApplicationResponses");

      migrationBuilder.RenameIndex(
          name: "IX_PedigreeApplicationResponses_PedigreeApplicationPedigreeApplciationId",
          table: "PedigreeApplicationResponses",
          newName: "IX_PedigreeApplicationResponses_PedigreeApplicationPedigreeApp~");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserID",
          table: "UserSettings",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "SettingID",
          table: "UserSettings",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserSettingPermissionID",
          table: "UserSettings",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserMainID",
          table: "UserSessions",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "Token",
          table: "UserSessions",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "ExpiryDate",
          table: "UserSessions",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<DateTime>(
          name: "EntryDate",
          table: "UserSessions",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "SessionID",
          table: "UserSessions",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isOwner",
          table: "Users",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "isKennel",
          table: "Users",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeveloper",
          table: "Users",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "isBreeder",
          table: "Users",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "isAdmin",
          table: "Users",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "Username",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "UserPermissionId",
          table: "Users",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "Surname",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ProfilePicture",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "PostalCode",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Password",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "OrgStructureID",
          table: "Users",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "IsDeleted",
          table: "Users",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "Firstname",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Email",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Country",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Cellnumber",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Address_Postal",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Address",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "UserID",
          table: "Users",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "ViewAllUsers",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "ViewAllPedigrees",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "ViewAllKennels",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "ViewAllBreeders",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "EditUsers",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "EditPermissions",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "EditPedigreeFields",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "EditMenuNav",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "EditKennelFields",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "EditGlobalSettings",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "EditBreederFields",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserPermissionsId",
          table: "UserPermissions",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "WebsiteUrl",
          table: "SystemConfigurations",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "UserName",
          table: "SystemConfigurations",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "SMTPClient",
          table: "SystemConfigurations",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<int>(
          name: "Port",
          table: "SystemConfigurations",
          type: "integer",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "Password",
          table: "SystemConfigurations",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ErrorRedirectTo",
          table: "SystemConfigurations",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ErrorRedirectAction",
          table: "SystemConfigurations",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "EmailFromAddress",
          table: "SystemConfigurations",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "ConfigID",
          table: "SystemConfigurations",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "StatCards",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "ValueTopAlg",
          table: "StatCards",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ValueBottomAlg",
          table: "StatCards",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<double>(
          name: "Value",
          table: "StatCards",
          type: "double precision",
          nullable: false,
          oldClrType: typeof(double),
          oldType: "REAL");

      migrationBuilder.AlterColumn<Guid>(
          name: "CreatedbyUserID",
          table: "StatCards",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<DateTime>(
          name: "CreatedDate",
          table: "StatCards",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "CardTo",
          table: "StatCards",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CardNavName",
          table: "StatCards",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CardName",
          table: "StatCards",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CardDescription",
          table: "StatCards",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CardAction",
          table: "StatCards",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "CardID",
          table: "StatCards",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "Settings",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "To",
          table: "Settings",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "SettingNavName",
          table: "Settings",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "SettingName",
          table: "Settings",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<int>(
          name: "Order",
          table: "Settings",
          type: "integer",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "Action",
          table: "Settings",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "ID",
          table: "Settings",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeApplication_ResponseResponseId",
          table: "ResponseDocuments",
          type: "uuid",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DocumentName",
          table: "ResponseDocuments",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DocumentBinary",
          table: "ResponseDocuments",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "DateUploaded",
          table: "ResponseDocuments",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "ResponseDocId",
          table: "ResponseDocuments",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "PermissionName",
          table: "PermissionsStructures",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Path",
          table: "PermissionsStructures",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "OrgStructureID",
          table: "PermissionsStructures",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "PermissionID",
          table: "PermissionsStructures",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserId",
          table: "PedigreeUsers",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeID",
          table: "PedigreeUsers",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeUserId",
          table: "PedigreeUsers",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AddColumn<long>(
          name: "PedigreeBAID",
          table: "PedigreeUsers",
          type: "bigint",
          nullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "SireID",
          table: "Pedigrees",
          type: "uuid",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Sex",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "RightImage",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "RegID",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "PostalCode",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerId",
          table: "Pedigrees",
          type: "uuid",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Name",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "LeftImage",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelId",
          table: "Pedigrees",
          type: "uuid",
          nullable: false,
          defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
          oldClrType: typeof(Guid),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<bool>(
          name: "IsDeleted",
          table: "Pedigrees",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "FrontImage",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<bool>(
          name: "Flagged",
          table: "Pedigrees",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "Description",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "DamID",
          table: "Pedigrees",
          type: "uuid",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DOI",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DOB",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Country",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "CoOwnerId",
          table: "Pedigrees",
          type: "uuid",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ChipNo",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederId",
          table: "Pedigrees",
          type: "uuid",
          nullable: false,
          defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
          oldClrType: typeof(Guid),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Breed",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "BackImage",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<long>(
          name: "BAID",
          table: "Pedigrees",
          type: "bigint",
          nullable: false,
          oldClrType: typeof(long),
          oldType: "INTEGER")
          .Annotation("Sqlite:Autoincrement", true);

      migrationBuilder.AlterColumn<string>(
          name: "AddressPostal",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Address",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "AdditionalImage",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeID",
          table: "Pedigrees",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AddColumn<long>(
          name: "DamBAID",
          table: "Pedigrees",
          type: "bigint",
          nullable: true);

      migrationBuilder.AddColumn<long>(
          name: "SireBAID",
          table: "Pedigrees",
          type: "bigint",
          nullable: true);

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "PedigreePageField",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldTypeID",
          table: "PedigreePageField",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "FieldName",
          table: "PedigreePageField",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldID",
          table: "PedigreePageField",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "PedigreeFieldsValues",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "Required",
          table: "PedigreeFieldsValues",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeID",
          table: "PedigreeFieldsValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeFieldsID",
          table: "PedigreeFieldsValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "FieldValueAlpha",
          table: "PedigreeFieldsValues",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldValue",
          table: "PedigreeFieldsValues",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeFieldValueID",
          table: "PedigreeFieldsValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AddColumn<long>(
          name: "PedigreeBAID",
          table: "PedigreeFieldsValues",
          type: "bigint",
          nullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "UserId",
          table: "PedigreeApplications",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeId",
          table: "PedigreeApplications",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<DateTime>(
          name: "DateApplied",
          table: "PedigreeApplications",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "Accepted",
          table: "PedigreeApplications",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeApplciationId",
          table: "PedigreeApplications",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AddColumn<DateTime>(
          name: "ApplicationFinalised",
          table: "PedigreeApplications",
          type: "timestamp without time zone",
          nullable: false,
          defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

      migrationBuilder.AddColumn<long>(
          name: "PedigreeBAID",
          table: "PedigreeApplications",
          type: "bigint",
          nullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeApplicationPedigreeApplciationId",
          table: "PedigreeApplicationResponses",
          type: "uuid",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Message",
          table: "PedigreeApplicationResponses",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "DateCaptured",
          table: "PedigreeApplicationResponses",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "ResponseId",
          table: "PedigreeApplicationResponses",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "Owners",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "OwnerSurname",
          table: "Owners",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "OwnerName",
          table: "Owners",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "OwnerAddress",
          table: "Owners",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "EmailAddress",
          table: "Owners",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CellNumber",
          table: "Owners",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerID",
          table: "Owners",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeID",
          table: "OwnerPedigrees",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerID",
          table: "OwnerPedigrees",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerPedigreeID",
          table: "OwnerPedigrees",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AddColumn<long>(
          name: "PedigreeBAID",
          table: "OwnerPedigrees",
          type: "bigint",
          nullable: true);

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "OwnerPageFields",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldTypeID",
          table: "OwnerPageFields",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "FieldName",
          table: "OwnerPageFields",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldID",
          table: "OwnerPageFields",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "OwnerFieldValues",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "Required",
          table: "OwnerFieldValues",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerID",
          table: "OwnerFieldValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerFieldsID",
          table: "OwnerFieldValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "FieldValue",
          table: "OwnerFieldValues",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerFieldValueID",
          table: "OwnerFieldValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "StructureName",
          table: "OrgStructures",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "ParentStructureID",
          table: "OrgStructures",
          type: "uuid",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "StructureID",
          table: "OrgStructures",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserID",
          table: "Loggings",
          type: "uuid",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Topath",
          table: "Loggings",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "QueryString",
          table: "Loggings",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ErrorMessage",
          table: "Loggings",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "CaptureDate",
          table: "Loggings",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "LoggingID",
          table: "Loggings",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserId",
          table: "KennelUsers",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelID",
          table: "KennelUsers",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelUserId",
          table: "KennelUsers",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "Kennels",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "MembershipUpToDate",
          table: "Kennels",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "KennelName",
          table: "Kennels",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "EmailAddress",
          table: "Kennels",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelID",
          table: "Kennels",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "KennelPageFields",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldTypeID",
          table: "KennelPageFields",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "FieldName",
          table: "KennelPageFields",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldID",
          table: "KennelPageFields",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "KennelFieldsValues",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "Required",
          table: "KennelFieldsValues",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelID",
          table: "KennelFieldsValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelFieldsID",
          table: "KennelFieldsValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "FieldValueAlpha",
          table: "KennelFieldsValues",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldValue",
          table: "KennelFieldsValues",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelFieldValueID",
          table: "KennelFieldsValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "FieldTypes",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "FieldTypeName",
          table: "FieldTypes",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldID",
          table: "FieldTypes",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserID",
          table: "EmailCaptures",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "ToEmail",
          table: "EmailCaptures",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<bool>(
          name: "SentSuccessfull",
          table: "EmailCaptures",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "FromEmail",
          table: "EmailCaptures",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "EmailMessage",
          table: "EmailCaptures",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "CaptureDate",
          table: "EmailCaptures",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "EmailCaptureID",
          table: "EmailCaptures",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "Documents",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "DocumentID",
          table: "Documents",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserId",
          table: "BreederUsers",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelBreederId",
          table: "BreederUsers",
          type: "uuid",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederId",
          table: "BreederUsers",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederUserId",
          table: "BreederUsers",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "Breeders",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelId",
          table: "Breeders",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "EmailAddress",
          table: "Breeders",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CellNumber",
          table: "Breeders",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "BreederSurname",
          table: "Breeders",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "BreederFirstName",
          table: "Breeders",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederId",
          table: "Breeders",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "BreederPageFields",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldTypeID",
          table: "BreederPageFields",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "FieldName",
          table: "BreederPageFields",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldID",
          table: "BreederPageFields",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "BreederFieldsValues",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "Required",
          table: "BreederFieldsValues",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "FieldValueAlpha",
          table: "BreederFieldsValues",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldValue",
          table: "BreederFieldsValues",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederID",
          table: "BreederFieldsValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederFieldsID",
          table: "BreederFieldsValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederFieldValueID",
          table: "BreederFieldsValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "Username",
          table: "AuditLogins",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Status",
          table: "AuditLogins",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "CaptureDate",
          table: "AuditLogins",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "AuditID",
          table: "AuditLogins",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "TEXT");

      migrationBuilder.AddPrimaryKey(
          name: "PK_Pedigrees",
          table: "Pedigrees",
          column: "BAID");

      migrationBuilder.CreateIndex(
          name: "IX_PedigreeUsers_PedigreeBAID",
          table: "PedigreeUsers",
          column: "PedigreeBAID");

      migrationBuilder.CreateIndex(
          name: "IX_Pedigrees_DamBAID",
          table: "Pedigrees",
          column: "DamBAID",
          unique: true);

      migrationBuilder.CreateIndex(
          name: "IX_Pedigrees_SireBAID",
          table: "Pedigrees",
          column: "SireBAID",
          unique: true);

      migrationBuilder.CreateIndex(
          name: "IX_PedigreeFieldsValues_PedigreeBAID",
          table: "PedigreeFieldsValues",
          column: "PedigreeBAID");

      migrationBuilder.CreateIndex(
          name: "IX_PedigreeApplications_PedigreeBAID",
          table: "PedigreeApplications",
          column: "PedigreeBAID");

      migrationBuilder.CreateIndex(
          name: "IX_OwnerPedigrees_PedigreeBAID",
          table: "OwnerPedigrees",
          column: "PedigreeBAID");

      migrationBuilder.AddForeignKey(
          name: "FK_OwnerPedigrees_Pedigrees_PedigreeBAID",
          table: "OwnerPedigrees",
          column: "PedigreeBAID",
          principalTable: "Pedigrees",
          principalColumn: "BAID",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeApplicationResponses_PedigreeApplications_PedigreeA~",
          table: "PedigreeApplicationResponses",
          column: "PedigreeApplicationPedigreeApplciationId",
          principalTable: "PedigreeApplications",
          principalColumn: "PedigreeApplciationId",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeApplications_Pedigrees_PedigreeBAID",
          table: "PedigreeApplications",
          column: "PedigreeBAID",
          principalTable: "Pedigrees",
          principalColumn: "BAID",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeFieldsValues_Pedigrees_PedigreeBAID",
          table: "PedigreeFieldsValues",
          column: "PedigreeBAID",
          principalTable: "Pedigrees",
          principalColumn: "BAID",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Breeders_BreederId",
          table: "Pedigrees",
          column: "BreederId",
          principalTable: "Breeders",
          principalColumn: "BreederId",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Kennels_KennelId",
          table: "Pedigrees",
          column: "KennelId",
          principalTable: "Kennels",
          principalColumn: "KennelID",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Pedigrees_DamBAID",
          table: "Pedigrees",
          column: "DamBAID",
          principalTable: "Pedigrees",
          principalColumn: "BAID",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Pedigrees_SireBAID",
          table: "Pedigrees",
          column: "SireBAID",
          principalTable: "Pedigrees",
          principalColumn: "BAID",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeUsers_Pedigrees_PedigreeBAID",
          table: "PedigreeUsers",
          column: "PedigreeBAID",
          principalTable: "Pedigrees",
          principalColumn: "BAID",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_ResponseDocuments_PedigreeApplicationResponses_PedigreeAppl~",
          table: "ResponseDocuments",
          column: "PedigreeApplication_ResponseResponseId",
          principalTable: "PedigreeApplicationResponses",
          principalColumn: "ResponseId",
          onDelete: ReferentialAction.Restrict);
    }
  }
}