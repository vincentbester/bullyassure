﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaseConnector.Migrations
{
  public partial class UpdatedUserTable : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.AddColumn<bool>(
          name: "isAdmin",
          table: "Users",
          type: "INTEGER",
          nullable: false,
          defaultValue: false);

      migrationBuilder.AddColumn<bool>(
          name: "isBreeder",
          table: "Users",
          type: "INTEGER",
          nullable: false,
          defaultValue: false);

      migrationBuilder.AddColumn<bool>(
          name: "isDeveloper",
          table: "Users",
          type: "INTEGER",
          nullable: false,
          defaultValue: false);

      migrationBuilder.AddColumn<bool>(
          name: "isOwner",
          table: "Users",
          type: "INTEGER",
          nullable: false,
          defaultValue: false);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropColumn(
          name: "isAdmin",
          table: "Users");

      migrationBuilder.DropColumn(
          name: "isBreeder",
          table: "Users");

      migrationBuilder.DropColumn(
          name: "isDeveloper",
          table: "Users");

      migrationBuilder.DropColumn(
          name: "isOwner",
          table: "Users");
    }
  }
}