﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaseConnector.Migrations
{
  public partial class FixedApplicationID : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeApplicationResponses_PedigreeApplications_PedigreeApplicationPedigreeApplciationId",
          table: "PedigreeApplicationResponses");

      migrationBuilder.RenameColumn(
          name: "PedigreeApplciationId",
          table: "PedigreeApplications",
          newName: "PedigreeApplicationId");

      migrationBuilder.RenameColumn(
          name: "PedigreeApplicationPedigreeApplciationId",
          table: "PedigreeApplicationResponses",
          newName: "PedigreeApplicationId");

      migrationBuilder.RenameIndex(
          name: "IX_PedigreeApplicationResponses_PedigreeApplicationPedigreeApplciationId",
          table: "PedigreeApplicationResponses",
          newName: "IX_PedigreeApplicationResponses_PedigreeApplicationId");

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeApplicationResponses_PedigreeApplications_PedigreeApplicationId",
          table: "PedigreeApplicationResponses",
          column: "PedigreeApplicationId",
          principalTable: "PedigreeApplications",
          principalColumn: "PedigreeApplicationId",
          onDelete: ReferentialAction.Restrict);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeApplicationResponses_PedigreeApplications_PedigreeApplicationId",
          table: "PedigreeApplicationResponses");

      migrationBuilder.RenameColumn(
          name: "PedigreeApplicationId",
          table: "PedigreeApplications",
          newName: "PedigreeApplciationId");

      migrationBuilder.RenameColumn(
          name: "PedigreeApplicationId",
          table: "PedigreeApplicationResponses",
          newName: "PedigreeApplicationPedigreeApplciationId");

      migrationBuilder.RenameIndex(
          name: "IX_PedigreeApplicationResponses_PedigreeApplicationId",
          table: "PedigreeApplicationResponses",
          newName: "IX_PedigreeApplicationResponses_PedigreeApplicationPedigreeApplciationId");

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeApplicationResponses_PedigreeApplications_PedigreeApplicationPedigreeApplciationId",
          table: "PedigreeApplicationResponses",
          column: "PedigreeApplicationPedigreeApplciationId",
          principalTable: "PedigreeApplications",
          principalColumn: "PedigreeApplciationId",
          onDelete: ReferentialAction.Restrict);
    }
  }
}