﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DatabaseConnector.Migrations
{
  public partial class AddedUserPermissionTable : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.AddColumn<Guid>(
          name: "UserPermissionId",
          table: "Users",
          type: "TEXT",
          nullable: false,
          defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

      migrationBuilder.CreateTable(
          name: "UserPermissions",
          columns: table => new
          {
            UserPermissionsId = table.Column<Guid>(type: "TEXT", nullable: false),
            EditPermissions = table.Column<bool>(type: "INTEGER", nullable: false),
            EditUsers = table.Column<bool>(type: "INTEGER", nullable: false),
            EditGlobalSettings = table.Column<bool>(type: "INTEGER", nullable: false),
            EditKennelFields = table.Column<bool>(type: "INTEGER", nullable: false),
            EditBreederFields = table.Column<bool>(type: "INTEGER", nullable: false),
            EditPedigreeFields = table.Column<bool>(type: "INTEGER", nullable: false),
            ViewAllKennels = table.Column<bool>(type: "INTEGER", nullable: false),
            ViewAllBreeders = table.Column<bool>(type: "INTEGER", nullable: false),
            ViewAllPedigrees = table.Column<bool>(type: "INTEGER", nullable: false),
            ViewAllUsers = table.Column<bool>(type: "INTEGER", nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_UserPermissions", x => x.UserPermissionsId);
          });

      migrationBuilder.CreateIndex(
          name: "IX_Users_UserPermissionId",
          table: "Users",
          column: "UserPermissionId");

      migrationBuilder.AddForeignKey(
          name: "FK_Users_UserPermissions_UserPermissionId",
          table: "Users",
          column: "UserPermissionId",
          principalTable: "UserPermissions",
          principalColumn: "UserPermissionsId",
          onDelete: ReferentialAction.Cascade);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_Users_UserPermissions_UserPermissionId",
          table: "Users");

      migrationBuilder.DropTable(
          name: "UserPermissions");

      migrationBuilder.DropIndex(
          name: "IX_Users_UserPermissionId",
          table: "Users");

      migrationBuilder.DropColumn(
          name: "UserPermissionId",
          table: "Users");
    }
  }
}