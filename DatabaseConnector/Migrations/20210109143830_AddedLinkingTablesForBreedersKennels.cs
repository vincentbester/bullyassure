﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DatabaseConnector.Migrations
{
  public partial class AddedLinkingTablesForBreedersKennels : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.CreateTable(
          name: "BreederUsers",
          columns: table => new
          {
            BreederUserId = table.Column<Guid>(type: "TEXT", nullable: false),
            BreederId = table.Column<Guid>(type: "TEXT", nullable: false),
            KennelBreederId = table.Column<Guid>(type: "TEXT", nullable: true),
            UserId = table.Column<Guid>(type: "TEXT", nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_BreederUsers", x => x.BreederUserId);
            table.ForeignKey(
                      name: "FK_BreederUsers_Breeders_KennelBreederId",
                      column: x => x.KennelBreederId,
                      principalTable: "Breeders",
                      principalColumn: "BreederId",
                      onDelete: ReferentialAction.Restrict);
            table.ForeignKey(
                      name: "FK_BreederUsers_Users_UserId",
                      column: x => x.UserId,
                      principalTable: "Users",
                      principalColumn: "UserID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "KennelUsers",
          columns: table => new
          {
            KennelUserId = table.Column<Guid>(type: "TEXT", nullable: false),
            KennelID = table.Column<Guid>(type: "TEXT", nullable: false),
            UserId = table.Column<Guid>(type: "TEXT", nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_KennelUsers", x => x.KennelUserId);
            table.ForeignKey(
                      name: "FK_KennelUsers_Kennels_KennelID",
                      column: x => x.KennelID,
                      principalTable: "Kennels",
                      principalColumn: "KennelID",
                      onDelete: ReferentialAction.Cascade);
            table.ForeignKey(
                      name: "FK_KennelUsers_Users_UserId",
                      column: x => x.UserId,
                      principalTable: "Users",
                      principalColumn: "UserID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateIndex(
          name: "IX_BreederUsers_KennelBreederId",
          table: "BreederUsers",
          column: "KennelBreederId");

      migrationBuilder.CreateIndex(
          name: "IX_BreederUsers_UserId",
          table: "BreederUsers",
          column: "UserId");

      migrationBuilder.CreateIndex(
          name: "IX_KennelUsers_KennelID",
          table: "KennelUsers",
          column: "KennelID");

      migrationBuilder.CreateIndex(
          name: "IX_KennelUsers_UserId",
          table: "KennelUsers",
          column: "UserId");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropTable(
          name: "BreederUsers");

      migrationBuilder.DropTable(
          name: "KennelUsers");
    }
  }
}