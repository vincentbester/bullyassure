﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DatabaseConnector.Migrations
{
  public partial class SplitPageFields : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_KennelFieldsValues_PageFields_KennelFieldsID",
          table: "KennelFieldsValues");

      migrationBuilder.DropForeignKey(
          name: "FK_OwnerFieldValues_PageFields_OwnerFieldsID",
          table: "OwnerFieldValues");

      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeFieldsValues_PageFields_OwnerFieldsID",
          table: "PedigreeFieldsValues");

      migrationBuilder.DropTable(
          name: "PageFields");

      migrationBuilder.RenameColumn(
          name: "OwnerFieldsID",
          table: "PedigreeFieldsValues",
          newName: "PedigreeFieldsID");

      migrationBuilder.RenameIndex(
          name: "IX_PedigreeFieldsValues_OwnerFieldsID",
          table: "PedigreeFieldsValues",
          newName: "IX_PedigreeFieldsValues_PedigreeFieldsID");

      migrationBuilder.AddColumn<string>(
          name: "EmailAddress",
          table: "Kennels",
          type: "TEXT",
          nullable: true);

      migrationBuilder.CreateTable(
          name: "KennelPageFields",
          columns: table => new
          {
            FieldID = table.Column<Guid>(type: "TEXT", nullable: false),
            FieldTypeID = table.Column<Guid>(type: "TEXT", nullable: false),
            FieldName = table.Column<string>(type: "TEXT", nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_KennelPageFields", x => x.FieldID);
            table.ForeignKey(
                      name: "FK_KennelPageFields_FieldTypes_FieldTypeID",
                      column: x => x.FieldTypeID,
                      principalTable: "FieldTypes",
                      principalColumn: "FieldID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "OwnerPageFields",
          columns: table => new
          {
            FieldID = table.Column<Guid>(type: "TEXT", nullable: false),
            FieldTypeID = table.Column<Guid>(type: "TEXT", nullable: false),
            FieldName = table.Column<string>(type: "TEXT", nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_OwnerPageFields", x => x.FieldID);
            table.ForeignKey(
                      name: "FK_OwnerPageFields_FieldTypes_FieldTypeID",
                      column: x => x.FieldTypeID,
                      principalTable: "FieldTypes",
                      principalColumn: "FieldID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "PedigreePageField",
          columns: table => new
          {
            FieldID = table.Column<Guid>(type: "TEXT", nullable: false),
            FieldTypeID = table.Column<Guid>(type: "TEXT", nullable: false),
            FieldName = table.Column<string>(type: "TEXT", nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_PedigreePageField", x => x.FieldID);
            table.ForeignKey(
                      name: "FK_PedigreePageField_FieldTypes_FieldTypeID",
                      column: x => x.FieldTypeID,
                      principalTable: "FieldTypes",
                      principalColumn: "FieldID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateIndex(
          name: "IX_KennelPageFields_FieldTypeID",
          table: "KennelPageFields",
          column: "FieldTypeID");

      migrationBuilder.CreateIndex(
          name: "IX_OwnerPageFields_FieldTypeID",
          table: "OwnerPageFields",
          column: "FieldTypeID");

      migrationBuilder.CreateIndex(
          name: "IX_PedigreePageField_FieldTypeID",
          table: "PedigreePageField",
          column: "FieldTypeID");

      migrationBuilder.AddForeignKey(
          name: "FK_KennelFieldsValues_KennelPageFields_KennelFieldsID",
          table: "KennelFieldsValues",
          column: "KennelFieldsID",
          principalTable: "KennelPageFields",
          principalColumn: "FieldID",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_OwnerFieldValues_OwnerPageFields_OwnerFieldsID",
          table: "OwnerFieldValues",
          column: "OwnerFieldsID",
          principalTable: "OwnerPageFields",
          principalColumn: "FieldID",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeFieldsValues_PedigreePageField_PedigreeFieldsID",
          table: "PedigreeFieldsValues",
          column: "PedigreeFieldsID",
          principalTable: "PedigreePageField",
          principalColumn: "FieldID",
          onDelete: ReferentialAction.Cascade);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_KennelFieldsValues_KennelPageFields_KennelFieldsID",
          table: "KennelFieldsValues");

      migrationBuilder.DropForeignKey(
          name: "FK_OwnerFieldValues_OwnerPageFields_OwnerFieldsID",
          table: "OwnerFieldValues");

      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeFieldsValues_PedigreePageField_PedigreeFieldsID",
          table: "PedigreeFieldsValues");

      migrationBuilder.DropTable(
          name: "KennelPageFields");

      migrationBuilder.DropTable(
          name: "OwnerPageFields");

      migrationBuilder.DropTable(
          name: "PedigreePageField");

      migrationBuilder.DropColumn(
          name: "EmailAddress",
          table: "Kennels");

      migrationBuilder.RenameColumn(
          name: "PedigreeFieldsID",
          table: "PedigreeFieldsValues",
          newName: "OwnerFieldsID");

      migrationBuilder.RenameIndex(
          name: "IX_PedigreeFieldsValues_PedigreeFieldsID",
          table: "PedigreeFieldsValues",
          newName: "IX_PedigreeFieldsValues_OwnerFieldsID");

      migrationBuilder.CreateTable(
          name: "PageFields",
          columns: table => new
          {
            FieldID = table.Column<Guid>(type: "TEXT", nullable: false),
            FieldName = table.Column<string>(type: "TEXT", nullable: true),
            FieldTypeID = table.Column<Guid>(type: "TEXT", nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_PageFields", x => x.FieldID);
            table.ForeignKey(
                      name: "FK_PageFields_FieldTypes_FieldTypeID",
                      column: x => x.FieldTypeID,
                      principalTable: "FieldTypes",
                      principalColumn: "FieldID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateIndex(
          name: "IX_PageFields_FieldTypeID",
          table: "PageFields",
          column: "FieldTypeID");

      migrationBuilder.AddForeignKey(
          name: "FK_KennelFieldsValues_PageFields_KennelFieldsID",
          table: "KennelFieldsValues",
          column: "KennelFieldsID",
          principalTable: "PageFields",
          principalColumn: "FieldID",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_OwnerFieldValues_PageFields_OwnerFieldsID",
          table: "OwnerFieldValues",
          column: "OwnerFieldsID",
          principalTable: "PageFields",
          principalColumn: "FieldID",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeFieldsValues_PageFields_OwnerFieldsID",
          table: "PedigreeFieldsValues",
          column: "OwnerFieldsID",
          principalTable: "PageFields",
          principalColumn: "FieldID",
          onDelete: ReferentialAction.Cascade);
    }
  }
}