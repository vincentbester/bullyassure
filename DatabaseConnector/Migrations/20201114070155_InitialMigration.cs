﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DatabaseConnector.Migrations
{
  public partial class InitialMigration : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.CreateTable(
          name: "AuditLogins",
          columns: table => new
          {
            AuditID = table.Column<Guid>(nullable: false),
            Username = table.Column<string>(nullable: true),
            Status = table.Column<string>(nullable: true),
            CaptureDate = table.Column<DateTime>(nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_AuditLogins", x => x.AuditID);
          });

      migrationBuilder.CreateTable(
          name: "Documents",
          columns: table => new
          {
            DocumentID = table.Column<Guid>(nullable: false),
            isDeleted = table.Column<bool>(nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Documents", x => x.DocumentID);
          });

      migrationBuilder.CreateTable(
          name: "FieldTypes",
          columns: table => new
          {
            FieldID = table.Column<Guid>(nullable: false),
            FieldTypeName = table.Column<string>(nullable: true),
            isDeleted = table.Column<bool>(nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_FieldTypes", x => x.FieldID);
          });

      migrationBuilder.CreateTable(
          name: "Jobs",
          columns: table => new
          {
            KennelID = table.Column<Guid>(nullable: false),
            KennelName = table.Column<string>(nullable: true),
            MembershipUpToDate = table.Column<bool>(nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Jobs", x => x.KennelID);
          });

      migrationBuilder.CreateTable(
          name: "Loggings",
          columns: table => new
          {
            LoggingID = table.Column<Guid>(nullable: false),
            UserID = table.Column<Guid>(nullable: true),
            ErrorMessage = table.Column<string>(nullable: true),
            Topath = table.Column<string>(nullable: true),
            QueryString = table.Column<string>(nullable: true),
            CaptureDate = table.Column<DateTime>(nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Loggings", x => x.LoggingID);
          });

      migrationBuilder.CreateTable(
          name: "OrgStructures",
          columns: table => new
          {
            StructureID = table.Column<Guid>(nullable: false),
            StructureName = table.Column<string>(nullable: true),
            ParentStructureID = table.Column<Guid>(nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_OrgStructures", x => x.StructureID);
            table.ForeignKey(
                      name: "FK_OrgStructures_OrgStructures_ParentStructureID",
                      column: x => x.ParentStructureID,
                      principalTable: "OrgStructures",
                      principalColumn: "StructureID",
                      onDelete: ReferentialAction.Restrict);
          });

      migrationBuilder.CreateTable(
          name: "Owners",
          columns: table => new
          {
            OwnerID = table.Column<Guid>(nullable: false),
            OwnerName = table.Column<string>(nullable: true),
            OwnerSurname = table.Column<string>(nullable: true),
            OwnerAddress = table.Column<string>(nullable: true),
            EmailAddress = table.Column<string>(nullable: true),
            MyProperty = table.Column<int>(nullable: false),
            isDeleted = table.Column<bool>(nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Owners", x => x.OwnerID);
          });

      migrationBuilder.CreateTable(
          name: "Pedigrees",
          columns: table => new
          {
            CandidateID = table.Column<Guid>(nullable: false),
            IDNumber = table.Column<string>(nullable: true),
            PassportNumber = table.Column<string>(nullable: true),
            Surname = table.Column<string>(nullable: true),
            Firstname = table.Column<string>(nullable: true),
            MiddleNames = table.Column<string>(nullable: true),
            Email = table.Column<string>(nullable: true),
            CellNumber = table.Column<string>(nullable: true),
            isDeleted = table.Column<bool>(nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Pedigrees", x => x.CandidateID);
          });

      migrationBuilder.CreateTable(
          name: "Settings",
          columns: table => new
          {
            ID = table.Column<Guid>(nullable: false),
            SettingName = table.Column<string>(nullable: true),
            Order = table.Column<int>(nullable: false),
            To = table.Column<string>(nullable: true),
            Action = table.Column<string>(nullable: true),
            SettingNavName = table.Column<string>(nullable: true),
            isDeleted = table.Column<bool>(nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Settings", x => x.ID);
          });

      migrationBuilder.CreateTable(
          name: "SystemConfigurations",
          columns: table => new
          {
            ConfigID = table.Column<Guid>(nullable: false),
            EmailFromAddress = table.Column<string>(nullable: true),
            UserName = table.Column<string>(nullable: true),
            Password = table.Column<string>(nullable: true),
            SMTPClient = table.Column<string>(nullable: true),
            Port = table.Column<int>(nullable: false),
            ErrorRedirectAction = table.Column<string>(nullable: true),
            ErrorRedirectTo = table.Column<string>(nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_SystemConfigurations", x => x.ConfigID);
          });

      migrationBuilder.CreateTable(
          name: "OwnerFields",
          columns: table => new
          {
            OwnerFieldID = table.Column<Guid>(nullable: false),
            FieldTypeID = table.Column<Guid>(nullable: false),
            FieldName = table.Column<string>(nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_OwnerFields", x => x.OwnerFieldID);
            table.ForeignKey(
                      name: "FK_OwnerFields_FieldTypes_FieldTypeID",
                      column: x => x.FieldTypeID,
                      principalTable: "FieldTypes",
                      principalColumn: "FieldID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "PermissionsStructures",
          columns: table => new
          {
            PermissionID = table.Column<Guid>(nullable: false),
            PermissionName = table.Column<string>(nullable: true),
            OrgStructureID = table.Column<Guid>(nullable: false),
            Path = table.Column<string>(nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_PermissionsStructures", x => x.PermissionID);
            table.ForeignKey(
                      name: "FK_PermissionsStructures_OrgStructures_OrgStructureID",
                      column: x => x.OrgStructureID,
                      principalTable: "OrgStructures",
                      principalColumn: "StructureID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "Users",
          columns: table => new
          {
            UserID = table.Column<Guid>(nullable: false),
            Username = table.Column<string>(nullable: true),
            Firstname = table.Column<string>(nullable: true),
            Surname = table.Column<string>(nullable: true),
            Password = table.Column<string>(nullable: true),
            Email = table.Column<string>(nullable: true),
            Cellnumber = table.Column<string>(nullable: true),
            ProfilePicture = table.Column<string>(nullable: true),
            isDeleted = table.Column<bool>(nullable: false),
            OrgStructureID = table.Column<Guid>(nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Users", x => x.UserID);
            table.ForeignKey(
                      name: "FK_Users_OrgStructures_OrgStructureID",
                      column: x => x.OrgStructureID,
                      principalTable: "OrgStructures",
                      principalColumn: "StructureID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "OwnerPedigree",
          columns: table => new
          {
            OwnerPedigreeID = table.Column<Guid>(nullable: false),
            OwnerID = table.Column<Guid>(nullable: false),
            PedigreeID = table.Column<Guid>(nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_OwnerPedigree", x => x.OwnerPedigreeID);
            table.ForeignKey(
                      name: "FK_OwnerPedigree_Owners_OwnerID",
                      column: x => x.OwnerID,
                      principalTable: "Owners",
                      principalColumn: "OwnerID",
                      onDelete: ReferentialAction.Cascade);
            table.ForeignKey(
                      name: "FK_OwnerPedigree_Pedigrees_PedigreeID",
                      column: x => x.PedigreeID,
                      principalTable: "Pedigrees",
                      principalColumn: "CandidateID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "OwnerFieldValues",
          columns: table => new
          {
            OwnerFieldValueID = table.Column<Guid>(nullable: false),
            OwnerID = table.Column<Guid>(nullable: false),
            FieldValue = table.Column<string>(nullable: true),
            OwnerFieldsID = table.Column<Guid>(nullable: false),
            Required = table.Column<bool>(nullable: false),
            isDeleted = table.Column<bool>(nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_OwnerFieldValues", x => x.OwnerFieldValueID);
            table.ForeignKey(
                      name: "FK_OwnerFieldValues_OwnerFields_OwnerFieldsID",
                      column: x => x.OwnerFieldsID,
                      principalTable: "OwnerFields",
                      principalColumn: "OwnerFieldID",
                      onDelete: ReferentialAction.Cascade);
            table.ForeignKey(
                      name: "FK_OwnerFieldValues_Owners_OwnerID",
                      column: x => x.OwnerID,
                      principalTable: "Owners",
                      principalColumn: "OwnerID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "EmailCaptures",
          columns: table => new
          {
            EmailCaptureID = table.Column<Guid>(nullable: false),
            ToEmail = table.Column<string>(nullable: true),
            FromEmail = table.Column<string>(nullable: true),
            EmailMessage = table.Column<string>(nullable: true),
            UserID = table.Column<Guid>(nullable: false),
            CaptureDate = table.Column<DateTime>(nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_EmailCaptures", x => x.EmailCaptureID);
            table.ForeignKey(
                      name: "FK_EmailCaptures_Users_UserID",
                      column: x => x.UserID,
                      principalTable: "Users",
                      principalColumn: "UserID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "StatCards",
          columns: table => new
          {
            CardID = table.Column<Guid>(nullable: false),
            Value = table.Column<double>(nullable: false),
            ValueTopAlg = table.Column<string>(nullable: true),
            ValueBottomAlg = table.Column<string>(nullable: true),
            CardName = table.Column<string>(nullable: true),
            CardDescription = table.Column<string>(nullable: true),
            CardAction = table.Column<string>(nullable: true),
            CardTo = table.Column<string>(nullable: true),
            CardNavName = table.Column<string>(nullable: true),
            CreatedbyUserID = table.Column<Guid>(nullable: false),
            CreatedDate = table.Column<DateTime>(nullable: false),
            isDeleted = table.Column<bool>(nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_StatCards", x => x.CardID);
            table.ForeignKey(
                      name: "FK_StatCards_Users_CreatedbyUserID",
                      column: x => x.CreatedbyUserID,
                      principalTable: "Users",
                      principalColumn: "UserID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "UserSessions",
          columns: table => new
          {
            SessionID = table.Column<Guid>(nullable: false),
            UserMainID = table.Column<Guid>(nullable: false),
            EntryDate = table.Column<DateTime>(nullable: false),
            ExpiryDate = table.Column<DateTime>(nullable: false),
            Token = table.Column<string>(nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_UserSessions", x => x.SessionID);
            table.ForeignKey(
                      name: "FK_UserSessions_Users_UserMainID",
                      column: x => x.UserMainID,
                      principalTable: "Users",
                      principalColumn: "UserID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "UserSettings",
          columns: table => new
          {
            UserSettingPermissionID = table.Column<Guid>(nullable: false),
            UserID = table.Column<Guid>(nullable: false),
            SettingID = table.Column<Guid>(nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_UserSettings", x => x.UserSettingPermissionID);
            table.ForeignKey(
                      name: "FK_UserSettings_Settings_SettingID",
                      column: x => x.SettingID,
                      principalTable: "Settings",
                      principalColumn: "ID",
                      onDelete: ReferentialAction.Cascade);
            table.ForeignKey(
                      name: "FK_UserSettings_Users_UserID",
                      column: x => x.UserID,
                      principalTable: "Users",
                      principalColumn: "UserID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateIndex(
          name: "IX_EmailCaptures_UserID",
          table: "EmailCaptures",
          column: "UserID");

      migrationBuilder.CreateIndex(
          name: "IX_OrgStructures_ParentStructureID",
          table: "OrgStructures",
          column: "ParentStructureID");

      migrationBuilder.CreateIndex(
          name: "IX_OwnerFields_FieldTypeID",
          table: "OwnerFields",
          column: "FieldTypeID");

      migrationBuilder.CreateIndex(
          name: "IX_OwnerFieldValues_OwnerFieldsID",
          table: "OwnerFieldValues",
          column: "OwnerFieldsID");

      migrationBuilder.CreateIndex(
          name: "IX_OwnerFieldValues_OwnerID",
          table: "OwnerFieldValues",
          column: "OwnerID");

      migrationBuilder.CreateIndex(
          name: "IX_OwnerPedigree_OwnerID",
          table: "OwnerPedigree",
          column: "OwnerID");

      migrationBuilder.CreateIndex(
          name: "IX_OwnerPedigree_PedigreeID",
          table: "OwnerPedigree",
          column: "PedigreeID");

      migrationBuilder.CreateIndex(
          name: "IX_PermissionsStructures_OrgStructureID",
          table: "PermissionsStructures",
          column: "OrgStructureID");

      migrationBuilder.CreateIndex(
          name: "IX_StatCards_CreatedbyUserID",
          table: "StatCards",
          column: "CreatedbyUserID");

      migrationBuilder.CreateIndex(
          name: "IX_Users_OrgStructureID",
          table: "Users",
          column: "OrgStructureID");

      migrationBuilder.CreateIndex(
          name: "IX_UserSessions_UserMainID",
          table: "UserSessions",
          column: "UserMainID");

      migrationBuilder.CreateIndex(
          name: "IX_UserSettings_SettingID",
          table: "UserSettings",
          column: "SettingID");

      migrationBuilder.CreateIndex(
          name: "IX_UserSettings_UserID",
          table: "UserSettings",
          column: "UserID");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropTable(
          name: "AuditLogins");

      migrationBuilder.DropTable(
          name: "Documents");

      migrationBuilder.DropTable(
          name: "EmailCaptures");

      migrationBuilder.DropTable(
          name: "Jobs");

      migrationBuilder.DropTable(
          name: "Loggings");

      migrationBuilder.DropTable(
          name: "OwnerFieldValues");

      migrationBuilder.DropTable(
          name: "OwnerPedigree");

      migrationBuilder.DropTable(
          name: "PermissionsStructures");

      migrationBuilder.DropTable(
          name: "StatCards");

      migrationBuilder.DropTable(
          name: "SystemConfigurations");

      migrationBuilder.DropTable(
          name: "UserSessions");

      migrationBuilder.DropTable(
          name: "UserSettings");

      migrationBuilder.DropTable(
          name: "OwnerFields");

      migrationBuilder.DropTable(
          name: "Owners");

      migrationBuilder.DropTable(
          name: "Pedigrees");

      migrationBuilder.DropTable(
          name: "Settings");

      migrationBuilder.DropTable(
          name: "Users");

      migrationBuilder.DropTable(
          name: "FieldTypes");

      migrationBuilder.DropTable(
          name: "OrgStructures");
    }
  }
}