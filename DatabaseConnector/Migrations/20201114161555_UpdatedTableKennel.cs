﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaseConnector.Migrations
{
  public partial class UpdatedTableKennel : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_KennelFieldsValues_Jobs_KennelID",
          table: "KennelFieldsValues");

      migrationBuilder.DropPrimaryKey(
          name: "PK_Jobs",
          table: "Jobs");

      migrationBuilder.RenameTable(
          name: "Jobs",
          newName: "Kennels");

      migrationBuilder.AddPrimaryKey(
          name: "PK_Kennels",
          table: "Kennels",
          column: "KennelID");

      migrationBuilder.AddForeignKey(
          name: "FK_KennelFieldsValues_Kennels_KennelID",
          table: "KennelFieldsValues",
          column: "KennelID",
          principalTable: "Kennels",
          principalColumn: "KennelID",
          onDelete: ReferentialAction.Cascade);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_KennelFieldsValues_Kennels_KennelID",
          table: "KennelFieldsValues");

      migrationBuilder.DropPrimaryKey(
          name: "PK_Kennels",
          table: "Kennels");

      migrationBuilder.RenameTable(
          name: "Kennels",
          newName: "Jobs");

      migrationBuilder.AddPrimaryKey(
          name: "PK_Jobs",
          table: "Jobs",
          column: "KennelID");

      migrationBuilder.AddForeignKey(
          name: "FK_KennelFieldsValues_Jobs_KennelID",
          table: "KennelFieldsValues",
          column: "KennelID",
          principalTable: "Jobs",
          principalColumn: "KennelID",
          onDelete: ReferentialAction.Cascade);
    }
  }
}