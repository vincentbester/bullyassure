﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using System;

namespace DatabaseConnector.Migrations
{
  public partial class UpdatedSystemConfig : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeApplicationResponses_PedigreeApplications_PedigreeApplicationPedigreeApplciationId",
          table: "PedigreeApplicationResponses");

      migrationBuilder.DropForeignKey(
          name: "FK_ResponseDocuments_PedigreeApplicationResponses_PedigreeApplication_ResponseResponseId",
          table: "ResponseDocuments");

      migrationBuilder.RenameIndex(
          name: "IX_PedigreeApplicationResponses_PedigreeApplicationPedigreeApplciationId",
          table: "PedigreeApplicationResponses",
          newName: "IX_PedigreeApplicationResponses_PedigreeApplicationPedigreeApp~");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserID",
          table: "UserSettings",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "SettingID",
          table: "UserSettings",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserSettingPermissionID",
          table: "UserSettings",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserMainID",
          table: "UserSessions",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "Token",
          table: "UserSessions",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "ExpiryDate",
          table: "UserSessions",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<DateTime>(
          name: "EntryDate",
          table: "UserSessions",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "SessionID",
          table: "UserSessions",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isOwner",
          table: "Users",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "isKennel",
          table: "Users",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeveloper",
          table: "Users",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "isBreeder",
          table: "Users",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "isAdmin",
          table: "Users",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "Username",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "UserPermissionId",
          table: "Users",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "Surname",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ProfilePicture",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "PostalCode",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Password",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "OrgStructureID",
          table: "Users",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "IsDeleted",
          table: "Users",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "Firstname",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Email",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Country",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Cellnumber",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Address_Postal",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Address",
          table: "Users",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "UserID",
          table: "Users",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "ViewAllUsers",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "ViewAllPedigrees",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "ViewAllKennels",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "ViewAllBreeders",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "EditUsers",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "EditPermissions",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "EditPedigreeFields",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "EditMenuNav",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "EditKennelFields",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "EditGlobalSettings",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "EditBreederFields",
          table: "UserPermissions",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserPermissionsId",
          table: "UserPermissions",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "UserName",
          table: "SystemConfigurations",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "SMTPClient",
          table: "SystemConfigurations",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<int>(
          name: "Port",
          table: "SystemConfigurations",
          type: "integer",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "Password",
          table: "SystemConfigurations",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ErrorRedirectTo",
          table: "SystemConfigurations",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ErrorRedirectAction",
          table: "SystemConfigurations",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "EmailFromAddress",
          table: "SystemConfigurations",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "ConfigID",
          table: "SystemConfigurations",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AddColumn<string>(
          name: "WebsiteUrl",
          table: "SystemConfigurations",
          type: "text",
          nullable: true);

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "StatCards",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "ValueTopAlg",
          table: "StatCards",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ValueBottomAlg",
          table: "StatCards",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<double>(
          name: "Value",
          table: "StatCards",
          type: "double precision",
          nullable: false,
          oldClrType: typeof(float),
          oldType: "REAL");

      migrationBuilder.AlterColumn<Guid>(
          name: "CreatedbyUserID",
          table: "StatCards",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<DateTime>(
          name: "CreatedDate",
          table: "StatCards",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "CardTo",
          table: "StatCards",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CardNavName",
          table: "StatCards",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CardName",
          table: "StatCards",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CardDescription",
          table: "StatCards",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CardAction",
          table: "StatCards",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "CardID",
          table: "StatCards",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "Settings",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "To",
          table: "Settings",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "SettingNavName",
          table: "Settings",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "SettingName",
          table: "Settings",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<int>(
          name: "Order",
          table: "Settings",
          type: "integer",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "Action",
          table: "Settings",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "ID",
          table: "Settings",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeApplication_ResponseResponseId",
          table: "ResponseDocuments",
          type: "uuid",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DocumentName",
          table: "ResponseDocuments",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DocumentBinary",
          table: "ResponseDocuments",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "DateUploaded",
          table: "ResponseDocuments",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "ResponseDocId",
          table: "ResponseDocuments",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "PermissionName",
          table: "PermissionsStructures",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Path",
          table: "PermissionsStructures",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "OrgStructureID",
          table: "PermissionsStructures",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "PermissionID",
          table: "PermissionsStructures",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserId",
          table: "PedigreeUsers",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeID",
          table: "PedigreeUsers",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<long>(
          name: "PedigreeBAID",
          table: "PedigreeUsers",
          type: "bigint",
          nullable: true,
          oldClrType: typeof(int),
          oldType: "INTEGER",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeUserId",
          table: "PedigreeUsers",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "SireID",
          table: "Pedigrees",
          type: "uuid",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<long>(
          name: "SireBAID",
          table: "Pedigrees",
          type: "bigint",
          nullable: true,
          oldClrType: typeof(int),
          oldType: "INTEGER",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Sex",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "RightImage",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "RegID",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "PostalCode",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeID",
          table: "Pedigrees",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerId",
          table: "Pedigrees",
          type: "uuid",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Name",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "LeftImage",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelId",
          table: "Pedigrees",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "IsDeleted",
          table: "Pedigrees",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "FrontImage",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<bool>(
          name: "Flagged",
          table: "Pedigrees",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "Description",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "DamID",
          table: "Pedigrees",
          type: "uuid",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<long>(
          name: "DamBAID",
          table: "Pedigrees",
          type: "bigint",
          nullable: true,
          oldClrType: typeof(int),
          oldType: "INTEGER",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DOI",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DOB",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Country",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "CoOwnerId",
          table: "Pedigrees",
          type: "uuid",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ChipNo",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederId",
          table: "Pedigrees",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "Breed",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "BackImage",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "AddressPostal",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Address",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "AdditionalImage",
          table: "Pedigrees",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<long>(
          name: "BAID",
          table: "Pedigrees",
          type: "bigint",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER")
          .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "PedigreePageField",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldTypeID",
          table: "PedigreePageField",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "FieldName",
          table: "PedigreePageField",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldID",
          table: "PedigreePageField",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "PedigreeFieldsValues",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "Required",
          table: "PedigreeFieldsValues",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeID",
          table: "PedigreeFieldsValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeFieldsID",
          table: "PedigreeFieldsValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<long>(
          name: "PedigreeBAID",
          table: "PedigreeFieldsValues",
          type: "bigint",
          nullable: true,
          oldClrType: typeof(int),
          oldType: "INTEGER",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldValueAlpha",
          table: "PedigreeFieldsValues",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldValue",
          table: "PedigreeFieldsValues",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeFieldValueID",
          table: "PedigreeFieldsValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserId",
          table: "PedigreeApplications",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeId",
          table: "PedigreeApplications",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<long>(
          name: "PedigreeBAID",
          table: "PedigreeApplications",
          type: "bigint",
          nullable: true,
          oldClrType: typeof(int),
          oldType: "INTEGER",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "DateApplied",
          table: "PedigreeApplications",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<DateTime>(
          name: "ApplicationFinalised",
          table: "PedigreeApplications",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "Accepted",
          table: "PedigreeApplications",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeApplciationId",
          table: "PedigreeApplications",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeApplicationPedigreeApplciationId",
          table: "PedigreeApplicationResponses",
          type: "uuid",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Message",
          table: "PedigreeApplicationResponses",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "DateCaptured",
          table: "PedigreeApplicationResponses",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "ResponseId",
          table: "PedigreeApplicationResponses",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "Owners",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "OwnerSurname",
          table: "Owners",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "OwnerName",
          table: "Owners",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "OwnerAddress",
          table: "Owners",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "EmailAddress",
          table: "Owners",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CellNumber",
          table: "Owners",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerID",
          table: "Owners",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "PedigreeID",
          table: "OwnerPedigrees",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<long>(
          name: "PedigreeBAID",
          table: "OwnerPedigrees",
          type: "bigint",
          nullable: true,
          oldClrType: typeof(int),
          oldType: "INTEGER",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerID",
          table: "OwnerPedigrees",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerPedigreeID",
          table: "OwnerPedigrees",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "OwnerPageFields",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldTypeID",
          table: "OwnerPageFields",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "FieldName",
          table: "OwnerPageFields",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldID",
          table: "OwnerPageFields",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "OwnerFieldValues",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "Required",
          table: "OwnerFieldValues",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerID",
          table: "OwnerFieldValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerFieldsID",
          table: "OwnerFieldValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "FieldValue",
          table: "OwnerFieldValues",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "OwnerFieldValueID",
          table: "OwnerFieldValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "StructureName",
          table: "OrgStructures",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "ParentStructureID",
          table: "OrgStructures",
          type: "uuid",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "StructureID",
          table: "OrgStructures",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserID",
          table: "Loggings",
          type: "uuid",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Topath",
          table: "Loggings",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "QueryString",
          table: "Loggings",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ErrorMessage",
          table: "Loggings",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "CaptureDate",
          table: "Loggings",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "LoggingID",
          table: "Loggings",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserId",
          table: "KennelUsers",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelID",
          table: "KennelUsers",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelUserId",
          table: "KennelUsers",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "Kennels",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "MembershipUpToDate",
          table: "Kennels",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "KennelName",
          table: "Kennels",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "EmailAddress",
          table: "Kennels",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelID",
          table: "Kennels",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "KennelPageFields",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldTypeID",
          table: "KennelPageFields",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "FieldName",
          table: "KennelPageFields",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldID",
          table: "KennelPageFields",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "KennelFieldsValues",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "Required",
          table: "KennelFieldsValues",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelID",
          table: "KennelFieldsValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelFieldsID",
          table: "KennelFieldsValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "FieldValueAlpha",
          table: "KennelFieldsValues",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldValue",
          table: "KennelFieldsValues",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelFieldValueID",
          table: "KennelFieldsValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "FieldTypes",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "FieldTypeName",
          table: "FieldTypes",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldID",
          table: "FieldTypes",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserID",
          table: "EmailCaptures",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "ToEmail",
          table: "EmailCaptures",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<bool>(
          name: "SentSuccessfull",
          table: "EmailCaptures",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "FromEmail",
          table: "EmailCaptures",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "EmailMessage",
          table: "EmailCaptures",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "CaptureDate",
          table: "EmailCaptures",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "EmailCaptureID",
          table: "EmailCaptures",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "Documents",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "DocumentID",
          table: "Documents",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "UserId",
          table: "BreederUsers",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelBreederId",
          table: "BreederUsers",
          type: "uuid",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederId",
          table: "BreederUsers",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederUserId",
          table: "BreederUsers",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "Breeders",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "KennelId",
          table: "Breeders",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "EmailAddress",
          table: "Breeders",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CellNumber",
          table: "Breeders",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "BreederSurname",
          table: "Breeders",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "BreederFirstName",
          table: "Breeders",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederId",
          table: "Breeders",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "BreederPageFields",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldTypeID",
          table: "BreederPageFields",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "FieldName",
          table: "BreederPageFields",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "FieldID",
          table: "BreederPageFields",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<bool>(
          name: "isDeleted",
          table: "BreederFieldsValues",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<bool>(
          name: "Required",
          table: "BreederFieldsValues",
          type: "boolean",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "INTEGER");

      migrationBuilder.AlterColumn<string>(
          name: "FieldValueAlpha",
          table: "BreederFieldsValues",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldValue",
          table: "BreederFieldsValues",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederID",
          table: "BreederFieldsValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederFieldsID",
          table: "BreederFieldsValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "BreederFieldValueID",
          table: "BreederFieldsValues",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<string>(
          name: "Username",
          table: "AuditLogins",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Status",
          table: "AuditLogins",
          type: "text",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "TEXT",
          oldNullable: true);

      migrationBuilder.AlterColumn<DateTime>(
          name: "CaptureDate",
          table: "AuditLogins",
          type: "timestamp without time zone",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AlterColumn<Guid>(
          name: "AuditID",
          table: "AuditLogins",
          type: "uuid",
          nullable: false,
          oldClrType: typeof(string),
          oldType: "TEXT");

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeApplicationResponses_PedigreeApplications_PedigreeA~",
          table: "PedigreeApplicationResponses",
          column: "PedigreeApplicationPedigreeApplciationId",
          principalTable: "PedigreeApplications",
          principalColumn: "PedigreeApplciationId",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_ResponseDocuments_PedigreeApplicationResponses_PedigreeAppl~",
          table: "ResponseDocuments",
          column: "PedigreeApplication_ResponseResponseId",
          principalTable: "PedigreeApplicationResponses",
          principalColumn: "ResponseId",
          onDelete: ReferentialAction.Restrict);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_PedigreeApplicationResponses_PedigreeApplications_PedigreeA~",
          table: "PedigreeApplicationResponses");

      migrationBuilder.DropForeignKey(
          name: "FK_ResponseDocuments_PedigreeApplicationResponses_PedigreeAppl~",
          table: "ResponseDocuments");

      migrationBuilder.DropColumn(
          name: "WebsiteUrl",
          table: "SystemConfigurations");

      migrationBuilder.RenameIndex(
          name: "IX_PedigreeApplicationResponses_PedigreeApplicationPedigreeApp~",
          table: "PedigreeApplicationResponses",
          newName: "IX_PedigreeApplicationResponses_PedigreeApplicationPedigreeApplciationId");

      migrationBuilder.AlterColumn<string>(
          name: "UserID",
          table: "UserSettings",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "SettingID",
          table: "UserSettings",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "UserSettingPermissionID",
          table: "UserSettings",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "UserMainID",
          table: "UserSessions",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "Token",
          table: "UserSessions",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ExpiryDate",
          table: "UserSessions",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<string>(
          name: "EntryDate",
          table: "UserSessions",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<string>(
          name: "SessionID",
          table: "UserSessions",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "isOwner",
          table: "Users",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "isKennel",
          table: "Users",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "isDeveloper",
          table: "Users",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "isBreeder",
          table: "Users",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "isAdmin",
          table: "Users",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "Username",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "UserPermissionId",
          table: "Users",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "Surname",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ProfilePicture",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "PostalCode",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Password",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "OrgStructureID",
          table: "Users",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "IsDeleted",
          table: "Users",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "Firstname",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Email",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Country",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Cellnumber",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Address_Postal",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Address",
          table: "Users",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "UserID",
          table: "Users",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "ViewAllUsers",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "ViewAllPedigrees",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "ViewAllKennels",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "ViewAllBreeders",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "EditUsers",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "EditPermissions",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "EditPedigreeFields",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "EditMenuNav",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "EditKennelFields",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "EditGlobalSettings",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "EditBreederFields",
          table: "UserPermissions",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "UserPermissionsId",
          table: "UserPermissions",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "UserName",
          table: "SystemConfigurations",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "SMTPClient",
          table: "SystemConfigurations",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<int>(
          name: "Port",
          table: "SystemConfigurations",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "integer");

      migrationBuilder.AlterColumn<string>(
          name: "Password",
          table: "SystemConfigurations",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ErrorRedirectTo",
          table: "SystemConfigurations",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ErrorRedirectAction",
          table: "SystemConfigurations",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "EmailFromAddress",
          table: "SystemConfigurations",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ConfigID",
          table: "SystemConfigurations",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "isDeleted",
          table: "StatCards",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "ValueTopAlg",
          table: "StatCards",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ValueBottomAlg",
          table: "StatCards",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<float>(
          name: "Value",
          table: "StatCards",
          type: "REAL",
          nullable: false,
          oldClrType: typeof(double),
          oldType: "double precision");

      migrationBuilder.AlterColumn<string>(
          name: "CreatedbyUserID",
          table: "StatCards",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "CreatedDate",
          table: "StatCards",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<string>(
          name: "CardTo",
          table: "StatCards",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CardNavName",
          table: "StatCards",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CardName",
          table: "StatCards",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CardDescription",
          table: "StatCards",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CardAction",
          table: "StatCards",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CardID",
          table: "StatCards",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "isDeleted",
          table: "Settings",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "To",
          table: "Settings",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "SettingNavName",
          table: "Settings",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "SettingName",
          table: "Settings",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<int>(
          name: "Order",
          table: "Settings",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(int),
          oldType: "integer");

      migrationBuilder.AlterColumn<string>(
          name: "Action",
          table: "Settings",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ID",
          table: "Settings",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "PedigreeApplication_ResponseResponseId",
          table: "ResponseDocuments",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DocumentName",
          table: "ResponseDocuments",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DocumentBinary",
          table: "ResponseDocuments",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DateUploaded",
          table: "ResponseDocuments",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<string>(
          name: "ResponseDocId",
          table: "ResponseDocuments",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "PermissionName",
          table: "PermissionsStructures",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Path",
          table: "PermissionsStructures",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "OrgStructureID",
          table: "PermissionsStructures",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "PermissionID",
          table: "PermissionsStructures",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "UserId",
          table: "PedigreeUsers",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "PedigreeID",
          table: "PedigreeUsers",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "PedigreeBAID",
          table: "PedigreeUsers",
          type: "INTEGER",
          nullable: true,
          oldClrType: typeof(long),
          oldType: "bigint",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "PedigreeUserId",
          table: "PedigreeUsers",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "SireID",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid",
          oldNullable: true);

      migrationBuilder.AlterColumn<int>(
          name: "SireBAID",
          table: "Pedigrees",
          type: "INTEGER",
          nullable: true,
          oldClrType: typeof(long),
          oldType: "bigint",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Sex",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "RightImage",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "RegID",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "PostalCode",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "PedigreeID",
          table: "Pedigrees",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "OwnerId",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Name",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "LeftImage",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "KennelId",
          table: "Pedigrees",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "IsDeleted",
          table: "Pedigrees",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "FrontImage",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<int>(
          name: "Flagged",
          table: "Pedigrees",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "Description",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DamID",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid",
          oldNullable: true);

      migrationBuilder.AlterColumn<int>(
          name: "DamBAID",
          table: "Pedigrees",
          type: "INTEGER",
          nullable: true,
          oldClrType: typeof(long),
          oldType: "bigint",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DOI",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DOB",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Country",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CoOwnerId",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ChipNo",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "BreederId",
          table: "Pedigrees",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "Breed",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "BackImage",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "AddressPostal",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Address",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "AdditionalImage",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<int>(
          name: "BAID",
          table: "Pedigrees",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(long),
          oldType: "bigint")
          .OldAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

      migrationBuilder.AlterColumn<int>(
          name: "isDeleted",
          table: "PedigreePageField",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "FieldTypeID",
          table: "PedigreePageField",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "FieldName",
          table: "PedigreePageField",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldID",
          table: "PedigreePageField",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "isDeleted",
          table: "PedigreeFieldsValues",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "Required",
          table: "PedigreeFieldsValues",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "PedigreeID",
          table: "PedigreeFieldsValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "PedigreeFieldsID",
          table: "PedigreeFieldsValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "PedigreeBAID",
          table: "PedigreeFieldsValues",
          type: "INTEGER",
          nullable: true,
          oldClrType: typeof(long),
          oldType: "bigint",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldValueAlpha",
          table: "PedigreeFieldsValues",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldValue",
          table: "PedigreeFieldsValues",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "PedigreeFieldValueID",
          table: "PedigreeFieldsValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "UserId",
          table: "PedigreeApplications",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "PedigreeId",
          table: "PedigreeApplications",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "PedigreeBAID",
          table: "PedigreeApplications",
          type: "INTEGER",
          nullable: true,
          oldClrType: typeof(long),
          oldType: "bigint",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DateApplied",
          table: "PedigreeApplications",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<string>(
          name: "ApplicationFinalised",
          table: "PedigreeApplications",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<int>(
          name: "Accepted",
          table: "PedigreeApplications",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "PedigreeApplciationId",
          table: "PedigreeApplications",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "PedigreeApplicationPedigreeApplciationId",
          table: "PedigreeApplicationResponses",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Message",
          table: "PedigreeApplicationResponses",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "DateCaptured",
          table: "PedigreeApplicationResponses",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<string>(
          name: "ResponseId",
          table: "PedigreeApplicationResponses",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "isDeleted",
          table: "Owners",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "OwnerSurname",
          table: "Owners",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "OwnerName",
          table: "Owners",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "OwnerAddress",
          table: "Owners",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "EmailAddress",
          table: "Owners",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CellNumber",
          table: "Owners",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "OwnerID",
          table: "Owners",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "PedigreeID",
          table: "OwnerPedigrees",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "PedigreeBAID",
          table: "OwnerPedigrees",
          type: "INTEGER",
          nullable: true,
          oldClrType: typeof(long),
          oldType: "bigint",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "OwnerID",
          table: "OwnerPedigrees",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "OwnerPedigreeID",
          table: "OwnerPedigrees",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "isDeleted",
          table: "OwnerPageFields",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "FieldTypeID",
          table: "OwnerPageFields",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "FieldName",
          table: "OwnerPageFields",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldID",
          table: "OwnerPageFields",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "isDeleted",
          table: "OwnerFieldValues",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "Required",
          table: "OwnerFieldValues",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "OwnerID",
          table: "OwnerFieldValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "OwnerFieldsID",
          table: "OwnerFieldValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "FieldValue",
          table: "OwnerFieldValues",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "OwnerFieldValueID",
          table: "OwnerFieldValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "StructureName",
          table: "OrgStructures",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ParentStructureID",
          table: "OrgStructures",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "StructureID",
          table: "OrgStructures",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "UserID",
          table: "Loggings",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Topath",
          table: "Loggings",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "QueryString",
          table: "Loggings",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "ErrorMessage",
          table: "Loggings",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CaptureDate",
          table: "Loggings",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<string>(
          name: "LoggingID",
          table: "Loggings",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "UserId",
          table: "KennelUsers",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "KennelID",
          table: "KennelUsers",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "KennelUserId",
          table: "KennelUsers",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "isDeleted",
          table: "Kennels",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "MembershipUpToDate",
          table: "Kennels",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "KennelName",
          table: "Kennels",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "EmailAddress",
          table: "Kennels",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "KennelID",
          table: "Kennels",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "isDeleted",
          table: "KennelPageFields",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "FieldTypeID",
          table: "KennelPageFields",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "FieldName",
          table: "KennelPageFields",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldID",
          table: "KennelPageFields",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "isDeleted",
          table: "KennelFieldsValues",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "Required",
          table: "KennelFieldsValues",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "KennelID",
          table: "KennelFieldsValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "KennelFieldsID",
          table: "KennelFieldsValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "FieldValueAlpha",
          table: "KennelFieldsValues",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldValue",
          table: "KennelFieldsValues",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "KennelFieldValueID",
          table: "KennelFieldsValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "isDeleted",
          table: "FieldTypes",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "FieldTypeName",
          table: "FieldTypes",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldID",
          table: "FieldTypes",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "UserID",
          table: "EmailCaptures",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "ToEmail",
          table: "EmailCaptures",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<int>(
          name: "SentSuccessfull",
          table: "EmailCaptures",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "FromEmail",
          table: "EmailCaptures",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "EmailMessage",
          table: "EmailCaptures",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CaptureDate",
          table: "EmailCaptures",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<string>(
          name: "EmailCaptureID",
          table: "EmailCaptures",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "isDeleted",
          table: "Documents",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "DocumentID",
          table: "Documents",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "UserId",
          table: "BreederUsers",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "KennelBreederId",
          table: "BreederUsers",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(Guid),
          oldType: "uuid",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "BreederId",
          table: "BreederUsers",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "BreederUserId",
          table: "BreederUsers",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "isDeleted",
          table: "Breeders",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "KennelId",
          table: "Breeders",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "EmailAddress",
          table: "Breeders",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CellNumber",
          table: "Breeders",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "BreederSurname",
          table: "Breeders",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "BreederFirstName",
          table: "Breeders",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "BreederId",
          table: "Breeders",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "isDeleted",
          table: "BreederPageFields",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "FieldTypeID",
          table: "BreederPageFields",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "FieldName",
          table: "BreederPageFields",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldID",
          table: "BreederPageFields",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<int>(
          name: "isDeleted",
          table: "BreederFieldsValues",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<int>(
          name: "Required",
          table: "BreederFieldsValues",
          type: "INTEGER",
          nullable: false,
          oldClrType: typeof(bool),
          oldType: "boolean");

      migrationBuilder.AlterColumn<string>(
          name: "FieldValueAlpha",
          table: "BreederFieldsValues",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "FieldValue",
          table: "BreederFieldsValues",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "BreederID",
          table: "BreederFieldsValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "BreederFieldsID",
          table: "BreederFieldsValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "BreederFieldValueID",
          table: "BreederFieldsValues",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AlterColumn<string>(
          name: "Username",
          table: "AuditLogins",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "Status",
          table: "AuditLogins",
          type: "TEXT",
          nullable: true,
          oldClrType: typeof(string),
          oldType: "text",
          oldNullable: true);

      migrationBuilder.AlterColumn<string>(
          name: "CaptureDate",
          table: "AuditLogins",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(DateTime),
          oldType: "timestamp without time zone");

      migrationBuilder.AlterColumn<string>(
          name: "AuditID",
          table: "AuditLogins",
          type: "TEXT",
          nullable: false,
          oldClrType: typeof(Guid),
          oldType: "uuid");

      migrationBuilder.AddForeignKey(
          name: "FK_PedigreeApplicationResponses_PedigreeApplications_PedigreeApplicationPedigreeApplciationId",
          table: "PedigreeApplicationResponses",
          column: "PedigreeApplicationPedigreeApplciationId",
          principalTable: "PedigreeApplications",
          principalColumn: "PedigreeApplciationId",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_ResponseDocuments_PedigreeApplicationResponses_PedigreeApplication_ResponseResponseId",
          table: "ResponseDocuments",
          column: "PedigreeApplication_ResponseResponseId",
          principalTable: "PedigreeApplicationResponses",
          principalColumn: "ResponseId",
          onDelete: ReferentialAction.Restrict);
    }
  }
}