﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DatabaseConnector.Migrations
{
  public partial class UpdatedTablesExtraFields : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.RenameColumn(
          name: "Surname",
          table: "Pedigrees",
          newName: "Sex");

      migrationBuilder.RenameColumn(
          name: "PassportNumber",
          table: "Pedigrees",
          newName: "OwnerId");

      migrationBuilder.RenameColumn(
          name: "MiddleNames",
          table: "Pedigrees",
          newName: "Name");

      migrationBuilder.RenameColumn(
          name: "IDNumber",
          table: "Pedigrees",
          newName: "Description");

      migrationBuilder.RenameColumn(
          name: "Firstname",
          table: "Pedigrees",
          newName: "DOI");

      migrationBuilder.RenameColumn(
          name: "Email",
          table: "Pedigrees",
          newName: "DOB");

      migrationBuilder.RenameColumn(
          name: "CellNumber",
          table: "Pedigrees",
          newName: "CoOwnerId");

      migrationBuilder.AddColumn<string>(
          name: "Address",
          table: "Users",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<string>(
          name: "Address_Postal",
          table: "Users",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<string>(
          name: "Country",
          table: "Users",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<bool>(
          name: "IsDeleted",
          table: "Users",
          type: "INTEGER",
          nullable: false,
          defaultValue: false);

      migrationBuilder.AddColumn<string>(
          name: "PostalCode",
          table: "Users",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<string>(
          name: "Breed",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<Guid>(
          name: "BreederId",
          table: "Pedigrees",
          type: "TEXT",
          nullable: false,
          defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

      migrationBuilder.AddColumn<string>(
          name: "ChipNo",
          table: "Pedigrees",
          type: "TEXT",
          nullable: true);

      migrationBuilder.AddColumn<bool>(
          name: "Flagged",
          table: "Pedigrees",
          type: "INTEGER",
          nullable: false,
          defaultValue: false);

      migrationBuilder.AddColumn<bool>(
          name: "isDeleted",
          table: "Breeders",
          type: "INTEGER",
          nullable: false,
          defaultValue: false);

      migrationBuilder.CreateIndex(
          name: "IX_Pedigrees_BreederId",
          table: "Pedigrees",
          column: "BreederId");

      migrationBuilder.CreateIndex(
          name: "IX_Pedigrees_CoOwnerId",
          table: "Pedigrees",
          column: "CoOwnerId");

      migrationBuilder.CreateIndex(
          name: "IX_Pedigrees_OwnerId",
          table: "Pedigrees",
          column: "OwnerId");

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Breeders_BreederId",
          table: "Pedigrees",
          column: "BreederId",
          principalTable: "Breeders",
          principalColumn: "BreederId",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Users_CoOwnerId",
          table: "Pedigrees",
          column: "CoOwnerId",
          principalTable: "Users",
          principalColumn: "UserID",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_Pedigrees_Users_OwnerId",
          table: "Pedigrees",
          column: "OwnerId",
          principalTable: "Users",
          principalColumn: "UserID",
          onDelete: ReferentialAction.Restrict);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Breeders_BreederId",
          table: "Pedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Users_CoOwnerId",
          table: "Pedigrees");

      migrationBuilder.DropForeignKey(
          name: "FK_Pedigrees_Users_OwnerId",
          table: "Pedigrees");

      migrationBuilder.DropIndex(
          name: "IX_Pedigrees_BreederId",
          table: "Pedigrees");

      migrationBuilder.DropIndex(
          name: "IX_Pedigrees_CoOwnerId",
          table: "Pedigrees");

      migrationBuilder.DropIndex(
          name: "IX_Pedigrees_OwnerId",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "Address",
          table: "Users");

      migrationBuilder.DropColumn(
          name: "Address_Postal",
          table: "Users");

      migrationBuilder.DropColumn(
          name: "Country",
          table: "Users");

      migrationBuilder.DropColumn(
          name: "IsDeleted",
          table: "Users");

      migrationBuilder.DropColumn(
          name: "PostalCode",
          table: "Users");

      migrationBuilder.DropColumn(
          name: "Breed",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "BreederId",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "ChipNo",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "Flagged",
          table: "Pedigrees");

      migrationBuilder.DropColumn(
          name: "isDeleted",
          table: "Breeders");

      migrationBuilder.RenameColumn(
          name: "Sex",
          table: "Pedigrees",
          newName: "Surname");

      migrationBuilder.RenameColumn(
          name: "OwnerId",
          table: "Pedigrees",
          newName: "PassportNumber");

      migrationBuilder.RenameColumn(
          name: "Name",
          table: "Pedigrees",
          newName: "MiddleNames");

      migrationBuilder.RenameColumn(
          name: "Description",
          table: "Pedigrees",
          newName: "IDNumber");

      migrationBuilder.RenameColumn(
          name: "DOI",
          table: "Pedigrees",
          newName: "Firstname");

      migrationBuilder.RenameColumn(
          name: "DOB",
          table: "Pedigrees",
          newName: "Email");

      migrationBuilder.RenameColumn(
          name: "CoOwnerId",
          table: "Pedigrees",
          newName: "CellNumber");
    }
  }
}