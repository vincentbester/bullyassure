﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DatabaseConnector.Migrations
{
  public partial class AddBreederTable : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.CreateTable(
          name: "BreederPageFields",
          columns: table => new
          {
            FieldID = table.Column<Guid>(type: "TEXT", nullable: false),
            FieldTypeID = table.Column<Guid>(type: "TEXT", nullable: false),
            FieldName = table.Column<string>(type: "TEXT", nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_BreederPageFields", x => x.FieldID);
            table.ForeignKey(
                      name: "FK_BreederPageFields_FieldTypes_FieldTypeID",
                      column: x => x.FieldTypeID,
                      principalTable: "FieldTypes",
                      principalColumn: "FieldID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "Breeders",
          columns: table => new
          {
            BreederId = table.Column<Guid>(type: "TEXT", nullable: false),
            KennelId = table.Column<Guid>(type: "TEXT", nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Breeders", x => x.BreederId);
            table.ForeignKey(
                      name: "FK_Breeders_Kennels_KennelId",
                      column: x => x.KennelId,
                      principalTable: "Kennels",
                      principalColumn: "KennelID",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "BreederFieldsValues",
          columns: table => new
          {
            BreederFieldValueID = table.Column<Guid>(type: "TEXT", nullable: false),
            BreederID = table.Column<Guid>(type: "TEXT", nullable: false),
            FieldValue = table.Column<string>(type: "TEXT", nullable: true),
            FieldValueAlpha = table.Column<string>(type: "TEXT", nullable: true),
            BreederFieldsID = table.Column<Guid>(type: "TEXT", nullable: false),
            Required = table.Column<bool>(type: "INTEGER", nullable: false),
            isDeleted = table.Column<bool>(type: "INTEGER", nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_BreederFieldsValues", x => x.BreederFieldValueID);
            table.ForeignKey(
                      name: "FK_BreederFieldsValues_BreederPageFields_BreederFieldsID",
                      column: x => x.BreederFieldsID,
                      principalTable: "BreederPageFields",
                      principalColumn: "FieldID",
                      onDelete: ReferentialAction.Cascade);
            table.ForeignKey(
                      name: "FK_BreederFieldsValues_Breeders_BreederID",
                      column: x => x.BreederID,
                      principalTable: "Breeders",
                      principalColumn: "BreederId",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateIndex(
          name: "IX_BreederFieldsValues_BreederFieldsID",
          table: "BreederFieldsValues",
          column: "BreederFieldsID");

      migrationBuilder.CreateIndex(
          name: "IX_BreederFieldsValues_BreederID",
          table: "BreederFieldsValues",
          column: "BreederID");

      migrationBuilder.CreateIndex(
          name: "IX_BreederPageFields_FieldTypeID",
          table: "BreederPageFields",
          column: "FieldTypeID");

      migrationBuilder.CreateIndex(
          name: "IX_Breeders_KennelId",
          table: "Breeders",
          column: "KennelId");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropTable(
          name: "BreederFieldsValues");

      migrationBuilder.DropTable(
          name: "BreederPageFields");

      migrationBuilder.DropTable(
          name: "Breeders");
    }
  }
}