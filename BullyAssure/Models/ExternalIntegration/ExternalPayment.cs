﻿namespace BullyAssure.Models.ExternalIntegration
{
  public class ExternalPayment
  {
    public string paymenthostURL { get; set; }
    public string merchant_id { get; set; }
    public string merchant_key { get; set; }
    public string returnUrl { get; set; }
    public string cancelUrl { get; set; }
    public string notifyUrl { get; set; }
    public string name_first { get; set; }
    public string email_address { get; set; }
    public string cell_number { get; set; }
    public string email_confirmation { get; set; }
    public string confirmation_address { get; set; }
  }
}