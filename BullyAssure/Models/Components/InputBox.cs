﻿using System;

namespace BullyAssure.Models.Components
{
  public class InputBox
  {
    public Guid ID { get; set; }
    public string Name { get; set; }
    public string Type { get; set; }
    public string Placeholder { get; set; }
    public string Value { get; set; }
  }
}