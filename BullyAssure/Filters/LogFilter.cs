﻿using ApplicationWorkers.Logging;
using DatabaseConnector;
using System;

namespace BullyAssure.Filters
{
  public class LogFilter
  {
    public static void Write(BullyAssureContext _db, string QueryString, string path, string UserID, Exception ex = null)
    {
      new LoggingHandler(_db).LogError(ex, UserID, path, QueryString);
    }
  }
}