﻿using DatabaseConnector;
using DatabaseConnector.Tables.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace BullyAssure.Filters
{
  public class ControllerActionFilter : ActionFilterAttribute
  {
    public BullyAssureContext db { get; set; }

    private async Task<bool> AuthenticateUser(string userId, string token, string path)
    {
      if (String.IsNullOrEmpty(token) || String.IsNullOrEmpty(userId)) return false;

      string PATHNAME = path.Split("/")[1].ToString();
      return await db.UserSessions.Include(e => e.UserMain).ThenInclude(f => f.OrgStructure)
          .AnyAsync(e => e.Token == token
              && e.UserMainID == Guid.Parse(userId)
              && e.ExpiryDate > DateTime.Now
              && db.PermissionsStructures.Any(h => h.OrgStructureID == e.UserMain.OrgStructureID
                    && h.Path == PATHNAME));
    }

    public override void OnActionExecuting(ActionExecutingContext context)
    {
      db = (BullyAssureContext)context.HttpContext.RequestServices.GetService(typeof(BullyAssureContext));

      //new LoggingHandler(db).AuditTrail(JsonSerializer.Serialize(context.HttpContext.Request.Form), context.HttpContext.Session.GetString("UserID"), context.HttpContext.Request.Path, context.HttpContext.Request.QueryString.ToString());

      if (!AuthenticateUser(context.HttpContext.Session.GetString("UserID"), context.HttpContext.Session.GetString("token"), context.HttpContext.Request.Path).Result)
      {
        if (!String.IsNullOrEmpty(context.HttpContext.Session.GetString("UserID")))
          LogFilter.Write(db, context.HttpContext.Request.QueryString.ToString(), context.HttpContext.Request.Path, context.HttpContext.Session.GetString("UserID"));
        var controller = (dynamic)context.Controller;
        if (!String.IsNullOrEmpty(context.HttpContext.Session.GetString("SYSTEMCONFIGURATION")))
        {
          context.Result = controller.RedirectToAction(
                  JsonSerializer.Deserialize<SystemConfiguration>(context.HttpContext.Session.GetString("SYSTEMCONFIGURATION")).ErrorRedirectAction ?? "LoginPage",
                  JsonSerializer.Deserialize<SystemConfiguration>(context.HttpContext.Session.GetString("SYSTEMCONFIGURATION")).ErrorRedirectTo ?? "Login"
              );
        }
        else
        {
          context.Result = controller.RedirectToAction("LoginPage", "Login");
        }
      }
    }

    public override void OnActionExecuted(ActionExecutedContext context)
    {
    }

    //public override void OnResultExecuting(ResultExecutingContext context)
    //{
    //  if (Startup.ConvertToPDF)
    //  {
    //    Startup.PDFStream = new MemoryStream();
    //    context.HttpContext.Response.Filter = new PDFStreamFilter(Startup.PDFStream);
    //  }
    //}

    //public override void OnResultExecuted(ResultExecutedContext context)
    //{
    //  if (Startup.ConvertToPDF)
    //  {
    //    context.HttpContext.Response.Clear();
    //    Startup.PDFStream.Seek(0, SeekOrigin.Begin);
    //    Stream byteStream = _PrintService.PrintToPDF(Startup.PDFStream);
    //    StreamReader reader = new StreamReader(byteStream);
    //    context.HttpContext.Response.AddHeader("content-disposition",
    //           "attachment; filename=report.pdf");
    //    context.HttpContext.Response.AddHeader("content-type",
    //           "application/pdf");
    //    context.HttpContext.Response.Write(reader.ReadToEnd());
    //  }
    //}
  }
}