﻿using ApplicationWorkers.Communications;
using BullyAssure.Filters;
using DatabaseConnector;
using DatabaseConnector.Tables.Breeders;
using DatabaseConnector.Tables.Configuration;
using DatabaseConnector.Tables.Kennels;
using DatabaseConnector.Tables.Owners;
using DatabaseConnector.Tables.Pedigrees;
using DatabaseConnector.Tables.Permissions;
using DatabaseConnector.Tables.Settings;
using DatabaseConnector.Tables.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace BullyAssure.Controllers
{
  public class SettingsController : Controller
  {
    private BullyAssureContext db;
    private IConfiguration config;
    private readonly EmailHandler emailHandler;

    public SettingsController(BullyAssureContext _db, IConfiguration _config, EmailHandler _emailHandler)
    {
      this.db = _db;
      this.config = _config;
      this.emailHandler = _emailHandler;
    }

    [ControllerActionFilter]
    [HttpGet]
    public IActionResult Index()
    {
      try
      {
        var UserID = HttpContext.Session.GetString("UserID");
        var user = this.db.Users.Include(f => f.UserPermission).FirstOrDefault(e => e.UserID == Guid.Parse(UserID));
        ViewData["User"] = user;

        return View("Index");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpGet]
    public IActionResult Create()
    {
      try
      {
        return View("Create", new Setting());
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public IActionResult SaveNewSetting(Setting SettingDetails)
    {
      try
      {
        db.Settings.Add(SettingDetails);
        db.SaveChanges();

        db.PermissionsStructures.Add(new PermissionsStructure
        {
          OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureID == (db.Users.FirstOrDefault(u => u.UserID == Guid.Parse(HttpContext.Session.GetString("UserID"))).OrgStructureID)).StructureID,
          Path = SettingDetails.To,
          PermissionName = $"{SettingDetails.SettingNavName} View"
        });

        db.SaveChanges();

        try
        {
          emailHandler.SendEmail(
                ToAddresses: new List<string> { "vbester3@gmail.com" },
                Subject: $"Bully Assure: New Setting",
                Body: $"Good Day <br />" +
                $"<br /> " +
                $"<br /> {db.Users.FirstOrDefault(u => u.UserID == Guid.Parse(HttpContext.Session.GetString("UserID"))).Firstname} added new Setting - {SettingDetails.SettingNavName} View" +
                $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
                $"<br /> " +
                $"<br />" +
                $"<br /> Kind Regards," +
               $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
                AttachmentsBinaries: null,
                UserID: HttpContext.Session.GetString("UserID"),
                systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
                );
        }
        catch (Exception)
        {
        }

        return RedirectToAction("Index", "Settings");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpGet]
    public IActionResult Delete(Guid SettingID)
    {
      try
      {
        var UserID = HttpContext.Session.GetString("UserID");

        if (!db.Users.Include(g => g.UserPermission).Any(f => f.UserID == Guid.Parse(UserID) && f.UserPermission.EditMenuNav))
        {
          return RedirectToAction("Index", "Settings");
        }
        var Settings = db.Settings.FirstOrDefault(e => e.ID == SettingID);
        Settings.isDeleted = true;
        db.Settings.Update(Settings);
        db.SaveChanges();

        try
        {
          emailHandler.SendEmail(
                ToAddresses: new List<string> { "vbester3@gmail.com" },
                Subject: $"Bully Assure: Deleted Setting",
                Body: $"Good Day <br />" +
                $"<br /> " +
                $"<br /> {db.Users.FirstOrDefault(u => u.UserID == Guid.Parse(HttpContext.Session.GetString("UserID"))).Firstname} deleted Setting - {Settings.SettingNavName} View" +
                $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
                $"<br /> " +
                $"<br />" +
                $"<br /> Kind Regards," +
               $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
                AttachmentsBinaries: null,
                UserID: HttpContext.Session.GetString("UserID"),
                systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
                );
        }
        catch (Exception)
        {
        }

        return RedirectToAction("Index", "Settings");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public async Task<IActionResult> SaveSystemSettings(SystemConfiguration systemConfiguration)
    {
      try
      {
        if (systemConfiguration != null)
        {
          this.db.SystemConfigurations.Update(systemConfiguration);
          await this.db.SaveChangesAsync();
        }

        try
        {
          emailHandler.SendEmail(
                ToAddresses: new List<string> { "vbester3@gmail.com" },
                Subject: $"Bully Assure: Updated System Settings",
                Body: $"Good Day <br />" +
                $"<br /> " +
                $"<br /> {db.Users.FirstOrDefault(u => u.UserID == Guid.Parse(HttpContext.Session.GetString("UserID"))).Firstname} updated System Settings" +
                $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
                $"<br /> " +
                $"<br />" +
                $"<br /> Kind Regards," +
               $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
                AttachmentsBinaries: null,
                UserID: HttpContext.Session.GetString("UserID"),
                systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
                );
        }
        catch (Exception)
        {
        }

        return RedirectToAction("Index", "Settings");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    #region Kennel Fields

    [ControllerActionFilter]
    [HttpGet]
    public IActionResult CreateKennelField()
    {
      try
      {
        return View("CreateKennelField", new KennelPageField());
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public IActionResult SaveKennelField(KennelPageField kennelPageField)
    {
      try
      {
        if (kennelPageField != null)
        {
          db.KennelPageFields.Update(kennelPageField);
          db.SaveChanges();
        }

        try
        {
          emailHandler.SendEmail(
                ToAddresses: new List<string> { "vbester3@gmail.com" },
                Subject: $"Bully Assure: New Kennel Field",
                Body: $"Good Day <br />" +
                $"<br /> " +
                $"<br /> {db.Users.FirstOrDefault(u => u.UserID == Guid.Parse(HttpContext.Session.GetString("UserID"))).Firstname} added new kennel field - {kennelPageField.FieldName} View" +
                $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
                $"<br /> " +
                $"<br />" +
                $"<br /> Kind Regards," +
               $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
                AttachmentsBinaries: null,
                UserID: HttpContext.Session.GetString("UserID"),
                systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
                );
        }
        catch (Exception)
        {
        }

        return RedirectToAction("Index", "Settings");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public IActionResult DeleteKennelField(string FieldID)
    {
      _ = !String.IsNullOrEmpty(FieldID) ? FieldID : throw new NullReferenceException();
      try
      {
        KennelPageField KennelPageField = this.db.KennelPageFields.FirstOrDefault(e => e.FieldID == Guid.Parse(FieldID));
        if (KennelPageField == null)
        {
          LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), new Exception($"Could not find Kennel Field with specified ID ({FieldID})"));
          return View("Error", new Exception($"Could not find Kennel Field with specified ID ({FieldID})"));
        }
        KennelPageField.isDeleted = true;
        db.KennelPageFields.Update(KennelPageField);

        db.SaveChanges();

        try
        {
          emailHandler.SendEmail(
                ToAddresses: new List<string> { "vbester3@gmail.com" },
                Subject: $"Bully Assure: Deleted Kennel Field",
                Body: $"Good Day <br />" +
                $"<br /> " +
                $"<br /> {db.Users.FirstOrDefault(u => u.UserID == Guid.Parse(HttpContext.Session.GetString("UserID"))).Firstname} deleted kennel field - {KennelPageField.FieldName} View" +
                $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
                $"<br /> " +
                $"<br />" +
                $"<br /> Kind Regards," +
               $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
                AttachmentsBinaries: null,
                UserID: HttpContext.Session.GetString("UserID"),
                systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
                );
        }
        catch (Exception)
        {
        }

        return RedirectToAction("Index", "Settings");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    #endregion Kennel Fields

    #region Breeder Fields

    [ControllerActionFilter]
    [HttpGet]
    public IActionResult CreateBreederField()
    {
      try
      {
        return View("CreateBreederField", new BreederPageField());
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public IActionResult SaveBreederField(BreederPageField breederPageField)
    {
      try
      {
        if (breederPageField != null)
        {
          db.BreederPageFields.Update(breederPageField);
          db.SaveChanges();
        }

        try
        {
          emailHandler.SendEmail(
                ToAddresses: new List<string> { "vbester3@gmail.com" },
                Subject: $"Bully Assure: New Breeder Field",
                Body: $"Good Day <br />" +
                $"<br /> " +
                $"<br /> {db.Users.FirstOrDefault(u => u.UserID == Guid.Parse(HttpContext.Session.GetString("UserID"))).Firstname} added new breeder field - {breederPageField.FieldName} View" +
                $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
                $"<br /> " +
                $"<br />" +
                $"<br /> Kind Regards," +
               $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
                AttachmentsBinaries: null,
                UserID: HttpContext.Session.GetString("UserID"),
                systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
                );
        }
        catch (Exception)
        {
        }

        return RedirectToAction("Index", "Settings");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public IActionResult DeleteBreederField(string FieldID)
    {
      _ = !String.IsNullOrEmpty(FieldID) ? FieldID : throw new NullReferenceException();
      try
      {
        BreederPageField BreederPageField = this.db.BreederPageFields.FirstOrDefault(e => e.FieldID == Guid.Parse(FieldID));
        if (BreederPageField == null)
        {
          LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), new Exception($"Could not find Breeder Field with specified ID ({FieldID})"));
          return View("Error", new Exception($"Could not find Breeder Field with specified ID ({FieldID})"));
        }
        BreederPageField.isDeleted = true;
        db.BreederPageFields.Update(BreederPageField);

        db.SaveChanges();

        try
        {
          emailHandler.SendEmail(
                ToAddresses: new List<string> { "vbester3@gmail.com" },
                Subject: $"Bully Assure: Deleted Breeder Field",
                Body: $"Good Day <br />" +
                $"<br /> " +
                $"<br /> {db.Users.FirstOrDefault(u => u.UserID == Guid.Parse(HttpContext.Session.GetString("UserID"))).Firstname} deleted breeder field - {BreederPageField.FieldName} View" +
                $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
                $"<br /> " +
                $"<br />" +
                $"<br /> Kind Regards," +
               $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
                AttachmentsBinaries: null,
                UserID: HttpContext.Session.GetString("UserID"),
                systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
                );
        }
        catch (Exception)
        {
        }

        return RedirectToAction("Index", "Settings");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    #endregion Breeder Fields

    #region Owner Fields

    [ControllerActionFilter]
    [HttpGet]
    public IActionResult CreateOwnerField()
    {
      try
      {
        return View("CreateOwnerField", new OwnerPageField());
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public IActionResult SaveOwnerField(OwnerPageField ownerPageField)
    {
      try
      {
        if (ownerPageField != null)
        {
          db.OwnerPageFields.Update(ownerPageField);
          db.SaveChanges();
        }
        return RedirectToAction("Index", "Settings");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public IActionResult DeleteOwnerField(string FieldID)
    {
      _ = !String.IsNullOrEmpty(FieldID) ? FieldID : throw new NullReferenceException();
      try
      {
        OwnerPageField OwnerPageField = this.db.OwnerPageFields.FirstOrDefault(e => e.FieldID == Guid.Parse(FieldID));
        if (OwnerPageField == null)
        {
          LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), new Exception($"Could not find Owner Field with specified ID ({FieldID})"));
          return View("Error", new Exception($"Could not find Owner Field with specified ID ({FieldID})"));
        }
        OwnerPageField.isDeleted = true;
        db.OwnerPageFields.Update(OwnerPageField);

        db.SaveChanges();
        return RedirectToAction("Index", "Settings");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    #endregion Owner Fields

    #region Pedigree Fields

    [ControllerActionFilter]
    [HttpGet]
    public IActionResult CreatePedigreeField()
    {
      try
      {
        return View("CreatePedigreeField", new PedigreePageField());
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public IActionResult SavePedigreeField(PedigreePageField pedigreePageField)
    {
      try
      {
        if (pedigreePageField != null)
        {
          db.PedigreePageField.Update(pedigreePageField);
          db.SaveChanges();
        }

        try
        {
          emailHandler.SendEmail(
                ToAddresses: new List<string> { "vbester3@gmail.com" },
                Subject: $"Bully Assure: New Pedigree Field",
                Body: $"Good Day <br />" +
                $"<br /> " +
                $"<br /> {db.Users.FirstOrDefault(u => u.UserID == Guid.Parse(HttpContext.Session.GetString("UserID"))).Firstname} new pedigree field - {pedigreePageField.FieldName} View" +
                $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
                $"<br /> " +
                $"<br />" +
                $"<br /> Kind Regards," +
               $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
                AttachmentsBinaries: null,
                UserID: HttpContext.Session.GetString("UserID"),
                systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
                );
        }
        catch (Exception)
        {
        }

        return RedirectToAction("Index", "Settings");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public IActionResult DeletePedigreeField(string FieldID)
    {
      _ = !String.IsNullOrEmpty(FieldID) ? FieldID : throw new NullReferenceException();
      try
      {
        PedigreePageField pedigreePageField = this.db.PedigreePageField.FirstOrDefault(e => e.FieldID == Guid.Parse(FieldID));
        if (pedigreePageField == null)
        {
          LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), new Exception($"Could not find Pedigree Field with specified ID ({FieldID})"));
          return View("Error", new Exception($"Could not find Pedigree Field with specified ID ({FieldID})"));
        }
        pedigreePageField.isDeleted = true;
        db.PedigreePageField.Update(pedigreePageField);

        db.SaveChanges();

        try
        {
          emailHandler.SendEmail(
                ToAddresses: new List<string> { "vbester3@gmail.com" },
                Subject: $"Bully Assure: Deleted Pedigree Field",
                Body: $"Good Day <br />" +
                $"<br /> " +
                $"<br /> {db.Users.FirstOrDefault(u => u.UserID == Guid.Parse(HttpContext.Session.GetString("UserID"))).Firstname} deleted pedigree field - {pedigreePageField.FieldName} View" +
                $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
                $"<br /> " +
                $"<br />" +
                $"<br /> Kind Regards," +
               $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
                AttachmentsBinaries: null,
                UserID: HttpContext.Session.GetString("UserID"),
                systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
                );
        }
        catch (Exception)
        {
        }

        return RedirectToAction("Index", "Settings");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    #endregion Pedigree Fields

    #region User

    [ControllerActionFilter]
    [HttpGet]
    public async Task<IActionResult> EditUser(string UserID)
    {
      try
      {
        var user = await this.db.Users.Include(f => f.UserPermission).FirstOrDefaultAsync(e => e.UserID == Guid.Parse(UserID));
        ViewData["User"] = user;

        if (user != null)
        {
          return View("EditUser", user);
        }

        return RedirectToAction("Index");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpGet]
    public async Task<IActionResult> DeleteUser(string UserID)
    {
      try
      {
        var user = await this.db.Users.Include(f => f.UserPermission).FirstOrDefaultAsync(e => e.UserID == Guid.Parse(UserID));
        if (user != null)
        {
          user.IsDeleted = true;

          this.db.Users.Update(user);
          await this.db.SaveChangesAsync();
        }

        try
        {
          emailHandler.SendEmail(
                ToAddresses: new List<string> { "vbester3@gmail.com" },
                Subject: $"Bully Assure: Deleted User",
                Body: $"Good Day <br />" +
                $"<br /> " +
                $"<br /> {db.Users.FirstOrDefault(u => u.UserID == Guid.Parse(HttpContext.Session.GetString("UserID"))).Firstname} deleted user - {user.Firstname} View" +
                $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
                $"<br /> " +
                $"<br />" +
                $"<br /> Kind Regards," +
               $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
                AttachmentsBinaries: null,
                UserID: HttpContext.Session.GetString("UserID"),
                systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
                );
        }
        catch (Exception)
        {
        }

        return RedirectToAction("Index");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpGet]
    public async Task<IActionResult> RestoreUser(string UserID)
    {
      try
      {
        var user = await this.db.Users.Include(f => f.UserPermission).FirstOrDefaultAsync(e => e.UserID == Guid.Parse(UserID));
        if (user != null)
        {
          user.IsDeleted = false;

          this.db.Users.Update(user);
          await this.db.SaveChangesAsync();
        }

        try
        {
          emailHandler.SendEmail(
                ToAddresses: new List<string> { "vbester3@gmail.com" },
                Subject: $"Bully Assure: Restored User",
                Body: $"Good Day <br />" +
                $"<br /> " +
                $"<br /> {db.Users.FirstOrDefault(u => u.UserID == Guid.Parse(HttpContext.Session.GetString("UserID"))).Firstname} restored user - {user.Firstname} View" +
                $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
                $"<br /> " +
                $"<br />" +
                $"<br /> Kind Regards," +
               $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
                AttachmentsBinaries: null,
                UserID: HttpContext.Session.GetString("UserID"),
                systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
                );
        }
        catch (Exception)
        {
        }

        return RedirectToAction("Index");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public async Task<IActionResult> UpdateUser(UserMain User)
    {
      try
      {
        if (User != null)
        {
          this.db.Users.Update(User);
          await this.db.SaveChangesAsync();
        }

        try
        {
          emailHandler.SendEmail(
                ToAddresses: new List<string> { "vbester3@gmail.com" },
                Subject: $"Bully Assure: Updated User",
                Body: $"Good Day <br />" +
                $"<br /> " +
                $"<br /> {db.Users.FirstOrDefault(u => u.UserID == Guid.Parse(HttpContext.Session.GetString("UserID"))).Firstname} Updated user - {User.Firstname} View" +
                $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
                $"<br /> " +
                $"<br />" +
                $"<br /> Kind Regards," +
               $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
                AttachmentsBinaries: null,
                UserID: HttpContext.Session.GetString("UserID"),
                systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
                );
        }
        catch (Exception)
        {
        }

        return RedirectToAction("Index");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    #endregion User

    [ControllerActionFilter]
    [HttpPost]
    public async Task<IActionResult> GlobalSettingsSave(GlobalSettings globalSettings)
    {
      try
      {
        _ = globalSettings ?? throw new NullReferenceException();

        this.db.GlobalSettings.Update(globalSettings);
        await this.db.SaveChangesAsync();

        return RedirectToAction("Index");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpGet]
    public async Task<IActionResult> ResendAllMails()
    {
      try
      {
        var emails = await this.db.EmailCaptures.Where(e => !e.SentSuccessfull).ToListAsync();

        foreach (var email in emails)
        {
          try
          {
            emailHandler.SendEmail(
                  ToAddresses: new List<string> { email.ToEmail },
                  Subject: "Bully Assure: Email Resend",
                  Body: email.EmailMessage,
                  AttachmentsBinaries: null,
                  UserID: HttpContext.Session.GetString("UserID"),
                  systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
                  );
            email.SentSuccessfull = true;
          }
          catch
          {
          }
        }

        this.db.EmailCaptures.RemoveRange(emails);
        await this.db.SaveChangesAsync();

        return RedirectToAction("Index");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }
  }
}