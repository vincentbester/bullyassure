﻿using BullyAssure.Models;
using Microsoft.ApplicationInsights;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Diagnostics;

namespace BullyAssure.Controllers
{
  public class ErrorController : Controller
  {
    private readonly TelemetryClient _telemetryClient;

    public ErrorController(TelemetryClient telemetryClient)
    {
      _telemetryClient = telemetryClient;
    }

    [Route("{ControllerName}/500")]
    [HttpGet]
    public IActionResult AppError()
    {
      var exceptionHandlerPathFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
      _telemetryClient.TrackException(exceptionHandlerPathFeature.Error);
      _telemetryClient.TrackEvent("Error.ServerError", new Dictionary<string, string>
      {
        ["originalPath"] = exceptionHandlerPathFeature.Path,
        ["error"] = exceptionHandlerPathFeature.Error.Message
      });
      return View("Error", new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }

    [Route("{ControllerName}/404")]
    [HttpGet]
    public IActionResult AppNotFound()
    {
      string originalPath = "unknown";
      if (HttpContext.Items.ContainsKey("originalPath"))
      {
        originalPath = HttpContext.Items["originalPath"] as string;
      }
      _telemetryClient.TrackEvent("Error.PageNotFound", new Dictionary<string, string>
      {
        ["originalPath"] = originalPath
      });
      return View("NotFound", new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
  }
}