using BullyAssure.Filters;
using BullyAssure.Models.Owners;
using DatabaseConnector;
using DatabaseConnector.Tables.Owners;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BullyAssure.Controllers
{
  public class OwnerController : Controller
  {
    private BullyAssureContext db;
    private IConfiguration config;

    public OwnerController(BullyAssureContext _db, IConfiguration _config)
    {
      db = _db;
      config = _config;
    }

    ///<summary>
    /// Gets Owners.
    ///</summary>
    [ControllerActionFilter]
    [HttpGet]
    public IActionResult Index()
    {
      try
      {
        var Owners = db.Owners.Include(e => e.OwnerFieldValues).ThenInclude(e => e.OwnerFields).Where(e => !e.isDeleted).OrderBy(e => e.OwnerName).ToList();

        return View("Index", Owners);
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpGet]
    public IActionResult Edit(string OwnerID)
    {
      try
      {
        var Owner = db.Owners.Include(e => e.OwnerFieldValues).ThenInclude(e => e.OwnerFields).First(e => e.OwnerID == Guid.Parse(OwnerID) && !e.isDeleted);

        return View("Edit", Owner);
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public IActionResult Save(OwnerMain OwnerDetails)
    {
      try
      {
        if (String.IsNullOrEmpty(OwnerDetails.OwnerName))
        {
          return View("Edit", OwnerDetails);
        }
        if (OwnerDetails.OwnerFieldValues != null) foreach (var field in OwnerDetails.OwnerFieldValues)
          {
            if (field.Required && String.IsNullOrEmpty(field.FieldValue))
            {
              return View("Edit", OwnerDetails);
            }
          }
        db.Owners.Update(OwnerDetails);
        db.SaveChanges();

        ViewData["Message"] = "Successfully saved";
        return RedirectToAction("Index", "Owner");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpGet]
    public IActionResult Delete([FromQuery] String OwnerID)
    {
      try
      {
        var UserID = HttpContext.Session.GetString("UserID");

        var OwnerDetails = db.Owners.FirstOrDefault(e => e.OwnerID == Guid.Parse(OwnerID));
        OwnerDetails.isDeleted = true;
        db.Owners.Update(OwnerDetails);
        db.SaveChanges();

        ViewData["Message"] = "Successfully saved";
        return RedirectToAction("Index", "Owner");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpGet]
    public IActionResult Create()
    {
      try
      {
        var NewOwner = new OwnerMains
        {
          OwnerID = Guid.NewGuid(),
          OwnerName = "",
          OwnerFieldValues = new List<OwnerFieldValue>(),
          isDeleted = false
        };
        var OwnerFieldValues = db.OwnerPageFields.ToList();
        foreach (var field in OwnerFieldValues)
        {
          NewOwner.OwnerFieldValues.Add(new OwnerFieldValue
          {
            OwnerFieldValueID = Guid.NewGuid(),
            OwnerID = NewOwner.OwnerID,
            isDeleted = false,
            OwnerFields = field,
            OwnerFieldsID = field.FieldID
          });
        }
        return View("Create", NewOwner);
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public IActionResult AddOwner(OwnerMain OwnerDetails)
    {
      try
      {
        db.Owners.Add(OwnerDetails);
        db.SaveChanges();

        return RedirectToAction("Index", "Owner");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }
  }
}