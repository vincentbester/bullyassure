﻿using ApplicationWorkers.Communications;
using BullyAssure.Filters;
using DatabaseConnector;
using DatabaseConnector.Tables.Breeders;
using DatabaseConnector.Tables.Configuration;
using DatabaseConnector.Tables.Permissions;
using DatabaseConnector.Tables.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace BullyAssure.Controllers
{
  public class BreederController : Controller
  {
    private readonly BullyAssureContext db;
    private readonly IConfiguration config;
    private readonly EmailHandler emailHandler;

    public BreederController(BullyAssureContext _db, IConfiguration _config, EmailHandler _emailHandler)
    {
      this.db = _db;
      this.config = _config;
      this.emailHandler = _emailHandler;
    }

    [ControllerActionFilter]
    public async Task<IActionResult> Index()
    {
      var user = (await this.db.Users.FirstOrDefaultAsync(e => e.UserID == Guid.Parse(HttpContext.Session.GetString("UserID"))));

      var BreederFields = (await this.db.BreederPageFields.Where(e => !e.isDeleted).ToListAsync());
      var BreederDetails = (await this.db.Breeders
        .Include(e => e.BreederFieldsValues.OrderBy(f => f.BreederFields.FieldName))
        .ThenInclude(g => g.BreederFields)
        .ThenInclude(g => g.FieldType)
        .Include(e => e.Kennel)
        .Where(f => !f.isDeleted).ToListAsync());

      if (user.isBreeder)
      {
        var breederUser = (await this.db.BreederUsers.Include(f => f.Breeder).ThenInclude(f => f.Kennel).FirstOrDefaultAsync(f => f.UserId == user.UserID));
        BreederDetails = BreederDetails.Where(e => e.KennelId == (breederUser?.Breeder?.Kennel?.KennelID ?? Guid.Empty)).ToList();
      }
      else
      if (user.isKennel)
      {
        var kennelUser = (await this.db.KennelUsers.FirstOrDefaultAsync(f => f.UserId == user.UserID));
        BreederDetails = BreederDetails.Where(e => e.KennelId == (kennelUser?.KennelID ?? Guid.Empty)).ToList();
      }
      else
      if (user.isOwner)
      {
        BreederDetails = this.db.Breeders.Where(e => false).ToList();
      }

      foreach (var item in BreederFields)
      {
        foreach (var Breeder in BreederDetails)
        {
          if (!Breeder.BreederFieldsValues.Any(e => e.BreederFieldsID == item.FieldID))
          {
            Breeder.BreederFieldsValues.Add(new BreederFieldsValues
            {
              BreederFieldsID = item.FieldID,
              BreederFields = item
            });
          }
        }
      }
      ViewData["Error"] = "";
      return View(BreederDetails);
    }

    [ControllerActionFilter]
    public async Task<IActionResult> Create()
    {
      var user = (await this.db.Users.FirstOrDefaultAsync(e => e.UserID == Guid.Parse(HttpContext.Session.GetString("UserID"))));

      ViewData["UserMain"] = user;
      ViewData["KennelID"] = (await this.db.KennelUsers.FirstOrDefaultAsync(e => e.UserId == user.UserID)).KennelID;

      var BreederPageFields = db.BreederPageFields.Where(e => !e.isDeleted).Include(e => e.FieldType).ToList();
      BreederMain BreederMain = new BreederMain();
      BreederMain.BreederFieldsValues = new List<BreederFieldsValues>();
      foreach (var item in BreederPageFields)
      {
        BreederMain.BreederFieldsValues.Add(new BreederFieldsValues
        {
          BreederFields = item,
          BreederFieldsID = item.FieldID
        });
      }

      var Kennels = db.Kennels.Where(e => !e.isDeleted).OrderBy(e => e.KennelName).ToList();
      Dictionary<Guid, string> kennels = new Dictionary<Guid, string>();
      Kennels.ForEach(x => kennels.Add(x.KennelID, x.KennelName));

      ViewData["Kennels"] = kennels;

      return View("Create", BreederMain);
    }

    [ControllerActionFilter]
    public IActionResult Save(BreederMain BreederDetails)
    {
      var NewBreeder = (BreederDetails.BreederId == Guid.Empty);

      if (BreederDetails != null)
      {
        db.Breeders.Update(BreederDetails);
        db.SaveChanges();

        string RandomPass = Encryption.PasswordEncrypt.RandomString(8);
        UserMain user = new UserMain
        {
          Username = $"{BreederDetails.BreederFirstName}_{BreederDetails.BreederSurname}_breeder",
          Email = BreederDetails.EmailAddress,
          Firstname = BreederDetails.BreederFirstName,
          isBreeder = true,
          Cellnumber = BreederDetails.CellNumber,
          OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Breeder").StructureID,
          Password = new Encryption.PasswordEncrypt().GeneratePassword(RandomPass),
          UserPermission = new UserPermissions()
        };

        if (NewBreeder)
        {
          db.Users.Add(user);
          db.SaveChanges();
          BreederUser breederUser = new BreederUser
          {
            BreederId = BreederDetails.BreederId,
            UserId = user.UserID
          };

          db.BreederUsers.Add(breederUser);
          db.SaveChanges();

          emailHandler.SendEmail(
            ToAddresses: new List<string> { BreederDetails.EmailAddress },
            Subject: $"Bully Assure: Breeder Successfully { (NewBreeder ? "Registered" : "Saved") }",
            Body: $"Good Day <br />" +
              $"<br /> " +
              $"<br /> {BreederDetails.BreederFirstName} has successfully been { (NewBreeder ? "registered" : "saved") } on Bully Assure system." +
              $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
              $"<br /> " +
              $"<br /> Login Details: " +
              $"{ (NewBreeder ? "<br />" + "Username: " + user.Username : "") }" +
              $"{ (NewBreeder ? "<br />" + "Password: " + RandomPass : "") }" +
              $"<br />" +
              $"<br /> Kind Regards," +
              $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
            AttachmentsBinaries: null,
            UserID: HttpContext.Session.GetString("UserID"),
            systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
            );
        }

        ViewData["Error"] = "Successfully saved Breeder details.";
      }
      else
      {
        ViewData["Error"] = "Failed to save Breeder details.";
      }
      return RedirectToAction("Index", "Breeder");
    }

    [ControllerActionFilter]
    public async Task<IActionResult> Edit(string BreederID)
    {
      if (!String.IsNullOrEmpty(BreederID))
      {
        var user = (await this.db.Users.FirstOrDefaultAsync(e => e.UserID == Guid.Parse(HttpContext.Session.GetString("UserID"))));

        ViewData["UserMain"] = user;

        var Kennels = db.Kennels.Where(e => !e.isDeleted).OrderBy(e => e.KennelName).ToList();
        Dictionary<Guid, string> kennels = new Dictionary<Guid, string>();
        Kennels.ForEach(x => kennels.Add(x.KennelID, x.KennelName));

        ViewData["Kennels"] = kennels;

        return View("Edit", db.Breeders.Include(e => e.BreederFieldsValues).ThenInclude(f => f.BreederFields).ThenInclude(g => g.FieldType).FirstOrDefault(e => e.BreederId == Guid.Parse(BreederID) && !e.isDeleted));
      }
      else
      {
        ViewData["Error"] = "Failed to edit Breeder details.";
        return View();
      }
    }

    [ControllerActionFilter]
    public IActionResult Delete(string BreederID)
    {
      if (!String.IsNullOrEmpty(BreederID))
      {
        var breeder = db.Breeders.FirstOrDefault(e => e.BreederId == Guid.Parse(BreederID));
        if (breeder == null)
        {
          ViewData["Error"] = "Breeder not found.";
          return View("Index");
        }
        breeder.isDeleted = true;
        db.Breeders.Update(breeder);
        db.SaveChanges();

        try
        {
          emailHandler.SendEmail(
                ToAddresses: new List<string> { "vbester3@gmail.com" },
                Subject: $"Bully Assure: Deleted Breeder",
                Body: $"Good Day <br />" +
                $"<br /> " +
                $"<br /> {db.Users.FirstOrDefault(u => u.UserID == Guid.Parse(HttpContext.Session.GetString("UserID"))).Firstname} deleted Breeder - {breeder.BreederFirstName} View" +
                $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
                $"<br /> " +
                $"<br />" +
                $"<br /> Kind Regards," +
               $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
                AttachmentsBinaries: null,
                UserID: HttpContext.Session.GetString("UserID"),
                systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
                );
        }
        catch (Exception)
        {
        }

        ViewData["Error"] = "Breeder Successfully Deleted.";
        return RedirectToAction("Index", "Breeder");
      }
      else
      {
        ViewData["Error"] = "Failed to delete Breeder details.";
        return View("Index");
      }
    }
  }
}