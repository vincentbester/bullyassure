using ApplicationWorkers.Communications;
using BullyAssure.Filters;
using DatabaseConnector;
using DatabaseConnector.Tables.Audits;
using DatabaseConnector.Tables.Configuration;
using DatabaseConnector.Tables.Users;
using Encryption;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace BullyAssure.Controllers
{
  public class LoginController : Controller
  {
    private BullyAssureContext db;
    private IConfiguration config;
    private readonly EmailHandler emailHandler;

    public LoginController(BullyAssureContext _db, IConfiguration _config, EmailHandler _emailHandler)
    {
      this.db = _db;
      this.config = _config;
      this.emailHandler = _emailHandler;
    }

    [HttpGet]
    public IActionResult LoginPage()
    {
      HttpContext.Request.HttpContext.Session.Clear();
      return View();
    }

    [ControllerActionFilter]
    [HttpGet]
    [Route("{ControllerName}/SignOff")]
    public IActionResult SignOff()
    {
      HttpContext.Request.HttpContext.Session.Clear();
      return View("LoginPage");
    }

    [HttpPost]
    [Route("{ControllerName}/Login")]
    public async Task<IActionResult> Login([FromForm] string username, [FromForm] string password)
    {
      try
      {
        if (!string.IsNullOrEmpty(username) || !string.IsNullOrEmpty(password))
        {
          var User = await this.db.Users.FirstOrDefaultAsync(e => e.Username == username && e.Password == new PasswordEncrypt().GeneratePassword(password) && !e.IsDeleted);
          if (User == null)
          {
            LogAuditLogin LogLogin = new LogAuditLogin(db);
            LogLogin.LogAudit(username, "Failed");

            ViewData["Error"] = "Login Failed";
            return View("LoginPage");
          }
          else
          {
            if (User.isKennel)
            {
              if (!await this.db.KennelUsers.Include(e => e.Kennel)
                .AnyAsync(e => e.UserId == User.UserID
                && e.Kennel.MembershipUpToDate
                && !e.Kennel.isDeleted))
              {
                ViewData["Error"] = "Membership not up to date";
                return View("LoginPage");
              }
            }
            else if (User.isBreeder)
            {
              if (!await this.db.BreederUsers.Include(e => e.Breeder).ThenInclude(f => f.Kennel)
                .AnyAsync(e => e.UserId == User.UserID
                && e.Breeder.Kennel.MembershipUpToDate
                && !e.Breeder.isDeleted
                && !e.Breeder.Kennel.isDeleted))
              {
                ViewData["Error"] = "Membership not up to date";
                return View("LoginPage");
              }
            }

            LogAuditLogin LogLogin = new LogAuditLogin(db);
            LogLogin.LogAudit(username, "Success");
            var tokenString = GenerateJSONWebToken(User.UserID);

            db.UserSessions.Update(new UserSession
            {
              EntryDate = DateTime.Now,
              ExpiryDate = DateTime.Now.AddHours(1),
              Token = tokenString,
              UserMainID = User.UserID
            });
            db.SaveChanges();

            HttpContext.Request.HttpContext.Session.Clear();
            HttpContext.Session.SetString("UserID", User.UserID.ToString());
            HttpContext.Session.SetString("token", tokenString);
            HttpContext.Session.SetString("ExpiryDate", DateTime.Now.AddHours(1).ToString());

            HttpContext.Session.SetString("SYSTEMCONFIGURATION", JsonSerializer.Serialize(db.SystemConfigurations.First() ?? throw new NullReferenceException("No System Configuration Defined.")));

            ViewData["FirstName"] = User.Firstname;
            ViewData["LastName"] = User.Surname;

            emailHandler.SendEmail(
              ToAddresses: new List<string> { User.Email },
              Subject: $"Bully Assure: Successful Login",
              Body: $"Good Day <br />" +
              $"<br /> " +
              $"<br /> You successfully logged into Bully Assure system." +
              $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
              $"<br /> " +
              $"<br />" +
              $"<br /> Kind Regards," +
             $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
              AttachmentsBinaries: null,
              UserID: HttpContext.Session.GetString("UserID"),
              systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
              );

            return RedirectToAction("Index", "Dashboard");
          }
        }
        else
        {
          ViewData["Error"] = "No login details provided";
          return View("LoginPage");
        }
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        ViewData["Error"] = "Error Occurred";
        return View("LoginPage");
      }
    }

    private string GenerateJSONWebToken(Guid UserID)
    {
      return new Encryptor().Encrypt(UserID.ToString());
    }
  }
}