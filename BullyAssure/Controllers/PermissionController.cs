﻿using BullyAssure.Filters;
using Microsoft.AspNetCore.Mvc;

namespace BullyAssure.Controllers
{
  public class PermissionController : Controller
  {
    [HttpGet]
    [ControllerActionFilter]
    public IActionResult Index()
    {
      return View();
    }
  }
}