﻿using ApplicationWorkers.Communications;
using BullyAssure.Filters;
using BullyAssure.Helpers;
using BullyAssure.Models.ExternalIntegration;
using DatabaseConnector;
using DatabaseConnector.Tables.Breeders;
using DatabaseConnector.Tables.Configuration;
using DatabaseConnector.Tables.Pedigrees;
using DatabaseConnector.Tables.Permissions;
using DatabaseConnector.Tables.Users;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.css;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BullyAssure.Controllers
{
  public class PedigreeController : Controller
  {
    private readonly BullyAssureContext db;
    private readonly IConfiguration config;
    private readonly EmailHandler emailHandler;
    private readonly IViewRenderService view;
    private readonly IWebHostEnvironment environment;
    private readonly ICSSResolver cssResolver;
    private readonly ICssFile cssFile;

    public PedigreeController(
      BullyAssureContext _db,
      IConfiguration _config,
      EmailHandler _emailHandler,
      IViewRenderService view,
      IWebHostEnvironment _environment,
      ICSSResolver cSSResolver,
      ICssFile _cssFile)
    {
      this.environment = _environment;
      this.db = _db;
      this.config = _config;
      this.emailHandler = _emailHandler;
      this.view = view;
      this.cssResolver = cSSResolver;
      this.cssFile = _cssFile;
    }

    [ControllerActionFilter]
    [HttpGet]
    public IActionResult Index()
    {
      try
      {
        string UserID = HttpContext.Session.GetString("UserID");
        var user = db.Users.Include(g => g.UserPermission).FirstOrDefault(e => e.UserID == Guid.Parse(UserID));
        ViewData["User"] = user;

        var pedigreeFields = db.PedigreePageField.Where(e => !e.isDeleted).ToList();
        var pedigrees = db.Pedigrees
          .Include(e => e.PedigreeFieldsValues.OrderBy(f => f.PedigreeFields.FieldName))
            .ThenInclude(g => g.PedigreeFields)
            .ThenInclude(g => g.FieldType)
          .Include(e => e.Dam)
          .Include(e => e.Sire)
          .Include(e => e.Breeder)
          .Include(e => e.CoOwner)
          .Include(e => e.Owner)
          .Include(e => e.PedigreeApplication)
          .Where(e => (//db.PedigreeApplications.Any(f => f.Accepted && f.PedigreeId == e.PedigreeID) ||
               user.UserPermission.ViewAllPedigrees
            || e.KennelId == (this.db.KennelUsers.FirstOrDefault(f => f.UserId == user.UserID) == null ? Guid.Empty : this.db.KennelUsers.FirstOrDefault(f => f.UserId == user.UserID).KennelID)
            || e.BreederId == (this.db.BreederUsers.FirstOrDefault(f => f.UserId == user.UserID) == null ? Guid.Empty : this.db.BreederUsers.FirstOrDefault(f => f.UserId == user.UserID).BreederId)
            || e.OwnerId == user.UserID
          ))
          .ToList();

        foreach (var item in pedigreeFields)
        {
          foreach (var pedigree in pedigrees)
          {
            if (!pedigree.PedigreeFieldsValues.Any(e => e.PedigreeFieldsID == item.FieldID))
            {
              pedigree.PedigreeFieldsValues.Add(new PedigreeFieldsValue
              {
                PedigreeFieldsID = item.FieldID,
                PedigreeFields = item
              });
            }
          }
        }

        if (!(user.isAdmin || user.isDeveloper || user.isBreeder || user.isKennel))
        {
          pedigrees = pedigrees.Where(e =>
          (
              e.OwnerId == Guid.Parse(UserID)
              || e.CoOwnerId == Guid.Parse(UserID)
              || e.BreederId == Guid.Parse(UserID)
          )
          && this.db.PedigreeApplications.Any(f => f.Accepted && f.PedigreeId == e.PedigreeID))
          .ToList();
        }

        pedigrees.ForEach(x => x.PedigreeApplication = db.PedigreeApplications.FirstOrDefault(e => e.PedigreeId == x.PedigreeID));

        return View("Index", pedigrees);
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpGet]
    public async Task<IActionResult> Edit(string PedigreeID)
    {
      var user = await this.db.Users.Include(x => x.UserPermission).FirstOrDefaultAsync(e => e.UserID == Guid.Parse(HttpContext.Session.GetString("UserID")));
      ViewData["User"] = user;

      var Breeders = new List<BreederMain>();
      if (user.isKennel || user.isBreeder)
      {
        Breeders = db.Breeders
            .Where(e => !e.isDeleted
              && (
                  e.KennelId == (db.KennelUsers.First(e => e.UserId == user.UserID).KennelID) ||
                  e.BreederId == (db.BreederUsers.First(e => e.UserId == user.UserID).BreederId)
              )
            )
            .OrderBy(e => e.BreederFirstName).ToList();
      }
      else
      {
        Breeders = db.Breeders.Where(e => !e.isDeleted).OrderBy(e => e.BreederFirstName).ToList();
      }
      Dictionary<Guid, string> breeders = new Dictionary<Guid, string>();
      Breeders.ForEach(x => breeders.Add(x.BreederId, x.BreederFirstName));

      ViewData["Breeders"] = breeders;

      var Sires = db.Pedigrees.OrderBy(e => e.Name).Where(e => e.Sex.ToLower() == "m" && e.PedigreeID != Guid.Parse(PedigreeID)).ToList();
      Dictionary<long, string> sires = new Dictionary<long, string>();
      Sires.ForEach(x => sires.Add(x.BAID, x.Name));

      ViewData["Sires"] = sires;

      var Dams = db.Pedigrees.OrderBy(e => e.Name).Where(e => e.Sex.ToLower() == "f" && e.PedigreeID != Guid.Parse(PedigreeID)).ToList();
      Dictionary<long, string> dams = new Dictionary<long, string>();
      Dams.ForEach(x => dams.Add(x.BAID, x.Name));

      ViewData["Dams"] = dams;

      var pedigreeFields = db.PedigreePageField.Where(e => !e.isDeleted).ToList();
      var pedigrees = db.Pedigrees
        .Include(e => e.PedigreeFieldsValues.OrderBy(f => f.PedigreeFields.FieldName))
          .ThenInclude(g => g.PedigreeFields)
          .ThenInclude(g => g.FieldType)
        .Include(e => e.Dam)
        .Include(e => e.Sire)
        .Include(e => e.Breeder)
        .Include(e => e.CoOwner)
        .Include(e => e.Owner)
        .Include(e => e.PedigreeApplication)
        .Where(e => e.PedigreeID == Guid.Parse(PedigreeID) &&
          (db.PedigreeApplications.Any(f => f.Accepted && f.PedigreeId == e.PedigreeID)
            || user.UserPermission.ViewAllPedigrees
            || e.KennelId == (this.db.KennelUsers.FirstOrDefault(f => f.UserId == user.UserID) == null ? Guid.Empty : this.db.KennelUsers.FirstOrDefault(f => f.UserId == user.UserID).KennelID)
            || e.BreederId == (this.db.BreederUsers.FirstOrDefault(f => f.UserId == user.UserID) == null ? Guid.Empty : this.db.BreederUsers.FirstOrDefault(f => f.UserId == user.UserID).BreederId)
            || e.OwnerId == user.UserID
          ))
        .ToList();

      foreach (var item in pedigreeFields)
      {
        foreach (var pedigree in pedigrees)
        {
          if (!pedigree.PedigreeFieldsValues.Any(e => e.PedigreeFieldsID == item.FieldID))
          {
            pedigree.PedigreeFieldsValues.Add(new PedigreeFieldsValue
            {
              PedigreeFieldsID = item.FieldID,
              PedigreeFields = item
            });
          }
        }
      }

      if (!(user.isAdmin || user.isDeveloper || user.isBreeder || user.isKennel))
      {
        pedigrees = pedigrees.Where(e =>
            e.OwnerId == Guid.Parse(HttpContext.Session.GetString("UserID"))
            || e.CoOwnerId == Guid.Parse(HttpContext.Session.GetString("UserID"))
            || e.BreederId == Guid.Parse(HttpContext.Session.GetString("UserID"))).ToList();
      }

      pedigrees.ForEach(x => x.PedigreeApplication = db.PedigreeApplications.FirstOrDefault(e => e.PedigreeId == x.PedigreeID));

      return View("Edit", pedigrees.First());
    }

    [ControllerActionFilter]
    [HttpGet]
    public IActionResult Create()
    {
      try
      {
        var user = this.db.Users.FirstOrDefault(e => e.UserID == Guid.Parse(HttpContext.Session.GetString("UserID")));
        ViewData["User"] = user;

        var Breeders = new List<BreederMain>();
        if (user.isKennel || user.isBreeder)
        {
          Breeders = db.Breeders
              .Where(e => !e.isDeleted
                && (
                    e.KennelId == (db.KennelUsers.First(e => e.UserId == user.UserID).KennelID) ||
                    e.BreederId == (db.BreederUsers.First(e => e.UserId == user.UserID).BreederId)
                )
              )
              .OrderBy(e => e.BreederFirstName).ToList();
        }
        else
        {
          Breeders = db.Breeders.Where(e => !e.isDeleted).OrderBy(e => e.BreederFirstName).ToList();
        }
        Dictionary<Guid, string> breeders = new Dictionary<Guid, string>();
        Breeders.ForEach(x => breeders.Add(x.BreederId, x.BreederFirstName));

        ViewData["Breeders"] = breeders;

        var Sires = db.Pedigrees.OrderBy(e => e.Name).Where(e => e.Sex.ToLower() == "m").ToList();
        Dictionary<Guid, string> sires = new Dictionary<Guid, string>();
        Sires.ForEach(x => sires.Add(x.PedigreeID, x.Name));

        ViewData["Sires"] = sires;

        var Dams = db.Pedigrees.OrderBy(e => e.Name).Where(e => e.Sex.ToLower() == "f").ToList();
        Dictionary<Guid, string> dams = new Dictionary<Guid, string>();
        Dams.ForEach(x => dams.Add(x.PedigreeID, x.Name));

        ViewData["Dams"] = dams;

        return View("Create", new PedigreeMain());
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public async Task<IActionResult> Save([FromForm] string pedigreeRegistrationsSerialized, [FromForm] PedigreeMain PedigreeMain)
    {
      List<PedigreeMain> pedigreeMains = new List<PedigreeMain>();
      if (pedigreeRegistrationsSerialized != null)
      {
        pedigreeMains = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PedigreeMain>>(pedigreeRegistrationsSerialized);
      }
      else
      {
        pedigreeMains = new List<PedigreeMain>
        {
          PedigreeMain
        };
      }

      try
      {
        foreach (var pedigreeMain in pedigreeMains)
        {
          if (pedigreeMain == null)
          {
            ViewData["Message"] = "Pedigree Data can not be null";
            return RedirectToAction("Create", "Pedigree");
          }

          var user = this.db.Users.FirstOrDefault(e => e.UserID == Guid.Parse(HttpContext.Session.GetString("UserID")));

          if (pedigreeMain.PedigreeID == Guid.Empty)
          {
            pedigreeMain.KennelId = (user.isKennel ? (await this.db.KennelUsers.FirstOrDefaultAsync(f => f.UserId == user.UserID)).KennelID : null);
            pedigreeMain.BreederId = (user.isBreeder ? (await this.db.BreederUsers.FirstOrDefaultAsync(f => f.UserId == user.UserID)).BreederId : null);
          }

          db.Pedigrees.Update(pedigreeMain);
          db.SaveChanges();

          var NewPedigree = (pedigreeMain.PedigreeID == Guid.Empty);

          if (NewPedigree)
          {
            pedigreeMain.DOI = DateTime.Now.ToString();

            if (pedigreeMain.Owner != null)
            {
              string RandomPass = Encryption.PasswordEncrypt.RandomString(8);
              UserMain owner = new UserMain
              {
                Username = $"{pedigreeMain.Owner.OwnerName}_{pedigreeMain.Owner.OwnerSurname}_owner",
                Email = pedigreeMain.Owner.EmailAddress,
                Firstname = pedigreeMain.Owner.OwnerName,
                isOwner = true,
                Cellnumber = pedigreeMain.Owner.CellNumber,
                OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Owner").StructureID,
                Password = new Encryption.PasswordEncrypt().GeneratePassword(RandomPass),
                UserPermission = new UserPermissions()
              };

              if (!user.isKennel || !user.isBreeder)
              {
                db.Users.Add(owner);
                db.SaveChanges();

                if (pedigreeMain.Owner != null) emailHandler.SendEmail(
                  ToAddresses: new List<string> { pedigreeMain.Owner.EmailAddress },
                  Subject: $"Bully Assure: Owner Successfully { (NewPedigree ? "Registered" : "Saved") }",
                  Body: $"Good Day <br />" +
                    $"<br /> " +
                    $"<br /> {pedigreeMain.Owner.OwnerName} has successfully been { (NewPedigree ? "registered" : "saved") } on Bully Assure system." +
                    $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
                    $"<br /> " +
                    $"<br /> Login Details: " +
                    $"<br />{ (NewPedigree ? "<br />" + "Username: " + owner.Username : "") }" +
                    $"<br />{ (NewPedigree ? "<br />" + "Password: " + RandomPass : "") }" +
                    $"<br />" +
                    $"<br /> Kind Regards," +
                    $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
                  AttachmentsBinaries: null,
                  UserID: HttpContext.Session.GetString("UserID"),
                  systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
                  );
                PedigreeApplication pedigreeApplication = new PedigreeApplication
                {
                  PedigreeId = pedigreeMain.PedigreeID,
                  UserId = owner.UserID,
                  DateApplied = DateTime.Now
                };

                db.PedigreeApplications.Add(pedigreeApplication);
                db.SaveChanges();

                PedigreeUser pedigreeUser = new PedigreeUser
                {
                  PedigreeID = pedigreeMain.PedigreeID,
                  UserId = owner.UserID
                };

                db.PedigreeUsers.Add(pedigreeUser);
                db.SaveChanges();

                emailHandler.SendEmail(
                  ToAddresses: new List<string> { pedigreeMain.Owner.EmailAddress, JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).EmailFromAddress },
                  Subject: $"Bully Assure: New Pedigree Application",
                  Body: $"Good Day <br />" +
                    $"<br /> " +
                    $"<br /> {pedigreeMain.Owner.OwnerName} has applied a new pedigree on Bully Assure system." +
                    $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
                    $"<br /> " +
                    $"<br /> Kind Regards," +
                    $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
                  AttachmentsBinaries: null,
                  UserID: HttpContext.Session.GetString("UserID"),
                  systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
                  );
              }
            }

            if (!(user.isKennel || user.isBreeder))
            {
              if (pedigreeMain.CoOwner != null)
              {
                string RandomPass = Encryption.PasswordEncrypt.RandomString(8);
                UserMain CoOwner = new UserMain
                {
                  Username = $"{pedigreeMain.CoOwner.OwnerName}_{pedigreeMain.CoOwner.OwnerSurname}_coowner",
                  Email = pedigreeMain.CoOwner.EmailAddress,
                  Firstname = pedigreeMain.CoOwner.OwnerName,
                  isOwner = true,
                  Cellnumber = pedigreeMain.CoOwner.CellNumber,
                  OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Owner").StructureID,
                  Password = new Encryption.PasswordEncrypt().GeneratePassword(RandomPass),
                  UserPermission = new UserPermissions()
                };
                if (NewPedigree)
                {
                  db.Users.Add(CoOwner);
                  db.SaveChanges();

                  PedigreeUser pedigreeUser_CoOwner = new PedigreeUser
                  {
                    PedigreeID = pedigreeMain.PedigreeID,
                    UserId = CoOwner.UserID
                  };

                  db.PedigreeUsers.Add(pedigreeUser_CoOwner);
                  db.SaveChanges();

                  if (pedigreeMain.CoOwner != null) emailHandler.SendEmail(
                  ToAddresses: new List<string> { pedigreeMain.CoOwner.EmailAddress },
                  Subject: $"Bully Assure: Co Owner Successfully { (NewPedigree ? "Registered" : "Saved") }",
                  Body: $"Good Day <br />" +
                    $"<br /> " +
                    $"<br /> {pedigreeMain.CoOwner.OwnerName} has successfully been { (NewPedigree ? "registered" : "saved") } on Bully Assure system." +
                    $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
                    $"<br /> " +
                    $"{Environment.NewLine } Login Details: " +
                    $"<br />{ (NewPedigree ? "<br />" + "Username: " + CoOwner.Username : "") }" +
                    $"<br />{ (NewPedigree ? "<br />" + "Password: " + RandomPass : "") }" +
                    $"<br />" +
                    $"<br /> Kind Regards," +
               $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
                  AttachmentsBinaries: null,
                  UserID: HttpContext.Session.GetString("UserID"),
                  systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
                  );
                }
              }
            }
          }
          else
          {
            if (pedigreeMain.Owner != null) emailHandler.SendEmail(
                  ToAddresses: new List<string> { pedigreeMain.Owner?.EmailAddress, JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).EmailFromAddress },
                  Subject: $"Bully Assure: Pedigree Details Updated for {pedigreeMain.Name}",
                  Body: $"Good Day <br />" +
                    $"<br /> " +
                    $"<br /> Pedigree Details Updated for {pedigreeMain.Name} on Bully Assure system." +
                    $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
                    $"<br /> " +
                    $"<br /> Kind Regards," +
              $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
                  AttachmentsBinaries: null,
                  UserID: HttpContext.Session.GetString("UserID"),
                  systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
                  );
          }
        }
        return RedirectToAction("Index", "Pedigree");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpGet]
    public async Task<IActionResult> Applications()
    {
      try
      {
        string UserID = HttpContext.Session.GetString("UserID");
        var user = db.Users.Include(g => g.UserPermission).FirstOrDefault(e => e.UserID == Guid.Parse(UserID));
        ViewData["User"] = user;

        var PedigreeApplications = await this.db.PedigreeApplications
          .Include(f => f.Responses)
            .ThenInclude(g => g.User)
          .Include(f => f.Responses)
            .ThenInclude(g => g.ResponseDocuments)
          .Include(g => g.Pedigree)
            .ThenInclude(g => g.Dam)
          .Include(g => g.Pedigree)
            .ThenInclude(g => g.Sire)
          .Include(g => g.Pedigree)
            .ThenInclude(g => g.Breeder)
          .Include(g => g.Pedigree)
            .ThenInclude(g => g.CoOwner)
          .Include(g => g.Pedigree)
            .ThenInclude(g => g.Owner)
          .Include(g => g.Pedigree)
            .ThenInclude(g => g.PedigreeFieldsValues.OrderBy(f => f.PedigreeFields.FieldName))
            .ThenInclude(g => g.PedigreeFields)
            .ThenInclude(g => g.FieldType)
          .ToListAsync();

        if (!(user.isAdmin || user.isDeveloper))
        {
          PedigreeApplications = PedigreeApplications.Where(e => e.UserId == user.UserID && !e.Pedigree.IsDeleted).ToList();
        }

        PedigreeApplications.ForEach(x => x.Responses.ForEach(e => e.ResponseDocuments.ForEach(f => f.DocumentBinary = String.Empty)));

        var globalsettings = await this.db.GlobalSettings.FirstAsync();
        var systemconfigs = await this.db.SystemConfigurations.FirstAsync();

        ExternalPayment externalPayment = new ExternalPayment
        {
          paymenthostURL = this.config["ExternalPaymentsUrl"],
          merchant_id = globalsettings.MerchantID,
          merchant_key = globalsettings.MerchantKey,
          returnUrl = systemconfigs.WebsiteUrl + "/Pedigree/PaymentSuccess",
          email_address = systemconfigs.UserName
        };

        ViewData["ExternalPayment"] = externalPayment;
        ViewData["GlobalSettings"] = globalsettings;

        return View("Applications", PedigreeApplications);
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [HttpGet]
    public async Task<IActionResult> PaymentSuccess(string PedigreeId)
    {
      try
      {
        var pedigreeId = Guid.Parse(PedigreeId);

        var application = await this.db.PedigreeApplications.FirstOrDefaultAsync(e => e.PedigreeId == pedigreeId);
        application.PaymentReceived = true;
        application.PaymentReceivedDate = DateTime.UtcNow;

        this.db.PedigreeApplications.Update(application);
        await this.db.SaveChangesAsync();

        return RedirectToAction("Index");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [HttpGet]
    public async Task<IActionResult> PedigreeApplication(string PedigreeID)
    {
      HttpContext.Session.SetString("SYSTEMCONFIGURATION", JsonSerializer.Serialize(db.SystemConfigurations.First() ?? throw new NullReferenceException("No System Configuration Defined.")));

      var pedigree = await this.db.Pedigrees
          .Include(e => e.PedigreeFieldsValues.OrderBy(f => f.PedigreeFields.FieldName))
            .ThenInclude(g => g.PedigreeFields)
            .ThenInclude(g => g.FieldType)
          .Include(e => e.Dam)
          .Include(e => e.Sire)
          .Include(e => e.Breeder)
          .Include(e => e.CoOwner)
          .Include(e => e.Owner)
          .FirstOrDefaultAsync(e => e.PedigreeID == Guid.Parse(PedigreeID));
      if (db.PedigreeApplications.Any(e => e.PedigreeId == Guid.Parse(PedigreeID)))
        return View("ApplicationUnsuccessful", "There is already an Application for this Pedigree.");
      return View("PedigreeApplication", pedigree);
    }

    [HttpPost]
    public async Task<IActionResult> StartApplication(PedigreeMain PedigreeDetails)
    {
      try
      {
        var existingPedigree = await this.db.Pedigrees.AsNoTracking().FirstOrDefaultAsync(e => e.PedigreeID == PedigreeDetails.PedigreeID && e.BAID == PedigreeDetails.BAID);

        if (existingPedigree == null) return View("ApplicationUnsuccessful", "The indicated Pedigree does not exist, please consult Admin or Breeder/Kennel");
        if (PedigreeDetails.CoOwner.CellNumber == "" || PedigreeDetails.CoOwner.EmailAddress == "" || PedigreeDetails.CoOwner.OwnerName == "" || PedigreeDetails.CoOwner.OwnerSurname == "") return View("ApplicationUnsuccessful", "Please enter Owner details.");

        string RandomPass = Encryption.PasswordEncrypt.RandomString(8);
        UserMain owner = new UserMain
        {
          Username = $"{PedigreeDetails.Owner.OwnerName}_{PedigreeDetails.Owner.OwnerSurname}_owner",
          Email = PedigreeDetails.Owner.EmailAddress,
          Firstname = PedigreeDetails.Owner.OwnerName,
          isOwner = true,
          Cellnumber = PedigreeDetails.Owner.CellNumber,
          OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Owner").StructureID,
          Password = new Encryption.PasswordEncrypt().GeneratePassword(RandomPass),
          UserPermission = new UserPermissions()
        };

        db.Users.Add(owner);
        db.SaveChanges();
        try
        {
          emailHandler.SendEmail(
            ToAddresses: new List<string> { PedigreeDetails.Owner.EmailAddress },
            Subject: $"Bully Assure: Owner Successfully Registered",
            Body: $"Good Day <br />" +
              $"<br /> " +
              $"<br /> {PedigreeDetails.Owner.OwnerName} has successfully been registered on Bully Assure system." +
              $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
              $"<br /> " +
              $"{Environment.NewLine } Login Details: " +
              $"<br />{ "Username: " + owner.Username}" +
              $"<br />{ "Password: " + RandomPass}" +
              $"<br />" +
              $"<br /> Kind Regards," +
              $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
            AttachmentsBinaries: null,
            UserID: owner.UserID.ToString(),
            systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
            );
        }
        catch (Exception)
        {
        }

        PedigreeApplication pedigreeApplication = new PedigreeApplication
        {
          PedigreeId = PedigreeDetails.PedigreeID,
          UserId = owner.UserID,
          DateApplied = DateTime.Now
        };

        db.PedigreeApplications.Add(pedigreeApplication);
        db.SaveChanges();

        PedigreeUser pedigreeUser = new PedigreeUser
        {
          PedigreeID = PedigreeDetails.PedigreeID,
          UserId = owner.UserID
        };

        db.PedigreeUsers.Add(pedigreeUser);
        db.SaveChanges();

        if (PedigreeDetails.CoOwner.CellNumber != "" && PedigreeDetails.CoOwner.EmailAddress != "" && PedigreeDetails.CoOwner.OwnerName != "" && PedigreeDetails.CoOwner.OwnerSurname != "")
        {
          RandomPass = Encryption.PasswordEncrypt.RandomString(8);
          UserMain CoOwner = new UserMain
          {
            Username = $"{PedigreeDetails.CoOwner.OwnerName}_{PedigreeDetails.CoOwner.OwnerSurname}_coowner",
            Email = PedigreeDetails.CoOwner.EmailAddress,
            Firstname = PedigreeDetails.CoOwner.OwnerName,
            isOwner = true,
            Cellnumber = PedigreeDetails.CoOwner.CellNumber,
            OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Owner").StructureID,
            Password = new Encryption.PasswordEncrypt().GeneratePassword(RandomPass),
            UserPermission = new UserPermissions()
          };
          db.Users.Add(CoOwner);
          db.SaveChanges();

          PedigreeUser pedigreeUser_CoOwner = new PedigreeUser
          {
            PedigreeID = PedigreeDetails.PedigreeID,
            UserId = CoOwner.UserID
          };

          db.PedigreeUsers.Add(pedigreeUser_CoOwner);
          db.SaveChanges();

          if (!String.IsNullOrEmpty(PedigreeDetails.CoOwner.EmailAddress)) emailHandler.SendEmail(
          ToAddresses: new List<string> { PedigreeDetails.CoOwner.EmailAddress },
          Subject: $"Bully Assure: Co Owner Successfully Registered",
          Body: $"Good Day <br />" +
            $"<br /> " +
            $"<br /> {PedigreeDetails.CoOwner.OwnerName} has successfully been registered on Bully Assure system." +
            $"<br /> {PedigreeDetails.Owner.OwnerName} {PedigreeDetails.Owner.OwnerSurname} has registered you as a co-owner for {PedigreeDetails.Name}." +
            $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
            $"<br /> " +
            $"{Environment.NewLine } Login Details: " +
            $"<br />{ "Username: " + CoOwner.Username}" +
            $"<br />{ "Password: " + RandomPass}" +
            $"<br />" +
            $"<br /> Kind Regards," +
            $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
          AttachmentsBinaries: null,
          UserID: owner.UserID.ToString(),
          systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
          );
        }

        PedigreeDetails.BreederId = existingPedigree.BreederId;
        PedigreeDetails.KennelId = existingPedigree.KennelId;
        PedigreeDetails.DOB = existingPedigree.DOB;
        PedigreeDetails.DOI = existingPedigree.DOI;
        PedigreeDetails.DamID = existingPedigree.DamID;
        PedigreeDetails.SireID = existingPedigree.SireID;
        PedigreeDetails.Breed = existingPedigree.Breed;
        PedigreeDetails.Sex = existingPedigree.Sex;
        PedigreeDetails.ChipNo = existingPedigree.ChipNo;
        PedigreeDetails.Description = String.IsNullOrEmpty(PedigreeDetails.Description)
          ? existingPedigree.Description
          : PedigreeDetails.Description;

        this.db.Pedigrees.Update(PedigreeDetails);
        await this.db.SaveChangesAsync();

        emailHandler.SendEmail(
          ToAddresses: new List<string> { PedigreeDetails.Owner.EmailAddress, JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).EmailFromAddress },
          Subject: $"Bully Assure: New Pedigree Application",
          Body: $"Good Day <br />" +
            $"<br /> " +
            $"<br /> {PedigreeDetails.Owner.OwnerName} has applied for a new pedigree on Bully Assure system." +
            $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
            $"<br /> " +
            $"<br /> Kind Regards," +
            $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
          AttachmentsBinaries: null,
          UserID: owner.UserID.ToString(),
          systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
          );

        return View("ApplicationSuccessful", $"Pedigree has successfully been applied for. Please keep an eye on your email({owner.Email}) for any updates.");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpGet]
    public async Task<IActionResult> Delete(string PedigreeID)
    {
      try
      {
        if (!String.IsNullOrEmpty(PedigreeID))
        {
          var pedigree = await this.db.Pedigrees.FirstOrDefaultAsync(e => e.PedigreeID == Guid.Parse(PedigreeID));

          pedigree.IsDeleted = true;

          this.db.Pedigrees.Update(pedigree);
          await this.db.SaveChangesAsync();

          return RedirectToAction("Index");
        }
        return RedirectToAction("Index");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpGet]
    public async Task<IActionResult> AddResponse(string PedigreeApplicationId)
    {
      try
      {
        var user = await this.db.Users.FirstOrDefaultAsync(e => e.UserID == Guid.Parse(HttpContext.Session.GetString("UserID")));

        var pedigreeApplicationResponses = await this.db.PedigreeApplications
          .Include(e => e.Pedigree)
          .Include(e => e.Responses)
            .ThenInclude(f => f.User)
          .Include(e => e.Responses)
            .ThenInclude(f => f.ResponseDocuments)
          .FirstOrDefaultAsync(e => e.PedigreeApplicationId == Guid.Parse(PedigreeApplicationId));
        ViewData["PedigreeApplication"] = pedigreeApplicationResponses;
        return View("ApplicationResponses");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public async Task<IActionResult> AddApplicationResponses(PedigreeApplication_Response pedigreeApplication_Response, [FromForm] IFormFileCollection responseDocuments, [FromForm] string PedigreeApplicationId)
    {
      try
      {
        string UserID = HttpContext.Session.GetString("UserID");
        var user = db.Users.Include(g => g.UserPermission).FirstOrDefault(e => e.UserID == Guid.Parse(UserID));
        ViewData["User"] = user;

        if (responseDocuments.Count > 0)
        {
          pedigreeApplication_Response.ResponseDocuments = new List<ResponseDocuments>();

          foreach (var file in responseDocuments)
          {
            if (file.Length > 0)
            {
              using (var ms = new MemoryStream())
              {
                await file.CopyToAsync(ms);
                var fileBytes = ms.ToArray();
                string s = Convert.ToBase64String(fileBytes);
                pedigreeApplication_Response.ResponseDocuments.Add(new ResponseDocuments
                {
                  DateUploaded = DateTime.Now,
                  DocumentBinary = s,
                  DocumentName = file.FileName
                });
              }
            }
          }
        }

        pedigreeApplication_Response.DateCaptured = DateTime.Now;
        pedigreeApplication_Response.UserID = user.UserID;

        var pedigreeApplication = await this.db.PedigreeApplications.Include(f => f.Responses).Include(f => f.Pedigree).ThenInclude(f => f.Owner).FirstOrDefaultAsync(e => e.PedigreeApplicationId == Guid.Parse(PedigreeApplicationId));

        pedigreeApplication.Responses.Add(pedigreeApplication_Response);
        this.db.PedigreeApplications.Update(pedigreeApplication);
        await this.db.SaveChangesAsync();

        emailHandler.SendEmail(
          ToAddresses: new List<string> { pedigreeApplication.Pedigree.Owner.EmailAddress, JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).EmailFromAddress },
          Subject: $"Bully Assure: New Pedigree Application Response",
          Body: $"Good Day <br />" +
            $"<br /> " +
            $"<br /> {pedigreeApplication.Pedigree.Name}'s application has a new response from {user.Firstname} {user.Surname}." +
            $"<br /> Response message: {pedigreeApplication_Response.Message}." +
            $"<br /> " +
            $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
            $"<br /> " +
            $"<br /> Kind Regards," +
            $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
          AttachmentsBinaries: null,
          UserID: user.UserID.ToString(),
          systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
          );

        return RedirectToAction("Applications");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public async Task<IActionResult> SellPedigree([FromForm] string EmailAddress, [FromForm] string PedigreeId)
    {
      try
      {
        if (!String.IsNullOrEmpty(EmailAddress))
        {
          emailHandler.SendEmail(
          ToAddresses: new List<string> { EmailAddress },
          Subject: $"Bully Assure: Pedigree Adoption Request",
          Body: $"Good Day <br />" +
            $"<br /> " +
            $"<br /> Congratulations on purchasing your new pedigree! Your Pedigree application awaits you." +
            $"<br /> Bully Assure Litter Id: {(await this.db.Pedigrees.FirstOrDefaultAsync(e => e.PedigreeID == Guid.Parse(PedigreeId))).BAID}" +
            $"<br /> " +
            $"<br /> Click on the below link to capture your application details." +
            $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}/Pedigree/PedigreeApplication?PedigreeID={PedigreeId}\">Start Application</a>" +
            $"<br />" +
            $"<br /> Kind Regards," +
            $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
          AttachmentsBinaries: null,
          UserID: HttpContext.Session.GetString("UserID"),
          systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
          );
        }

        return RedirectToAction("Index");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public async Task<IActionResult> ChangeOwnership([FromForm] string EmailAddress, [FromForm] string PedigreeId)
    {
      try
      {
        if (!String.IsNullOrEmpty(EmailAddress))
        {
          emailHandler.SendEmail(
          ToAddresses: new List<string> { EmailAddress },
          Subject: $"Bully Assure: Pedigree Adoption Request",
          Body: $"Good Day <br />" +
            $"<br /> " +
            $"<br /> Congratulations on adopting your new pedigree! Your Pedigree application awaits you." +
            $"<br /> Bully Assure Litter Id: {(await this.db.Pedigrees.FirstOrDefaultAsync(e => e.PedigreeID == Guid.Parse(PedigreeId))).BAID}" +
            $"<br /> " +
            $"<br /> Click on the below link to capture your application details." +
            $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}/Pedigree/PedigreeApplication?PedigreeID={PedigreeId}\">Start Application</a>" +
            $"<br />" +
            $"<br /> Kind Regards," +
            $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
          AttachmentsBinaries: null,
          UserID: HttpContext.Session.GetString("UserID"),
          systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
          );
        }

        return RedirectToAction("Index");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpGet]
    public async Task<IActionResult> ApplicationAccept(string PedigreeApplicationId)
    {
      try
      {
        string UserID = HttpContext.Session.GetString("UserID");
        var user = db.Users.Include(g => g.UserPermission).FirstOrDefault(e => e.UserID == Guid.Parse(UserID));

        var PedigreeApplication = await this.db.PedigreeApplications
          .Include(f => f.Responses)
            .ThenInclude(g => g.User)
          .Include(f => f.Responses)
            .ThenInclude(g => g.ResponseDocuments)
          .Include(g => g.Pedigree)
          .Include(e => e.Pedigree.Dam)
          .Include(e => e.Pedigree.Sire)
          .Include(e => e.Pedigree.Breeder)
          .Include(e => e.Pedigree.CoOwner)
          .Include(e => e.Pedigree.Owner)
          .Include(e => e.Pedigree.PedigreeFieldsValues.OrderBy(f => f.PedigreeFields.FieldName))
            .ThenInclude(g => g.PedigreeFields)
            .ThenInclude(g => g.FieldType)
            .FirstOrDefaultAsync(e => e.PedigreeApplicationId == Guid.Parse(PedigreeApplicationId));

        PedigreeApplication.Accepted = true;
        PedigreeApplication.Closed = true;
        PedigreeApplication.ApplicationDateFinalised = DateTime.Now;

        this.db.PedigreeApplications.Update(PedigreeApplication);
        await this.db.SaveChangesAsync();

        emailHandler.SendEmail(
          ToAddresses: new List<string> { PedigreeApplication.Pedigree.Owner.EmailAddress },
          Subject: $"Bully Assure: Pedigree Application Accepted",
          Body: $"Good Day <br />" +
            $"<br /> " +
            $"<br /> Congratulations! Your pedigree application has been accepted." +
            $"<br />" +
            $"<br /> Kind Regards," +
            $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
          AttachmentsBinaries: null,
          UserID: HttpContext.Session.GetString("UserID"),
          systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
          );

        return RedirectToAction("Applications");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return BadRequest(ex.Message);
      }
    }

    [ControllerActionFilter]
    [HttpGet]
    public async Task<IActionResult> ApplicationReject(string PedigreeApplicationId)
    {
      try
      {
        string UserID = HttpContext.Session.GetString("UserID");
        var user = db.Users.Include(g => g.UserPermission).FirstOrDefault(e => e.UserID == Guid.Parse(UserID));

        var PedigreeApplication = await this.db.PedigreeApplications
          .Include(f => f.Responses)
            .ThenInclude(g => g.User)
          .Include(f => f.Responses)
            .ThenInclude(g => g.ResponseDocuments)
          .Include(g => g.Pedigree)
          .Include(e => e.Pedigree.Dam)
          .Include(e => e.Pedigree.Sire)
          .Include(e => e.Pedigree.Breeder)
          .Include(e => e.Pedigree.CoOwner)
          .Include(e => e.Pedigree.Owner)
          .Include(e => e.Pedigree.PedigreeFieldsValues.OrderBy(f => f.PedigreeFields.FieldName))
            .ThenInclude(g => g.PedigreeFields)
            .ThenInclude(g => g.FieldType)
            .FirstOrDefaultAsync(e => e.PedigreeApplicationId == Guid.Parse(PedigreeApplicationId));

        PedigreeApplication.Accepted = false;
        PedigreeApplication.Closed = true;
        PedigreeApplication.ApplicationDateFinalised = DateTime.Now;

        this.db.PedigreeApplications.Update(PedigreeApplication);
        await this.db.SaveChangesAsync();

        emailHandler.SendEmail(
          ToAddresses: new List<string> { PedigreeApplication.Pedigree.Owner.EmailAddress },
          Subject: $"Bully Assure: Pedigree Application Rejected",
          Body: $"Good Day <br />" +
            $"<br /> " +
            $"<br /> Your Pedigree Application has unfortunately been rejected." +
            $"<br />" +
            $"<br /> Kind Regards," +
            $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
          AttachmentsBinaries: null,
          UserID: HttpContext.Session.GetString("UserID"),
          systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
          );

        return RedirectToAction("Applications");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return BadRequest(ex.Message);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public async Task<IActionResult> GetDocument([FromForm] string DocID, [FromForm] string DocName)
    {
      try
      {
        var document = await this.db.ResponseDocuments.FirstOrDefaultAsync(e => e.ResponseDocId == Guid.Parse(DocID) && e.DocumentName == DocName);

        return Ok(document.DocumentBinary);
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return BadRequest(ex.Message);
      }
    }

    [ControllerActionFilter]
    [HttpGet]
    public async Task<IActionResult> GetPedigreeCertificateTemplate(string PedigreeID)
    {
      try
      {
        if (!String.IsNullOrEmpty(PedigreeID))
        {
          var pedigree = await this.db.Pedigrees.Include(e => e.Sire).Include(e => e.Dam).FirstOrDefaultAsync(f => f.PedigreeID == Guid.Parse(PedigreeID));
          return View("PedigreeCertificate", pedigree);
        }

        return NotFound(new { result = "No valid pedigreeID provided." });
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return BadRequest(ex.Message);
      }
    }
  }
}