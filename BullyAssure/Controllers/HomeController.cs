﻿using BullyAssure.Filters;
using BullyAssure.Models;
using DatabaseConnector;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace BullyAssure.Controllers
{
  public class HomeController : Controller
  {
    private BullyAssureContext db;
    private readonly ILogger<HomeController> _logger;

    public HomeController(BullyAssureContext _db, ILogger<HomeController> logger)
    {
      db = _db;
      _logger = logger;
    }

    [ControllerActionFilter]
    [HttpGet]
    public IActionResult Index()
    {
      return RedirectToAction("Index", "Dashboard");
    }

    [ControllerActionFilter]
    [HttpGet]
    public IActionResult Privacy()
    {
      return View();
    }

    [HttpGet]
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
      return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
  }
}