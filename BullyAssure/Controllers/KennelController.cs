﻿using ApplicationWorkers.Communications;
using BullyAssure.Filters;
using DatabaseConnector;
using DatabaseConnector.Tables.Configuration;
using DatabaseConnector.Tables.Kennels;
using DatabaseConnector.Tables.Permissions;
using DatabaseConnector.Tables.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace BullyAssure.Controllers
{
  public class KennelController : Controller
  {
    private readonly BullyAssureContext db;
    private readonly IConfiguration config;
    private readonly EmailHandler emailHandler;

    public KennelController(BullyAssureContext _db, IConfiguration _config, EmailHandler _emailHandler)
    {
      this.db = _db;
      this.config = _config;
      this.emailHandler = _emailHandler;
    }

    [ControllerActionFilter]
    public IActionResult Index()
    {
      var kennelFields = db.KennelPageFields.Where(e => !e.isDeleted).ToList();
      var kennelDetails = db.Kennels.Include(e => e.KennelFieldsValues.OrderBy(f => f.KennelFields.FieldName)).ThenInclude(g => g.KennelFields).ThenInclude(g => g.FieldType).Where(f => !f.isDeleted).ToList();
      foreach (var item in kennelFields)
      {
        foreach (var kennel in kennelDetails)
        {
          if (!kennel.KennelFieldsValues.Any(e => e.KennelFieldsID == item.FieldID))
          {
            kennel.KennelFieldsValues.Add(new KennelFieldsValues
            {
              KennelFieldsID = item.FieldID,
              KennelFields = item
            });
          }
        }
      }
      ViewData["Error"] = "";
      return View(kennelDetails);
    }

    [ControllerActionFilter]
    public IActionResult Create()
    {
      var KennelPageFields = db.KennelPageFields.Include(e => e.FieldType).Where(e => !e.isDeleted).ToList();
      KennelMain kennelMain = new KennelMain();
      kennelMain.KennelFieldsValues = new List<KennelFieldsValues>();
      foreach (var item in KennelPageFields)
      {
        kennelMain.KennelFieldsValues.Add(new KennelFieldsValues
        {
          KennelFields = item,
          KennelFieldsID = item.FieldID
        });
      }
      return View("Create", kennelMain);
    }

    [ControllerActionFilter]
    public IActionResult Save(KennelMain KennelDetails)
    {
      try
      {
        if (KennelDetails != null)
        {
          var NewKennel = (KennelDetails.KennelID == Guid.Empty);
          db.Kennels.Update(KennelDetails);
          db.SaveChanges();

          string RandomPass = Encryption.PasswordEncrypt.RandomString(8);
          UserMain user = new UserMain
          {
            Username = $"{KennelDetails.KennelName}_kennel",
            Email = KennelDetails.EmailAddress,
            Firstname = KennelDetails.KennelName,
            isKennel = true,
            Cellnumber = null,
            OrgStructureID = db.OrgStructures.FirstOrDefault(e => e.StructureName == "Kennel").StructureID,
            Password = new Encryption.PasswordEncrypt().GeneratePassword(RandomPass),
            UserPermission = new UserPermissions()
          };

          if (NewKennel)
          {
            db.Users.Add(user);
            db.SaveChanges();

            KennelUser kennelUsers = new KennelUser
            {
              KennelID = KennelDetails.KennelID,
              UserId = user.UserID
            };

            db.KennelUsers.Add(kennelUsers);
            db.SaveChanges();

            emailHandler.SendEmail(
           ToAddresses: new List<string> { KennelDetails.EmailAddress },
           Subject: $"Bully Assure: Kennel Successfully Registered",
           Body: $"Good Day <br />" +
             $"<br /> " +
             $"<br /> {user.Email} has successfully been { (NewKennel ? "registered" : "saved") } on Bully Assure system." +
             $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
             $"<br /> " +
             $"{ (NewKennel ? "<br />" + "Login Details: " : "") }" +
             $"{ (NewKennel ? "<br />" + "Username: " + user.Username : "") }" +
             $"{ (NewKennel ? "<br />" + "Password: " + RandomPass : "") }" +
             $"<br />" +
             $"<br /> Kind Regards," +
             $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
           AttachmentsBinaries: null,
           UserID: HttpContext.Session.GetString("UserID"),
           systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
           );
          }
          else
          {
            emailHandler.SendEmail(
              ToAddresses: new List<string> { KennelDetails.EmailAddress },
              Subject: $"Bully Assure: Kennel Successfully Saved",
              Body: $"Good Day <br />" +
              $"<br /> " +
              $"<br /> {KennelDetails.KennelName} has successfully been { (NewKennel ? "registered" : "saved") } on Bully Assure system." +
              $"<br /> If you are not aware of this, please contact System Administrator on <a href=\"mailto:{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).UserName}\">System Admin</a>." +
              $"<br /> " +
              $"<br />" +
              $"<br /> Kind Regards," +
             $"<br /> <a href=\"{JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION")).WebsiteUrl}\">Bully Assure System</a>",
              AttachmentsBinaries: null,
              UserID: HttpContext.Session.GetString("UserID"),
              systemConfiguration: JsonSerializer.Deserialize<SystemConfiguration>(HttpContext.Session.GetString("SYSTEMCONFIGURATION"))
              );
          }

          ViewData["Error"] = "Successfully saved kennel details.";
        }
        else
        {
          ViewData["Error"] = "Failed to save kennel details.";
        }
        return RedirectToAction("Index", "Kennel");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        ViewData["Error"] = "Error Occurred";
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    public IActionResult Edit(string KennelID)
    {
      if (!String.IsNullOrEmpty(KennelID))
      {
        return View("Edit", db.Kennels.Include(e => e.KennelFieldsValues).ThenInclude(f => f.KennelFields).ThenInclude(g => g.FieldType).FirstOrDefault(e => e.KennelID == Guid.Parse(KennelID)));
      }
      else
      {
        ViewData["Error"] = "Failed to edit kennel details.";
        return View();
      }
    }

    [ControllerActionFilter]
    public IActionResult Delete(string KennelID)
    {
      if (!String.IsNullOrEmpty(KennelID))
      {
        var kennel = db.Kennels.FirstOrDefault(e => e.KennelID == Guid.Parse(KennelID));
        if (kennel == null)
        {
          ViewData["Error"] = "Kennel not found.";
          return View("Index");
        }
        kennel.isDeleted = true;
        db.Kennels.Update(kennel);
        db.SaveChanges();

        ViewData["Error"] = "Kennel Successfully Deleted.";
        return RedirectToAction("Index", "Kennel");
      }
      else
      {
        ViewData["Error"] = "Failed to delete kennel.";
        return View();
      }
    }
  }
}