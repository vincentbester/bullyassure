using BullyAssure.Filters;
using DatabaseConnector;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace BullyAssure.Controllers
{
  public class FieldController : Controller
  {
    private BullyAssureContext db;
    private IConfiguration config;

    public FieldController(BullyAssureContext _db, IConfiguration _config)
    {
      db = _db;
      config = _config;
    }

    [ControllerActionFilter]
    [HttpGet]
    public IActionResult GetFields()
    {
      return View();
    }
  }
}