﻿using ApplicationWorkers.Communications;
using BullyAssure.Filters;
using DatabaseConnector;
using DatabaseConnector.Tables.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BullyAssure.Controllers
{
  public class UserController : Controller
  {
    private readonly BullyAssureContext db;
    private readonly IConfiguration config;
    private readonly EmailHandler emailHandler;

    public UserController(BullyAssureContext _db, IConfiguration _config, EmailHandler _emailHandler)
    {
      this.db = _db;
      this.config = _config;
      this.emailHandler = _emailHandler;
    }

    [ControllerActionFilter]
    [HttpGet]
    public IActionResult Index()
    {
      try
      {
        var UserID = HttpContext.Session.GetString("UserID");

        UserMain userMain = this.db.Users.FirstOrDefault(e => e.UserID == Guid.Parse(UserID));

        return View("Index", userMain);
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpGet]
    public IActionResult Edit(string UserId)
    {
      try
      {
        UserMain userMain = this.db.Users.Include(e => e.UserPermission).FirstOrDefault(e => e.UserID == Guid.Parse(UserId));

        return View("Edit", userMain);
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }

    [ControllerActionFilter]
    [HttpPost]
    public async Task<IActionResult> UpdateUser(UserMain userMain)
    {
      _ = userMain ?? throw new NullReferenceException();
      try
      {
        userMain.Password = (await this.db.Users.AsNoTracking().FirstOrDefaultAsync(e => e.UserID == userMain.UserID)).Password;
        this.db.Update(userMain);
        await this.db.SaveChangesAsync();

        return RedirectToAction("Index");
      }
      catch (Exception ex)
      {
        LogFilter.Write(db, HttpContext.Request.QueryString.ToString(), HttpContext.Request.Path, HttpContext.Session.GetString("UserID"), ex);
        return View("Error", ex);
      }
    }
  }
}