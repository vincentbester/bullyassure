﻿using BullyAssure.Models;
using DatabaseConnector;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Diagnostics;
using System.Linq;

namespace BullyAssure.Controllers
{
  public class ComponentsController : Controller
  {
    private BullyAssureContext db;
    private IConfiguration config;

    public ComponentsController(BullyAssureContext _db, IConfiguration _config)
    {
      db = _db;
      config = _config;
    }

    private bool AuthenticateUser(string token, string userId)
    {
      return db.UserSessions.Any(e => e.Token == token && e.UserMainID == Guid.Parse(userId) && e.ExpiryDate > DateTime.Now);
    }

    [HttpGet]
    public IActionResult inputbox()
    {
      return View();
    }

    [HttpGet]
    public IActionResult StatCard()
    {
      return View();
    }

    [HttpGet]
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
      return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
  }
}