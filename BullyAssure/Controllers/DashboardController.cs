using BullyAssure.Filters;
using BullyAssure.Models;
using DatabaseConnector;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace BullyAssure.Controllers
{
  public class DashboardController : Controller
  {
    public static BullyAssureContext db;
    private IConfiguration config;

    public DashboardController(BullyAssureContext _db, IConfiguration _config)
    {
      db = _db;
      config = _config;
    }

    [ControllerActionFilter]
    [HttpGet]
    public async Task<IActionResult> Index()
    {
      var UserId = HttpContext.Session.GetString("UserID");

      ViewData["FirstName"] = (await db.Users.FirstOrDefaultAsync(e => e.UserID == Guid.Parse(UserId))).Firstname;
      ViewData["LastName"] = (await db.Users.FirstOrDefaultAsync(e => e.UserID == Guid.Parse(UserId))).Firstname;

      return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    [HttpGet]
    public IActionResult Error()
    {
      return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
  }
}