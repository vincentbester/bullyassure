﻿using BullyAssure.Helpers;
using DatabaseConnector.Tables.Pedigrees;
using System;
using System.Web.Http;
using System.Web.Mvc;

namespace BullyAssure.Controllers.APIControllers
{
  public class CertificateController : ApiController
  {
    public string GetPedigreeCertificate(PedigreeMain pedigree, object Controller)
    {
      try
      {
        string cert = RazorViewToString.RenderRazorViewToString((System.Web.Mvc.Controller)ControllerContext.Controller, "PedigreeCertificate", pedigree);
        return cert;
      }
      catch (Exception)
      {
      }
      return null;
    }
  }
}