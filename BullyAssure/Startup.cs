using ApplicationWorkers.Communications;
using BullyAssure.Helpers;
using DatabaseConnector;
using iTextSharp.tool.xml.css;
using iTextSharp.tool.xml.pipeline.css;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.OpenApi.Models;
using System;
using System.IO;

namespace BullyAssure
{
  public class Startup
  {
    private readonly string MyAllowSpecificOrigins = "customCORS";
    private readonly IWebHostEnvironment _env;

    public static bool ConvertToPDF = false;
    public static MemoryStream PDFStream = new MemoryStream();

    public Startup(IConfiguration configuration, IWebHostEnvironment env)
    {
      Configuration = configuration;
      _env = env;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
      services.AddSession(so =>
      {
        so.IdleTimeout = TimeSpan.FromMinutes(20);
      });

      services.AddDistributedMemoryCache();

      services.AddCors(options =>
      {
        options.AddPolicy(name: MyAllowSpecificOrigins,
                  builder =>
                  {
                    builder.AllowAnyOrigin()
                              .AllowAnyHeader()
                              .AllowAnyMethod();
                  });
      });

      services.AddSwaggerGen(c =>
      {
        c.SwaggerDoc("v1", new OpenApiInfo
        {
          Version = "v1",
          Title = "Bully Assure API Documentation",
          Description = "Web API back end for Bully Assure.",
          TermsOfService = new Uri("https://bullyassure.com"),
          Contact = new OpenApiContact
          {
            Name = "Vincent Bester",
            Email = "vbester3@gmail.com",
            Url = new Uri("https://twitter.com/V_Bester1993"),
          },
          License = new OpenApiLicense
          {
            Name = "Use under LICX",
            Url = new Uri("https://example.com/license"),
          }
        });
      });

      if (this._env.EnvironmentName == "Development")
      {
        services.AddDbContext<BullyAssureContext>(options =>
          options.UseSqlServer(Configuration.GetConnectionString("SQLConnection")));
      }
      else
      {
        services.AddDbContext<BullyAssureContext>(options =>
         options.UseSqlite($"Data Source=\"{Environment.CurrentDirectory}/{Configuration.GetConnectionString("Sqlite")}\""));
      }
      services.AddTransient<IViewRenderService, ViewRenderService>();
      services.AddScoped<BullyAssureContext>();
      services.AddScoped<EmailHandler>();

      services.AddScoped<ICssFile, CssFileImpl>();
      services.AddScoped<ICSSResolver, StyleAttrCSSResolver>();

      services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

      services.AddControllersWithViews();

      services.AddMvc(options =>
      {
        options.EnableEndpointRouting = false;
      });
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      app.UseDeveloperExceptionPage();
      //app.UseStatusCodePagesWithRedirects("/error/{0}");

      //app.Use(async (ctx, next) =>
      //{
      //    await next();

      //    if (ctx.Response.StatusCode == 404 && !ctx.Response.HasStarted)
      //    {
      //        //Re-execute the request so the user gets the error page
      //        string originalPath = ctx.Request.Path.Value;
      //        ctx.Items["originalPath"] = originalPath;
      //        ctx.Request.Path = "/error/404";
      //        await next();
      //    }
      //});

      // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
      app.UseHsts();

      app.UseAuthentication();

      app.UseSwagger();

      app.UseSwaggerUI(c =>
      {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "Bully Assure");
      });

      app.UseCors(MyAllowSpecificOrigins);

      app.UseHttpsRedirection();
      app.UseStaticFiles();

      app.UseRouting();

      app.UseAuthorization();

      // IMPORTANT: This session call MUST go before UseMvc()
      app.UseSession();

      // Add MVC to the request pipeline.
      app.UseMvc(routes =>
      {
        routes.MapRoute(
                  name: "default",
                  template: "{controller=Home}/{action=Index}/{id?}");
      });
    }
  }
}