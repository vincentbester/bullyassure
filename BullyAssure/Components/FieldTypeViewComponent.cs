﻿using DatabaseConnector;
using DatabaseConnector.Tables.Fields;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullyAssure.Components
{
  public class FieldTypeViewComponent : ViewComponent
  {
    private readonly BullyAssureContext db;

    public FieldTypeViewComponent(BullyAssureContext context)
    {
      db = context;
    }

    public async Task<IViewComponentResult> InvokeAsync()
    {
      var items = await GetPageFieldAsync();
      return View(items);
    }

    private Task<List<FieldType>> GetPageFieldAsync()
    {
      var UserID = HttpContext.Session.GetString("UserID");
      return db.FieldTypes.Where(e => !e.isDeleted).ToListAsync();
    }
  }
}