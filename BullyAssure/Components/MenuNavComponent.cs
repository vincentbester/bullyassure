﻿using DatabaseConnector;
using DatabaseConnector.Tables.Settings;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullyAssure.Components
{
  public class MenuNavViewComponent : ViewComponent
  {
    private readonly BullyAssureContext db;

    public MenuNavViewComponent(BullyAssureContext context)
    {
      db = context;
    }

    public async Task<IViewComponentResult> InvokeAsync()
    {
      var items = await GetMenuAsync();
      return View(items);
    }

    private Task<List<Setting>> GetMenuAsync()
    {
      var UserID = HttpContext.Session.GetString("UserID");
      return db.Settings
          .Where(e =>
              db.PermissionsStructures.Include(g => g.OrgStructure)
                  .Any(f =>
                      f.OrgStructure.StructureID == db.Users.FirstOrDefault(g => g.UserID == Guid.Parse(UserID)).OrgStructureID
                      && f.Path == e.To)
              && !e.isDeleted)
          .OrderBy(e => e.Order).ToListAsync();
    }
  }
}