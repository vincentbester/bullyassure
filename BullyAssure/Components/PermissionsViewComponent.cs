﻿using DatabaseConnector;
using DatabaseConnector.Tables.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullyAssure.Components
{
  public class PermissionsViewComponent : ViewComponent
  {
    private readonly BullyAssureContext db;

    public PermissionsViewComponent(BullyAssureContext context)
    {
      this.db = context;
    }

    public async Task<IViewComponentResult> InvokeAsync()
    {
      var items = await GetUsersAsync();
      return View(items);
    }

    private Task<List<UserMain>> GetUsersAsync()
    {
      var UserID = HttpContext.Session.GetString("UserID");
      return db.Users
        .Include(e => e.OrgStructure)
          .ThenInclude(e => e.ParentStructure)
          .Include(e => e.UserPermission)
        .Where(e => e.UserID != Guid.Parse(UserID))
        .ToListAsync();
    }
  }
}