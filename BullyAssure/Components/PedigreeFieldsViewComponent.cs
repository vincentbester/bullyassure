﻿using DatabaseConnector;
using DatabaseConnector.Tables.Pedigrees;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullyAssure.Components
{
  public class PedigreeFieldsViewComponent : ViewComponent
  {
    private readonly BullyAssureContext db;
    private IConfiguration config;

    public PedigreeFieldsViewComponent(BullyAssureContext context, IConfiguration _config)
    {
      db = context;
      config = _config;
    }

    public async Task<IViewComponentResult> InvokeAsync()
    {
      var UserID = HttpContext.Session.GetString("UserID");
      var items = await GetPedigreeFieldsAsync();
      return View(items);
    }

    private async Task<List<PedigreePageField>> GetPedigreeFieldsAsync()
    {
      var UserID = HttpContext.Session.GetString("UserID");
      return await db.PedigreePageField.Include(e => e.FieldType).Where(e => !e.isDeleted).ToListAsync();
    }
  }
}