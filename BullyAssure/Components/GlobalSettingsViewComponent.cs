﻿using DatabaseConnector;
using DatabaseConnector.Tables.Settings;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace BullyAssure.Components
{
  public class GlobalSettingsViewComponent : ViewComponent
  {
    private readonly BullyAssureContext db;

    public GlobalSettingsViewComponent(BullyAssureContext context)
    {
      db = context;
    }

    public async Task<IViewComponentResult> InvokeAsync()
    {
      var items = await GetGlobalSettingsAsync();
      return View(items);
    }

    private Task<GlobalSettings> GetGlobalSettingsAsync()
    {
      var UserID = HttpContext.Session.GetString("UserID");
      return db.GlobalSettings.FirstOrDefaultAsync();
    }
  }
}