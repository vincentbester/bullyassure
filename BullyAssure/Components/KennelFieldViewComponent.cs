﻿using DatabaseConnector;
using DatabaseConnector.Tables.Kennels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullyAssure.Components
{
  public class KennelFieldViewComponent : ViewComponent
  {
    private readonly BullyAssureContext db;

    public KennelFieldViewComponent(BullyAssureContext context)
    {
      db = context;
    }

    public async Task<IViewComponentResult> InvokeAsync()
    {
      var items = await GetKennelPageFieldAsync();
      return View(items);
    }

    private Task<List<KennelPageField>> GetKennelPageFieldAsync()
    {
      var UserID = HttpContext.Session.GetString("UserID");
      return db.KennelPageFields.Include(e => e.FieldType).Where(e => !e.isDeleted).ToListAsync();
    }
  }
}