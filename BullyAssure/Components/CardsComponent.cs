﻿using DatabaseConnector;
using DatabaseConnector.Tables.StatCards;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BullyAssure.Components
{
  public class CardsViewComponent : ViewComponent
  {
    private readonly BullyAssureContext db;
    private IConfiguration config;

    public CardsViewComponent(BullyAssureContext context, IConfiguration _config)
    {
      db = context;
      config = _config;
    }

    public async Task<IViewComponentResult> InvokeAsync()
    {
      var UserID = HttpContext.Session.GetString("UserID");
      var items = await GetCardsAsync();
      foreach (var item in items)
      {
        item.Value = (GetValue(item.ValueTopAlg.Replace("|[UserID]|", UserID.ToUpper())) / GetValue(item.ValueBottomAlg.Replace("|[UserID]|", UserID.ToUpper()))) * 100;
      }
      return View(items);
    }

    private Task<List<StatCard>> GetCardsAsync()
    {
      var UserID = HttpContext.Session.GetString("UserID");
      return db.StatCards.ToListAsync();
    }

    private double GetValue(string SQLCmd)
    {
      try
      {
        double Value = 0;
        ;
        using (SqliteConnection conn = new SqliteConnection(db.Database.GetDbConnection().ConnectionString))
        {
          conn.Open();
          using (SqliteCommand cmd = new SqliteCommand(SQLCmd, conn))
          {
            using (SqliteDataReader rd = cmd.ExecuteReader())
            {
              while (rd.Read())
              {
                Value = Convert.ToDouble(rd["Value"].ToString());
              }
            }
          }
        }
        return Value;
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message);
        return (double)0;
      }
    }
  }
}