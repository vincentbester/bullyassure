﻿using DatabaseConnector;
using DatabaseConnector.Tables.Breeders;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullyAssure.Components
{
  public class BreederFieldViewComponent : ViewComponent
  {
    private readonly BullyAssureContext db;

    public BreederFieldViewComponent(BullyAssureContext context)
    {
      db = context;
    }

    public async Task<IViewComponentResult> InvokeAsync()
    {
      var items = await GetBreederPageFieldAsync();
      return View(items);
    }

    private Task<List<BreederPageField>> GetBreederPageFieldAsync()
    {
      var UserID = HttpContext.Session.GetString("UserID");
      return db.BreederPageFields.Include(e => e.FieldType).Where(e => !e.isDeleted).ToListAsync();
    }
  }
}