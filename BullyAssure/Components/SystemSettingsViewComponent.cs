﻿using DatabaseConnector;
using DatabaseConnector.Tables.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace BullyAssure.Components
{
  public class SystemSettingsViewComponent : ViewComponent
  {
    private readonly BullyAssureContext db;

    public SystemSettingsViewComponent(BullyAssureContext context)
    {
      db = context;
    }

    public async Task<IViewComponentResult> InvokeAsync()
    {
      var items = await GetSystemSettingsAsync();
      return View(items);
    }

    private Task<SystemConfiguration> GetSystemSettingsAsync()
    {
      var UserID = HttpContext.Session.GetString("UserID");
      return db.SystemConfigurations.FirstOrDefaultAsync();
    }
  }
}