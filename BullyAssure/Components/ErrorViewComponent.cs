﻿using DatabaseConnector;
using DatabaseConnector.Tables.Audits;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullyAssure.Components
{
  public class ErrorViewComponent : ViewComponent
  {
    private readonly BullyAssureContext db;

    public ErrorViewComponent(BullyAssureContext context)
    {
      db = context;
    }

    public async Task<IViewComponentResult> InvokeAsync()
    {
      var items = await GetLoggingsAsync();
      return View(items);
    }

    private Task<List<ApplicationLogging>> GetLoggingsAsync()
    {
      var UserID = HttpContext.Session.GetString("UserID");
      return db.Loggings.OrderByDescending(e => e.CaptureDate).ToListAsync();
    }
  }
}