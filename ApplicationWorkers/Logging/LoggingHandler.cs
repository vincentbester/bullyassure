﻿using DatabaseConnector;
using DatabaseConnector.Tables.Audits;
using System;

namespace ApplicationWorkers.Logging
{
  public class LoggingHandler
  {
    private BullyAssureContext db { get; set; }

    public LoggingHandler(BullyAssureContext _db)
    {
      db = _db;
    }

    public void LogError(Exception ex, string UserID, string ToPath, string QueryString)
    {
      var _loggin = new ApplicationLogging();
      if (ex != null)
      {
        _loggin.ErrorMessage = $"Error Message: {ex.Message}. || Stack Trace: {ex.StackTrace} || Inner Exception: {ex.InnerException}";
      }
      _loggin.QueryString = QueryString;
      _loggin.Topath = ToPath;
      _loggin.UserID = String.IsNullOrEmpty(UserID) ? Guid.Empty : Guid.Parse(UserID);
      _loggin.CaptureDate = DateTime.Now;

      db.Loggings.Add(_loggin);
      db.SaveChanges();
    }

    public void AuditTrail(string jsonBody, string UserID, string ToPath, string QueryString)
    {
      var _loggin = new ApplicationLogging();
      if (!string.IsNullOrEmpty(jsonBody))
      {
        _loggin.ErrorMessage = jsonBody;
      }
      else
      {
        _loggin.ErrorMessage = "No Form Data specified";
      }
      _loggin.QueryString = QueryString;
      _loggin.Topath = ToPath;
      _loggin.UserID = String.IsNullOrEmpty(UserID) ? Guid.Empty : Guid.Parse(UserID);
      _loggin.CaptureDate = DateTime.Now;

      db.Loggings.Add(_loggin);
      db.SaveChanges();
    }
  }
}