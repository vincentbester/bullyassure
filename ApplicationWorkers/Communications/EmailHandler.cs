﻿using ApplicationWorkers.Logging;
using DatabaseConnector;
using DatabaseConnector.Tables.Communication;
using DatabaseConnector.Tables.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;

namespace ApplicationWorkers.Communications
{
  public static class EmailConfig
  {
    public static string EmailFromAddress { get; set; }
    public static string UserName { get; set; }
    public static string Password { get; set; }
    public static string SMTPClient { get; set; }
    public static int Port { get; set; }
  }

  public class EmailHandler
  {
    private BullyAssureContext db { get; set; }

    public EmailHandler(BullyAssureContext _db)
    {
      db = _db;
    }

    public bool SendEmail(List<string> ToAddresses, string Subject, string Body, Dictionary<string, string> AttachmentsBinaries, string UserID, SystemConfiguration systemConfiguration)
    {
      var emails = new List<EmailCapture>();
      try
      {
        MailMessage mail = new MailMessage();
        SmtpClient SmtpServer = new SmtpClient(systemConfiguration.SMTPClient);
        mail.From = new MailAddress(systemConfiguration.UserName);
        foreach (var to in ToAddresses)
        {
          if (!String.IsNullOrEmpty(to))
            mail.To.Add(to);
        }
        mail.Subject = Subject;
        mail.Body = Body;
        mail.IsBodyHtml = true;

        if (AttachmentsBinaries != null) foreach (var attach in AttachmentsBinaries)
          {
            System.Net.Mail.Attachment attachment;
            attachment = new System.Net.Mail.Attachment(new MemoryStream(Convert.FromBase64String(attach.Value)), attach.Key);
            mail.Attachments.Add(attachment);
          }

        SmtpServer.Port = systemConfiguration.Port;
        SmtpServer.Credentials = new System.Net.NetworkCredential(systemConfiguration.UserName, systemConfiguration.Password);
        SmtpServer.EnableSsl = true;
        SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
        SmtpServer.UseDefaultCredentials = false;
        SmtpServer.Timeout = 100000;

        foreach (var to in ToAddresses)
        {
          emails.Add(new EmailCapture
          {
            EmailMessage = Body,
            FromEmail = EmailConfig.EmailFromAddress,
            ToEmail = to,
            UserID = String.IsNullOrEmpty(UserID) ? Guid.Empty : Guid.Parse(UserID),
            CaptureDate = DateTime.Now
          });
        }

        SmtpServer.Send(mail);

        foreach (var item in emails)
        {
          item.SentSuccessfull = true;
        }

        db.EmailCaptures.AddRange(emails);
        db.SaveChanges();

        return true;
      }
      catch (Exception ex)
      {
        new LoggingHandler(db).LogError(ex, null, "SendEmail", "");

        db.EmailCaptures.AddRange(emails);
        db.SaveChanges();

        return false;
      }
    }
  }
}